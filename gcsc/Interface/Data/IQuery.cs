﻿using System.Collections.Generic;
using System.Data;

namespace GCSC.Interface.Data
{
	public interface IQuery
	{
		int Execute();

		int Execute(Dictionary<string, object> parameters);

		int ExecuteScript();

		string Command { get; set; }

		string ConnectionString { get; set; }

		string Schema { get; set; }

		int ExecutionCount { get; }

		bool PreparedStatement { get; }

		CommandType CommandType { get; set; }
	}
}
