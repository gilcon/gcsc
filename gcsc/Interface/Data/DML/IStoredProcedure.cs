﻿namespace GCSC.Interface.Data.DML
{
	public interface IStoredProcedure
	{
		void Dispose(bool isDisposing);

		void Add<T>(T item, bool track);

		void Add(params object[] value);

		void Add(object value);

		void Add(string parameterName, object value);

		object ParamReturnValue(string ParamaterName);

		object[][] Read(bool refresh = false);

		object[][] Read(string connectionString, bool refresh = false);

		int Execute(bool refresh = false);

		int Execute(string connectionString, bool refresh = false);

		string ProcedureName { set; }

		string Schema { set; }

		GCSC.Data.ExecutionType ExecutionType { get; set; }

		bool AllowMultipleExecution { get; set; }

		bool PreparedStatement { get; }
	}
}
