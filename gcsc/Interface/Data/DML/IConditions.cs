﻿using GCSC.Data;

namespace GCSC.Interface.Data.DML
{
	public interface IConditions
	{
		IConditions AssignConditions { get; set; }

		string Condition { get; set; }

		void Where(string columnName, string parameterName);

		void Where(string columnName, string parameterName, Operator op);

		void AndWhere(string columnName, string parameterName);

		void AndWhere(string columnName, string parameterName, Operator op);

		void OrWhere(string columnName, string parameterName);

		void OrWhere(string columnName, string parameterName, Operator op);

		void _AndWhereSymbol();

		void _OrWhereSymbol();

		void WhereIn(string columnName, string inQuery);

		void WhereNotIn(string columnName, string inQuery);

		void WhereIsNull(string columnName);

		void WhereIsNotNull(string columnName);

		void WhereBetween(string columnName, string min, string max);
	}
}
