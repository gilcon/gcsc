﻿namespace GCSC.Interface.Data.DML
{
	public interface IInsert
	{
		void Fields(params string[] columns);

		void Values(params object[] values);

		void ModifyValues(params object[] values);

		int ExecuteWithModifiedValue(params object[] values);

		string GenerateQueryString();

		int Execute();

		int Execute(string connectionString);

		string Table { set; }

		string Schema { set; }

		bool IsFullTable { get; set; }

		bool PreparedStatement { get; }
	}
}
