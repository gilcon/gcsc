﻿using System.Collections.Generic;

namespace GCSC.Interface.Data.DML
{
	public interface IDelete
	{
		void Dispose(bool isDisposing);

		void AddParamWithValue(string paramName, object value);

		void ModifyParamWithValue(string paramName, object value);

		int ExecuteWithModifiedValue(Dictionary<string, object> parameterNewValue);

		string GenerateQueryString(bool rebuildQueryString = false);

		int Execute();

		int Execute(string connectionString);

		string Table { set; }

		string Schema { set; }

		bool PreparedStatement { get; }
	}
}
