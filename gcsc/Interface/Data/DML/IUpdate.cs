﻿namespace GCSC.Interface.Data.DML
{
	public interface IUpdate
	{
		void Fields(params string[] columns);

		void Values(params object[] values);

		void AddParamWithValue(string paramName, object value);

		void ModifyValues(params object[] values);

		int ExecuteWithModifiedValue(params object[] values);

		string GenerateQueryString();

		int Execute();

		int Execute(string connectionString);

		string Table { set; }

		string Schema { set; }

		bool PreparedStatement { get; }
	}
}
