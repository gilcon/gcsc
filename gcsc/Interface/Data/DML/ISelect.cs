﻿namespace GCSC.Interface.Data
{
	public interface ISelect
	{
		void Clear();

		void RefreshReader();

		void RefreshScalar();

		void AddParamWithValue(string paramName, object value);

		object Scalar();

		object[][] Reader();

		string Command { get; set; }

		int FielCount { get; }

		int RowCount { get; }

		string Schema { get; set; }

		bool TableDirect { get; set; }

		bool PreparedStatement { get; set; }
	}
}
