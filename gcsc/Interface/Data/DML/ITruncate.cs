﻿namespace GCSC.Interface.Data.DML
{
	public interface ITruncate
	{
		void Dispose(bool isDisposing);

		int Execute();

		int Execute(string connectionString);

		string Table { set; }

		string Schema { set; }
	}
}
