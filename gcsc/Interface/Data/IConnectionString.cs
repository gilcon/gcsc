using System.Configuration;

namespace GCSC.Interface.Data
{
	public interface IConnectionString
	{
		void RegisterConnectionString(string connectionName);

		void InitConnectionString(string defaultConnectionStringName, ref bool connectionNameStatus);

		bool CheckConnectionString(string defaultConnectionStringName);

		void RemoveAll();

		void Remove(string connectionName);

		void InitDefaultSchema(string schemaName);

		ConfigurationUserLevel ConfigurationUserLevel { get; set; }
	}
}
