﻿namespace GCSC.Interface
{
	public interface IEditableObject
	{
		void InitObject();

		bool IsNew();

		void IsNew(bool status);

		bool IsDirty();

		void IsDirty(bool status);
	}
}
