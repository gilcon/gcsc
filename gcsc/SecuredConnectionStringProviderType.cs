﻿using System;

namespace GCSC
{
	[Flags]
	public enum SecuredConnectionStringProviderType
	{
		Default = 0,
		DefaultProviderConnectionStrings = Default,
		ProtectedConnectionStringCollection = 1
	}
}
