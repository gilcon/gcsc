using System;
using System.IO;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Text;

namespace GCSC.Util
{
	public sealed class FileHelper
	{
		public class Attribute
		{
			/// <summary>
			/// Add attribute to a File
			/// </summary>
			/// <param name="file">FileInfo: File</param>
			/// <param name="attribute">params FileAttributes: Attribute</param>
			public static void AddFileAttribute(FileInfo file, params FileAttributes[] attribute)
			{
				Array.ForEach(attribute, a =>
				{
					if ((file.Attributes & a) != a)
						file.Attributes = file.Attributes | a;
				});
			}

			/// <summary>
			/// Add attribute to a Directory
			/// </summary>
			/// <param name="dir">DirectoryInfo: Dir</param>
			/// <param name="attribute">params FileAttribute: Attribute</param>
			public static void AddDirAttribute(DirectoryInfo dir, params FileAttributes[] attribute)
			{
				Array.ForEach(attribute, a =>
				{
					if ((dir.Attributes & a) != a)
						dir.Attributes = dir.Attributes | a;
				});
			}

			/// <summary>
			/// Remove File Attributes
			/// </summary>
			/// <param name="file">FileInfo: File</param>
			/// <param name="attribute">params FileAttribute Attribute</param>
			public static void RemoveFileAttribute(FileInfo file, params FileAttributes[] attribute)
			{
				Array.ForEach(attribute, a =>
				{
					if ((file.Attributes & a) == a)
						file.Attributes = file.Attributes & ~a;
				});
			}

			/// <summary>
			/// Remove Directory Attributes
			/// </summary>
			/// <param name="dir">DirectoryInfo dir</param>
			/// <param name="attribute">params FileAttributes attribute</param>
			public static void RemoveDirAttribute(DirectoryInfo dir, params FileAttributes[] attribute)
			{
				Array.ForEach(attribute, a =>
				{
					if ((dir.Attributes & a) == a)
						dir.Attributes = dir.Attributes & ~a;
				});
			}

			/// <summary>
			/// Copy directory and its content to new location
			/// </summary>
			/// <param name="source">DirectoryInfo source</param>
			/// <param name="destination">DirectoryInfo destination</param>
			public static void CopyDirectory(DirectoryInfo source, DirectoryInfo destination)
			{
				if (!destination.Exists)
					destination.Create();

				Array.ForEach(source.GetFiles(), file => file.CopyTo(Path.Combine(destination.FullName, file.Name)));

				Array.ForEach(source.GetDirectories(), dir =>
				{
					string destinitionDir = Path.Combine(destination.FullName, dir.Name);
					CopyDirectory(dir, new DirectoryInfo(destinitionDir));
				});
			}

			/// <summary>
			/// Calculate the directory size
			/// </summary>
			/// <param name="directory">DirectoryInfo directory</param>
			/// <param name="includeSubdirectory">bool include subdirectory</param>
			/// <returns></returns>
			public static long CalculateDirectorySize(DirectoryInfo directory, bool includeSubdirectory)
			{
				long TotalSize = 0;

				Array.ForEach(directory.GetFiles(), file => TotalSize += file.Length);

				if (includeSubdirectory)
					Array.ForEach(directory.GetDirectories(), dir => TotalSize += CalculateDirectorySize(dir, true));

				return TotalSize;
			}

			/// <summary>
			/// Compate two files
			/// </summary>
			/// <param name="files">param string files</param>
			/// <returns>bool: true if they are thesame</returns>
			public static bool FileCompare(params string[] files)
			{
				if (files.Length != 2)
					throw new ArgumentOutOfRangeException("You need 2(two) files to compare");

				using (var hashAlg = HashAlgorithm.Create())
				{
					using (FileStream fsA = new FileStream(files[0], FileMode.Open), fsB = new FileStream(files[1], FileMode.Open))
					{
						if (BitConverter.ToString(hashAlg.ComputeHash(fsA)) ==
							BitConverter.ToString(hashAlg.ComputeHash(fsB)))
							return true;
						return false;
					}
				}
			}

			/// <summary>
			/// Create hash code of the file
			/// </summary>
			/// <param name="algorithm">string Algorithm to be used</param>
			/// <param name="file">string filename</param>
			/// <returns>string hashed code</returns>
			public static string FileHashCode(string algorithm, string file)
			{
				using (var hashAlg = HashAlgorithm.Create(algorithm))
				{
					using (Stream File = new FileStream(file, FileMode.Open, FileAccess.Read))
						return BitConverter.ToString(hashAlg.ComputeHash(File));
				}
			}

			/// <summary>
			/// Create hash code of a file with a key
			/// </summary>
			/// <param name="file">string filename</param>
			/// <param name="algorithm">string algorithm to be used</param>
			/// <param name="key">string key</param>
			/// <returns>string hashed code</returns>
			public static string KeyedFileHash(string file, string algorithm, string key)
			{
				using (var hashAlg = KeyedHashAlgorithm.Create(algorithm))
				{
					hashAlg.Key = Encoding.Unicode.GetBytes(key);

					using (Stream File = new FileStream(file, FileMode.Open, FileAccess.Read))
						return BitConverter.ToString(hashAlg.ComputeHash(File));
				}
			}

			public static void ACLAddRule(string filePath, string account, FileSystemRights rights, AccessControlType controlType)
			{
				var fSecurity = File.GetAccessControl(filePath);

				fSecurity.AddAccessRule(new FileSystemAccessRule(account, rights, controlType));

				File.SetAccessControl(filePath, fSecurity);
			}

			public static void ACLSetRule(string filePath, string account, FileSystemRights rights, AccessControlType controlType)
			{
				var fSecurity = File.GetAccessControl(filePath);

				fSecurity.ResetAccessRule(new FileSystemAccessRule(account, rights, controlType));

				File.SetAccessControl(filePath, fSecurity);
			}
		}
	}
}
