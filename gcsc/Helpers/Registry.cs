﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;

namespace GCSC.Helpers
{
	public static class Registry
	{
		private static string RootKeyString(RootKey r)
		{
			string value;

			switch ((int)r)
			{
				case 0:
					value = "HKEY_CLASSES_ROOT";
					break;

				case 1:
					value = "HKEY_CURRENT_CONFIG";
					break;

				case 2:
					value = "HKEY_CURRENT_USER";
					break;

				case 3:
					value = "HKEY_DYN_DATA";
					break;

				case 4:
					value = "HKEY_LOCAL_MACHINE";
					break;

				case 5:
					value = "HKEY_PERFORMANCE_DATA";
					break;

				case 6:
					value = "HKEY_USERS";
					break;

				default:
					value = "HKEY_CLASSES_ROOT";
					break;
			}

			return value;
		}

		public static void AddRegistry(RootKey r, string location, string valueName, object value, RegistryValueKind kind)
		{
			Microsoft.Win32.Registry.SetValue(String.Format(@"{0}\{1}", RootKeyString(r), location), valueName, value, kind);
		}

		public static RegistryKey[] GetRegistry(RegistryKey root, string searchKey)
		{
			var rc = new List<RegistryKey>();

			Array.ForEach(root.GetSubKeyNames(), keyName =>
			{
				using (var key = root.OpenSubKey(keyName))
				{
					if (keyName == searchKey)
						rc.Add(key);
					GetRegistry(key, searchKey);
				}
			});

			return rc.ToArray();
		}
	}
}
