﻿using Barcode.Model;
using System;

namespace GCSC.Devices.Barcode
{
    /// <summary>
    /// Contains information about a barcode that was scanned.
    /// </summary>
    public class BarcodeScannedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the BarcodeScannedEventArgs class.
        /// </summary>
        /// <param name="barcode">the barcode that was scanned</param>
        /// <param name="deviceInfo">information about the device that sent the
        /// barcode</param>
        /// <exception cref="ArgumentNullException">if the barcode or deviceInfo are
        /// null</exception>
        public BarcodeScannedEventArgs(string barcode, BarcodeScannerDeviceInfo deviceInfo)
        {
            if (barcode == null)
                throw new ArgumentNullException("barcode");

            if (deviceInfo == null)
                throw new ArgumentNullException("deviceInfo");

            Barcode = barcode;
            DeviceInfo = deviceInfo;
        }

        /// <summary>
        /// Gets the barcode that was scanned.
        /// </summary>
        public string Barcode { get; private set; }

        /// <summary>
        /// Gets information about the device that sent the barcode.
        /// </summary>
        public BarcodeScannerDeviceInfo DeviceInfo { get; private set; }
    }
}