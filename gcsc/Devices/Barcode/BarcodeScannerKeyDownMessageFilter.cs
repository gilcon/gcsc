﻿using System.Security.Permissions;
using System.Windows.Forms;

namespace GCSC.Devices.Barcode
{
    /// <summary>
    /// Filters WM_KEYDOWN messages.
    /// </summary>
    public class BarcodeScannerKeyDownMessageFilter : IMessageFilter
    {
        /// <summary>
        /// Gets or sets a value indicating whether or not the next keydown message
        /// should be filtered.
        /// </summary>
        public bool FilterNext { get; set; }

        /// <summary>
        /// Filters out a message before it is dispatched.
        /// </summary>
        /// <param name="m">The message to be dispatched. You cannot modify
        /// this message.</param>
        /// <returns>
        /// true to filter the message and stop it from being dispatched; false
        /// to allow the message to continue to the next filter or control.
        /// </returns>
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public bool PreFilterMessage(ref Message m)
        {
            bool filter = false;

            if (FilterNext && m.Msg == NativeMethods.WM_KEYDOWN)
            {
                filter = true;
                FilterNext = false;
            }

            return filter;
        }
    }
}