﻿using System.Configuration;

namespace GCSC.Devices.Barcode
{
    internal class BarcodeScannerListenerConfigurationElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the hardware ID.
        /// </summary>
        [ConfigurationProperty("id", IsRequired = true, IsKey = true)]
        public string Id
        {
            get { return (string)this["id"]; }
            set { this["id"] = value; }
        }
    }
}