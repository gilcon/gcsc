﻿namespace GCSC
{
	public enum WindowsOSType
	{
		Windows2000 = 500,
		WindowsXP = 510,
		WindowsXP64 = 520,
		WindowsServer2003 = 520,
		WindowsServer2003R2 = 520,
		WindowsVista = 600,
		WindowsServer2008 = 600,
		WindowsServer2008R2 = 610,
		Windows7 = 610,
		WindowsServer2012 = 620,
		Windows8 = 620,
		WindowsServer2012R2 = 630,
		Windows8_1 = 630,
		WindowsServer2016TP = 1000,
		Windows10 = 10000
	}
}
