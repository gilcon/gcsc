﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GCSC.UI
{
	public class EditableListRaiseEventArgs : EventArgs
	{
		public EditableListRaiseEventArgs(Interface.IEditableObject item)
		{
			Item = item;
			IsEnumarable = false;
		}

		public EditableListRaiseEventArgs(IEnumerable<Interface.IEditableObject> items)
		{
			Items = items;
			Item = Items.LastOrDefault();
			IsEnumarable = true;
		}

		public Interface.IEditableObject Item { get; private set; }

		public IEnumerable<Interface.IEditableObject> Items { get; private set; }

		public bool IsEnumarable { get; private set; }
	}
}
