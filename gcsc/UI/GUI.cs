﻿using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Calendar;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Popup;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using GCSC.Data.MySQL.DML;
using GCSC.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace GCSC.UI
{
	public sealed class GUI
	{
		/// <summary>
		/// Create hit info
		/// </summary>
		/// <param name="sender">object as sender</param>
		/// <param name="e">GridMenuEventArgs</param>
		/// <param name="menuItems">The items to display</param>
		/// <param name="onCLick">the function to be called</param>
		public static void PopupMenuX(GridView sender, PopupMenuShowingEventArgs e, List<string> menuItems,
			params EventHandler[] onCLick)
		{
			if (onCLick.Length < 1 || menuItems.Count != onCLick.Length)
				throw new ArgumentOutOfRangeException("menuItems or onClick");

			var c = new ContextMenu();

			var HitInfo = sender.CalcHitInfo(e.Point);

			if (!HitInfo.InRow) return;

			sender.FocusedRowHandle = HitInfo.RowHandle;

			for (int i = 0; i < onCLick.Length; i++)
				c.MenuItems.Add(menuItems[i], onCLick[i]);

			c.Show(sender.GridControl, e.Point);
		}

		/// <summary>
		/// Show Child Form
		/// </summary>
		/// <param name="parentForm">Parent Form</param>
		/// <param name="childForm">Child Form</param>
		public static void ShowFormChild(Form parentForm, Form childForm,
			FormStartPosition position = FormStartPosition.WindowsDefaultLocation)
		{
			childForm.MdiParent = parentForm;
			childForm.StartPosition = position;
			childForm.Show();
		}

		/// <summary>
		/// Get the row cell value
		/// </summary>
		/// <param name="GridView">GriedView</param>
		/// <param name="ColumnName">Coulumn Name</param>
		/// <returns>"String" Value</returns>
		public static Hashtable GetRowCellValueX(GridView GridView, params string[] ColumnName)
		{
			return GetRowCellValueX(GridView, GridView.FocusedRowHandle, false, ColumnName);
		}

		/// <summary>
		/// Get the row cell value
		/// </summary>
		/// <param name="GridView">GriedView</param>
		/// <param name="ColumnName">Coulumn Name</param>
		/// <returns>"String" Value</returns>
		public static Hashtable GetRowCellValueX(GridView GridView, bool isAssociative, params string[] ColumnName)
		{
			return GetRowCellValueX(GridView, GridView.FocusedRowHandle, isAssociative, ColumnName);
		}

		/// <summary>
		/// Get the row cell value
		/// </summary>
		/// <param name="GridView">GriedView</param>
		/// <param name="ColumnName">Coulumn Name</param>
		/// <returns>"String" Value</returns>
		public static Hashtable GetRowCellValueX(GridView GridView, int rowHandle, params string[] ColumnName)
		{
			return GetRowCellValueX(GridView, rowHandle, false, ColumnName);
		}

		/// <summary>
		/// Get the row cell value
		/// </summary>
		/// <param name="GridView">GriedView</param>
		/// <param name="isAssociative">Is Associative</param>
		/// <param name="ColumnName">Coulumn Name</param>
		/// <returns>"String" Value</returns>
		public static Hashtable GetRowCellValueX(GridView GridView, int rowHandle, bool isAssociative, params string[] ColumnName)
		{
			if (ColumnName.Length < 1)
				throw new ArgumentNullException("ColumnName");

			var h = new Hashtable(ColumnName.Length);

			if (!isAssociative)
				for (int i = 0; i < ColumnName.Length; i++)
					h[i] = GridView.GetRowCellValue(rowHandle, ColumnName[i]);
			else
				Array.ForEach(ColumnName, s => h[h.ContainsKey(s) ? "C_" + s : s] = GridView.GetRowCellValue(rowHandle, s));

			return h;
		}

		/// <summary>
		/// Populate the MRUEdit
		/// </summary>
		/// <param name="MRUEdit">MRUEdit</param>
		/// <param name="Command">Command</param>
		public static void MRUFiller(MRUEdit MRUEdit, string Command)
		{
			MRUFiller(MRUEdit, Command);
		}

		/// <summary>
		/// Populate the MRUEdit
		/// </summary>
		/// <param name="MRUEdit">MRUEDit</param>
		/// <param name="Command">Command</param>
		/// <param name="ConnectionString">ConnectionString</param>
		public static void MRUFiller(MRUEdit MRUEdit, string Command, string ConnectionString = "", Dictionary<string, object> parameters = null)
		{
			MRUEdit.Properties.Items.Clear();
			var s = new MySQLSelect(Command, ConnectionString);
			if (parameters != null)
				foreach (var kv in parameters)
					s.AddParamWithValue(kv.Key, kv.Value);

			Array.ForEach(s.Reader(), r => MRUEdit.Properties.Items.Add(r[0]));
		}

		/// <summary>
		/// Populate the MRUEdit
		/// </summary>
		/// <param name="MRUEdit">MRUEdit</param>
		/// <param name="value">Param array value</param>
		public static void MRUFiller(MRUEdit MRUEdit, params string[] value)
		{
			MRUEdit.Properties.Items.Clear();
			Array.ForEach(value, s => MRUEdit.Properties.Items.Add(s));
		}

		/// <summary>
		/// Populage the MRUControll
		/// </summary>
		/// <param name="MRUEdit">MRUEdit</param>
		/// <param name="value">collection value</param>
		///
		[Obsolete]
		public static void MRUFiller(MRUEdit MRUEdit, Collections<string> value)
		{
			MRUEdit.Properties.Items.Clear();
			foreach (string s in value)
				MRUEdit.Properties.Items.Add(s);
			value.Dispose();
		}

		public static void MRUFiller(MRUEdit MRUEdit, List<string> value)
		{
			MRUEdit.Properties.Items.Clear();
			value.ForEach(s => MRUEdit.Properties.Items.Add(s));
		}

		//[ToolboxItem(true)]
		//public class MonthDateEditX : DateEdit
		//{
		//	protected override PopupBaseForm CreatePopupForm()
		//	{
		//		if (Properties.VistaDisplayMode == DefaultBoolean.True)
		//			return (PopupBaseForm)new MyVistaPopupDateEditForm((DateEdit)this);
		//		Properties.VistaDisplayMode = DefaultBoolean.True;
		//		return (PopupBaseForm)new MyVistaPopupDateEditForm((DateEdit)this);
		//	}
		//}

		//private class MyVistaPopupDateEditForm : VistaPopupDateEditForm
		//{
		//	public MyVistaPopupDateEditForm(DateEdit ownerEdit)
		//		: base(ownerEdit)
		//	{
		//	}

		//	protected override DateEditCalendar CreateCalendar()
		//	{
		//		return (DateEditCalendar)new MyVistaDateEditCalendar(OwnerEdit.Properties,
		//		RuntimeHelpers.GetObjectValue(OwnerEdit.EditValue));
		//	}
		//}

		//private class MyVistaDateEditCalendar : VistaDateEditCalendar
		//{
		//	public MyVistaDateEditCalendar(RepositoryItemDateEdit item, object editDate)
		//		: base(item, editDate)
		//	{
		//	}

		//	protected override void Init()
		//	{
		//		base.Init();
		//		View = DateEditCalendarViewType.YearInfo;
		//	}

		//	protected override void OnItemClick(CalendarHitInfo hitInfo)
		//	{
		//		var dayNumberCellInfo = hitInfo.HitObject as DayNumberCellInfo;
		//		if (View == DateEditCalendarViewType.YearInfo)
		//			OnDateTimeCommit((object)new DateTime(DateTime.Year, dayNumberCellInfo.Date.Month,
		//			CorrectDay(DateTime.Year, dayNumberCellInfo.Date.Month, DateTime.Day), DateTime.Hour,
		//			DateTime.Minute, DateTime.Second), false);
		//		else
		//			base.OnItemClick(hitInfo);
		//	}
		//}
	}
}
