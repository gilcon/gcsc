﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace GCSC.UI.Controls
{
	public sealed class GridViewX
	{
		private bool _isIlistUsed;
		private readonly GridControl _gridControl;
		private readonly GridView _gridView;
		private readonly IBindingList _bindingList;
		private readonly IList _ilist;
		private Dictionary<string, GridControl> _sharedGridControl;
		private ContextMenu _contextMenu;
		private bool _contextMenuStatus, _gridSharing;

		public GridViewX(GridView gridView, IBindingList bindingList)
			: this(gridView.GridControl, gridView, bindingList)
		{ }

		public GridViewX(GridControl gridControl, GridView gridView, IBindingList bindingList)
		{
			_gridControl = gridControl;
			_gridView = gridView;
			_bindingList = bindingList;

			_gridControl.DataSource = _bindingList;
		}

		public GridViewX(GridView gridView, IList ilist)
			: this(gridView.GridControl, gridView, ilist)
		{ }

		public GridViewX(GridControl gridControl, GridView gridView, IList ilist)
		{
			_gridControl = gridControl;
			_gridView = gridView;
			_ilist = ilist;

			_gridControl.DataSource = ilist;
			_isIlistUsed = true;
		}

		private void _gridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
		{
			var view = (GridView)sender;
			var hitInfo = view.CalcHitInfo(e.Point);

			if (!hitInfo.InRow) return;
			if (hitInfo.RowHandle < 0) return;

			view.FocusedRowHandle = hitInfo.RowHandle;
			_contextMenu.Show(view.GridControl, e.Point);
		}

		public void AddPopupMenuShowing(string menuName, EventHandler onClick)
		{
			if (!_contextMenuStatus) throw new NotSupportedException("Context menu is disabled");

			_contextMenu.MenuItems.Add(menuName, onClick);
		}

		public void AddPopupMenuShowing(string parent, string menuName, EventHandler onClick)
		{
			if (!_contextMenuStatus) throw new NotSupportedException("Context menu is disabled");
			if (!_contextMenu.MenuItems.ContainsKey(parent))
				_contextMenu.MenuItems.Add(parent, new MenuItem[] { });
			_contextMenu.MenuItems[parent].MenuItems.Add(menuName, onClick);
		}

		public void ArrangeColumn(params int[] indexes)
		{
			for (int i = 0; i < _gridView.Columns.Count - 1; i++)
			{
				_gridView.Columns[i].VisibleIndex = indexes[i];
			}
		}

		public void BestFit(params int[] columns)
		{
			int m = columns.Max();
			if (m >= _gridView.Columns.Count) throw new ArgumentOutOfRangeException("index");
			Array.ForEach(columns, f => _gridView.Columns[f].BestFit());
		}

		public void BestFit()
		{
			_gridView.BestFitColumns();
		}

		public void DisposeContextMenu()
		{
			_contextMenu.Dispose();
		}

		public void ClearContextMenu()
		{
			_contextMenu.MenuItems.Clear();
		}

		public void HideColumn(params int[] index)
		{
			if (_gridView.Columns.Count - 1 < index.Max()) throw new ArgumentOutOfRangeException("index");
			foreach (int i in index)
				_gridView.Columns[i].Visible = false;
		}

		public void ChangesCaption(string caption, int index)
		{
			if (_gridView.Columns.Count - 1 < index) throw new ArgumentOutOfRangeException("index");

			_gridView.Columns[index].Caption = caption;
		}

		public void SharedDataSource(string alias, GridControl gridControl)
		{
			if (!_gridSharing) throw new NotSupportedException("Datasource sharing is not allowed.");
			_sharedGridControl.Add(alias, gridControl);
			_sharedGridControl[alias].DataSource = _isIlistUsed ? _ilist : _bindingList;
		}

		public GridControl SharedGridControl(string alias)
		{
			return _sharedGridControl[alias];
		}

		public Dictionary<string, GridControl> SharedGridControlsWithAlias()
		{
			return _sharedGridControl;
		}

		public GridControl[] SharedGridControls()
		{
			return _sharedGridControl.Values.ToArray();
		}

		public bool GetRowCellValue(out Hashtable retValue, params string[] ColumnNmae)
		{
			return GetRowCellValue(_gridView.FocusedRowHandle, false, out retValue, ColumnNmae);
		}

		public bool GetRowCellValue(bool isAssociative, out Hashtable retValue, params string[] ColumnName)
		{
			return GetRowCellValue(_gridView.FocusedRowHandle, isAssociative, out retValue, ColumnName);
		}

		public bool GetRowCellValue(int rowHandle, out Hashtable retValue, params string[] ColumnName)
		{
			return GetRowCellValue(rowHandle, false, out retValue, ColumnName);
		}

		public bool GetRowCellValue(int rowHandle, bool isAssociative, out Hashtable retValue, params string[] ColumnName)
		{
			if (ColumnName.Length < 1)
				throw new IndexOutOfRangeException("ColumnName must not be less than 1");
			retValue = new Hashtable(ColumnName.Length);
			if (rowHandle < 0) return false;
			if (!_gridView.IsValidRowHandle(rowHandle)) return false;
			if (!isAssociative)
			{
				for (int i = 0; i < ColumnName.Length; i++)
					retValue[i] = _gridView.GetRowCellValue(rowHandle, ColumnName[i]);
			}
			else
			{
				foreach (var s in ColumnName)
					retValue[retValue.ContainsKey(s) ? "C_" + s : s] = _gridView.GetRowCellValue(rowHandle, s);
			}
			return retValue.Count > 0 ? true : false;
		}

		public bool EnableGridSharing
		{
			get { return _gridSharing; }
			set
			{
				if (!_gridSharing && value)
					_sharedGridControl = new Dictionary<string, GridControl>();
				else if (_gridSharing && !value)
					_sharedGridControl = null;

				_gridSharing = value;
			}
		}

		public bool EnableContextMenu
		{
			get { return _contextMenuStatus; }
			set
			{
				if (value)
				{
					if (!_contextMenuStatus)
					{
						_gridView.PopupMenuShowing += _gridView_PopupMenuShowing;
						_contextMenu = new ContextMenu();
					}
					_contextMenuStatus = value;
				}
				else
				{
					_gridView.PopupMenuShowing -= _gridView_PopupMenuShowing;
					_contextMenu = null;
					_contextMenuStatus = value;
				}
			}
		}

		public GridControl MainGridControl { get { return _gridControl; } }

		public GridView MainGridView { get { return _gridView; } }
	}
}
