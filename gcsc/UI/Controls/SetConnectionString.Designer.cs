﻿namespace GCSC.UI.Controls
{
    partial class SetConnectionString
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.combo_compression = new DevExpress.XtraEditors.ComboBoxEdit();
			this.txt_connectionName = new DevExpress.XtraEditors.TextEdit();
			this.btn_close = new DevExpress.XtraEditors.SimpleButton();
			this.btn_connect = new DevExpress.XtraEditors.SimpleButton();
			this.btn_test = new DevExpress.XtraEditors.SimpleButton();
			this.txt_password = new DevExpress.XtraEditors.TextEdit();
			this.txt_user = new DevExpress.XtraEditors.TextEdit();
			this.txt_server = new DevExpress.XtraEditors.TextEdit();
			this.txt_database = new DevExpress.XtraEditors.TextEdit();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.combo_compression.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt_connectionName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt_password.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt_user.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt_server.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt_database.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			this.SuspendLayout();
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.combo_compression);
			this.layoutControl1.Controls.Add(this.txt_connectionName);
			this.layoutControl1.Controls.Add(this.btn_close);
			this.layoutControl1.Controls.Add(this.btn_connect);
			this.layoutControl1.Controls.Add(this.btn_test);
			this.layoutControl1.Controls.Add(this.txt_password);
			this.layoutControl1.Controls.Add(this.txt_user);
			this.layoutControl1.Controls.Add(this.txt_server);
			this.layoutControl1.Controls.Add(this.txt_database);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(759, 277, 250, 350);
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(515, 321);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// combo_compression
			// 
			this.combo_compression.EditValue = "False";
			this.combo_compression.Location = new System.Drawing.Point(193, 192);
			this.combo_compression.Name = "combo_compression";
			this.combo_compression.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F);
			this.combo_compression.Properties.Appearance.Options.UseFont = true;
			this.combo_compression.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.combo_compression.Properties.Items.AddRange(new object[] {
            "True",
            "False"});
			this.combo_compression.Size = new System.Drawing.Size(310, 32);
			this.combo_compression.StyleController = this.layoutControl1;
			this.combo_compression.TabIndex = 12;
			// 
			// txt_connectionName
			// 
			this.txt_connectionName.Location = new System.Drawing.Point(193, 156);
			this.txt_connectionName.Name = "txt_connectionName";
			this.txt_connectionName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F);
			this.txt_connectionName.Properties.Appearance.Options.UseFont = true;
			this.txt_connectionName.Size = new System.Drawing.Size(310, 32);
			this.txt_connectionName.StyleController = this.layoutControl1;
			this.txt_connectionName.TabIndex = 11;
			// 
			// btn_close
			// 
			this.btn_close.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_close.Appearance.Options.UseFont = true;
			this.btn_close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btn_close.Location = new System.Drawing.Point(345, 240);
			this.btn_close.Name = "btn_close";
			this.btn_close.Size = new System.Drawing.Size(146, 57);
			this.btn_close.StyleController = this.layoutControl1;
			this.btn_close.TabIndex = 10;
			this.btn_close.Text = "Close";
			this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
			// 
			// btn_connect
			// 
			this.btn_connect.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_connect.Appearance.Options.UseFont = true;
			this.btn_connect.Location = new System.Drawing.Point(184, 240);
			this.btn_connect.Name = "btn_connect";
			this.btn_connect.Size = new System.Drawing.Size(146, 57);
			this.btn_connect.StyleController = this.layoutControl1;
			this.btn_connect.TabIndex = 9;
			this.btn_connect.Text = "Connect";
			this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
			// 
			// btn_test
			// 
			this.btn_test.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_test.Appearance.Options.UseFont = true;
			this.btn_test.Location = new System.Drawing.Point(24, 240);
			this.btn_test.Name = "btn_test";
			this.btn_test.Size = new System.Drawing.Size(146, 57);
			this.btn_test.StyleController = this.layoutControl1;
			this.btn_test.TabIndex = 8;
			this.btn_test.Text = "Test";
			this.btn_test.Click += new System.EventHandler(this.btn_test_Click);
			// 
			// txt_password
			// 
			this.txt_password.Location = new System.Drawing.Point(193, 84);
			this.txt_password.Name = "txt_password";
			this.txt_password.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_password.Properties.Appearance.Options.UseFont = true;
			this.txt_password.Properties.PasswordChar = '*';
			this.txt_password.Size = new System.Drawing.Size(310, 32);
			this.txt_password.StyleController = this.layoutControl1;
			this.txt_password.TabIndex = 6;
			// 
			// txt_user
			// 
			this.txt_user.Location = new System.Drawing.Point(193, 48);
			this.txt_user.Name = "txt_user";
			this.txt_user.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_user.Properties.Appearance.Options.UseFont = true;
			this.txt_user.Size = new System.Drawing.Size(310, 32);
			this.txt_user.StyleController = this.layoutControl1;
			this.txt_user.TabIndex = 5;
			// 
			// txt_server
			// 
			this.txt_server.Location = new System.Drawing.Point(193, 12);
			this.txt_server.Name = "txt_server";
			this.txt_server.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_server.Properties.Appearance.Options.UseFont = true;
			this.txt_server.Size = new System.Drawing.Size(310, 32);
			this.txt_server.StyleController = this.layoutControl1;
			this.txt_server.TabIndex = 4;
			// 
			// txt_database
			// 
			this.txt_database.Location = new System.Drawing.Point(193, 120);
			this.txt_database.Name = "txt_database";
			this.txt_database.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_database.Properties.Appearance.Options.UseFont = true;
			this.txt_database.Size = new System.Drawing.Size(310, 32);
			this.txt_database.StyleController = this.layoutControl1;
			this.txt_database.TabIndex = 7;
			this.txt_database.EditValueChanged += new System.EventHandler(this.txt_database_EditValueChanged);
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.layoutControlGroup1.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlGroup1.CustomizationFormText = "Root";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlGroup2,
            this.layoutControlItem8,
            this.layoutControlItem9});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "Root";
			this.layoutControlGroup1.Size = new System.Drawing.Size(515, 321);
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.txt_server;
			this.layoutControlItem1.CustomizationFormText = "Server:";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(495, 36);
			this.layoutControlItem1.Text = "Server:";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(178, 23);
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.txt_user;
			this.layoutControlItem2.CustomizationFormText = "User ID:";
			this.layoutControlItem2.Location = new System.Drawing.Point(0, 36);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(495, 36);
			this.layoutControlItem2.Text = "User ID:";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(178, 23);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.txt_password;
			this.layoutControlItem3.CustomizationFormText = "Password:";
			this.layoutControlItem3.Location = new System.Drawing.Point(0, 72);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(495, 36);
			this.layoutControlItem3.Text = "Password:";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(178, 23);
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.txt_database;
			this.layoutControlItem4.CustomizationFormText = "Database:";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 108);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(495, 36);
			this.layoutControlItem4.Text = "Database:";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(178, 23);
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
			this.layoutControlGroup2.Location = new System.Drawing.Point(0, 216);
			this.layoutControlGroup2.Name = "layoutControlGroup2";
			this.layoutControlGroup2.Size = new System.Drawing.Size(495, 85);
			this.layoutControlGroup2.TextVisible = false;
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.btn_test;
			this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem5.MaxSize = new System.Drawing.Size(150, 0);
			this.layoutControlItem5.MinSize = new System.Drawing.Size(150, 36);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(150, 61);
			this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem5.TextVisible = false;
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.btn_connect;
			this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
			this.layoutControlItem6.Location = new System.Drawing.Point(160, 0);
			this.layoutControlItem6.MaxSize = new System.Drawing.Size(150, 0);
			this.layoutControlItem6.MinSize = new System.Drawing.Size(150, 36);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(150, 61);
			this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem6.TextVisible = false;
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.Control = this.btn_close;
			this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
			this.layoutControlItem7.Location = new System.Drawing.Point(321, 0);
			this.layoutControlItem7.MaxSize = new System.Drawing.Size(150, 0);
			this.layoutControlItem7.MinSize = new System.Drawing.Size(150, 36);
			this.layoutControlItem7.Name = "layoutControlItem7";
			this.layoutControlItem7.Size = new System.Drawing.Size(150, 61);
			this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem7.TextVisible = false;
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.AllowHotTrack = false;
			this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
			this.emptySpaceItem1.Location = new System.Drawing.Point(150, 0);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(10, 61);
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// emptySpaceItem2
			// 
			this.emptySpaceItem2.AllowHotTrack = false;
			this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
			this.emptySpaceItem2.Location = new System.Drawing.Point(310, 0);
			this.emptySpaceItem2.Name = "emptySpaceItem2";
			this.emptySpaceItem2.Size = new System.Drawing.Size(11, 61);
			this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.Control = this.txt_connectionName;
			this.layoutControlItem8.CustomizationFormText = "Connection Name:";
			this.layoutControlItem8.Location = new System.Drawing.Point(0, 144);
			this.layoutControlItem8.Name = "layoutControlItem8";
			this.layoutControlItem8.Size = new System.Drawing.Size(495, 36);
			this.layoutControlItem8.Text = "Connection Name:";
			this.layoutControlItem8.TextSize = new System.Drawing.Size(178, 23);
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.combo_compression;
			this.layoutControlItem9.CustomizationFormText = "Use Compression:";
			this.layoutControlItem9.Location = new System.Drawing.Point(0, 180);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Size = new System.Drawing.Size(495, 36);
			this.layoutControlItem9.Text = "Use Compression:";
			this.layoutControlItem9.TextSize = new System.Drawing.Size(178, 23);
			// 
			// SetConnectionString
			// 
			this.AcceptButton = this.btn_connect;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btn_close;
			this.ClientSize = new System.Drawing.Size(515, 321);
			this.ControlBox = false;
			this.Controls.Add(this.layoutControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "SetConnectionString";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "Set Connection String";
			this.TopMost = true;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetConnectionString_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.combo_compression.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt_connectionName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt_password.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt_user.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt_server.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt_database.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txt_password;
        private DevExpress.XtraEditors.TextEdit txt_user;
        private DevExpress.XtraEditors.TextEdit txt_server;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btn_close;
        private DevExpress.XtraEditors.SimpleButton btn_connect;
        private DevExpress.XtraEditors.SimpleButton btn_test;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.TextEdit txt_database;
        private DevExpress.XtraEditors.TextEdit txt_connectionName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.ComboBoxEdit combo_compression;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}