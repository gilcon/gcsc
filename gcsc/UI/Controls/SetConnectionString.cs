﻿using DevExpress.XtraEditors;
using GCSC.Data.MySQL;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace GCSC.UI.Controls
{
	public partial class SetConnectionString : XtraForm
	{
		private MySQLConnectionStringProperties connectionPropertiesOverride;

		private bool status;

		public SetConnectionString()
		{
			InitializeComponent();
			StartPosition = FormStartPosition.CenterScreen;
			connectionPropertiesOverride = new MySQLConnectionStringProperties();
		}

		public SetConnectionString(string defaultSchema)
		{
			InitializeComponent();
			StartPosition = FormStartPosition.CenterScreen;
			txt_database.Text = defaultSchema;
			connectionPropertiesOverride = new MySQLConnectionStringProperties();
		}

		public SetConnectionString(ref MySQLConnectionStringProperties connectionProperties)
		{
			InitializeComponent();
			StartPosition = FormStartPosition.CenterScreen;
			connectionPropertiesOverride = connectionProperties;
		}

		public SetConnectionString(ref MySQLConnectionStringProperties connectionProperties, string defaultSchema)
		{
			InitializeComponent();
			StartPosition = FormStartPosition.CenterScreen;
			connectionPropertiesOverride = connectionProperties;
			txt_database.Text = defaultSchema;
		}

		private void btn_close_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btn_test_Click(object sender, EventArgs e)
		{
			if (new Ping().Send(txt_server.Text.Trim()).Status != IPStatus.Success)
			{
				XtraMessageBox.Show(this, "The target machine is not reachable.");
				return;
			}

			using (var cn = new MySqlConnection(string.Format("server={0};User Id={1};password={2};Persist Security Info=True;database={3}",
																							   txt_server.Text, txt_user.Text, txt_password.Text, txt_database.Text)))
			{
				try
				{
					cn.Open();
					if (cn.State == ConnectionState.Open)
						XtraMessageBox.Show(this, "Connection Open", "Connection String Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				catch (MySqlException ex)
				{
					XtraMessageBox.Show(this, "Failed to connect:\n" + ex.Message, "Connection String Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void btn_connect_Click(object sender, EventArgs e)
		{
			if (txt_database.Text == "" || txt_database.Text.Length < 1)
			{
				XtraMessageBox.Show(this, "Database Name Required");
				return;
			}
			using (var cn = new MySqlConnection(string.Format("server={0};User Id={1};password={2};Persist Security Info=True;database={3}",
																							   txt_server.Text, txt_user.Text, txt_password.Text, txt_database.Text)))
			{
				try
				{
					cn.Open();
					if (cn.State == ConnectionState.Open)
					{
						connectionPropertiesOverride.Server = txt_server.Text;
						connectionPropertiesOverride.UserId = txt_user.Text;
						connectionPropertiesOverride.Password = txt_password.Text;
						connectionPropertiesOverride.Database = txt_database.Text;
						bool CONNECTION_STATUS = false;
						using (var mcn = new MySQLConnectionString(System.Configuration.ConfigurationUserLevel.None, ref connectionPropertiesOverride))
						{
							if (combo_compression.SelectedIndex == 0)
								mcn.UseCompression = true;

							CONNECTION_STATUS = mcn.CheckConnectionString(txt_connectionName.Text);
							if (!CONNECTION_STATUS)
								mcn.InitConnectionString(txt_connectionName.Text, ref CONNECTION_STATUS);

							if (!CONNECTION_STATUS)
							{
								mcn.RegisterConnectionString(txt_connectionName.Text);
								CONNECTION_STATUS = mcn.CheckConnectionString(txt_connectionName.Text);
							}

							mcn.InitDefaultSchema(txt_database.Text);

							XtraMessageBox.Show(this, "Successfully applied new connection setting", "Connection Status", MessageBoxButtons.OK, MessageBoxIcon.Information);

							Global.GLB_AppEvents = new Util.AppEvents("GCSC_CORE");
							Global.GLB_AppEvents.WriteToLog("Successfully Changed the connection", System.Diagnostics.EventLogEntryType.Information, CategoryType.WriteToFile, EventIDTytpe.Write);
							Global.GLB_AppEvents.CloseLog();

							status = true;
							Close();
						}
					}
				}
				catch (Exception ex)
				{
					Global.GLB_AppEvents = new Util.AppEvents("GCSC_CORE");
					Global.GLB_AppEvents.WriteToLog(ex.Message, System.Diagnostics.EventLogEntryType.Error, CategoryType.ReadFromDB, EventIDTytpe.ExceptionThrown);
					Global.GLB_AppEvents.CloseLog();

					XtraMessageBox.Show(this, "Failed to connect", "Connection String Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void SetConnectionString_FormClosing(object sender, FormClosingEventArgs e)
		{
			DialogResult = !status ? System.Windows.Forms.DialogResult.Cancel : System.Windows.Forms.DialogResult.OK;
		}

		private void txt_database_EditValueChanged(object sender, EventArgs e)
		{
			txt_connectionName.Text = txt_database.Text;
		}
	}
}
