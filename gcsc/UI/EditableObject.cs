﻿using System;
using System.ComponentModel;

namespace GCSC.UI
{
	public abstract class EditableObject : Interface.IEditableObject, INotifyPropertyChanged, ICloneable
	{
		private bool _isNew;
		private bool _isDirty;

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null) return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		public void InitObject()
		{
			_isNew = false;
			_isDirty = false;
		}

		public bool IsNew()
		{
			return _isNew;
		}

		public void IsNew(bool status)
		{
			_isNew = status;
		}

		public bool IsDirty()
		{
			return _isDirty;
		}

		public void IsDirty(bool status)
		{
			_isDirty = status;
		}

		protected T Set<T>(T oldValue, T newValue, string propertyName)
		{
			if (Equals(oldValue, newValue))
				return oldValue;

			NotifyPropertyChanged(propertyName);

			if (!_isNew)
				_isDirty = true;

			return newValue;
		}

		[Obsolete]
		public Interface.IEditableObject GetThis()
		{
			return this;
		}

		public object Clone()
		{
			return this.MemberwiseClone();
		}
	}
}
