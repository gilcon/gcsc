﻿using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace GCSC.MySQL
{
    public sealed class Query
    {
        public Query() : this(string.Empty) { }

        public Query(string queryString) : this(queryString, string.Empty) { }

        public Query(string queryString, string connectionString)
        {
            Command = queryString;
            ConnectionString = connectionString;
            Schema = string.Empty;
        }

        public void Clear()
        {
            Command = null;
            Schema = null;
            ConnectionString = null;
            CommandType = 0;
        }

        public int Execute()
        {
            return Execute(new Dictionary<string, object>());
        }

        public int Execute(bool clear)
        {
            return Execute(new Dictionary<string, object>(), clear);
        }

        public int Execute(Dictionary<string, object> parameters, bool clear = false)
        {
            using (var cn = new MySqlConnection())
            {
                if (ConnectionString.Length > 0)
                    cn.ConnectionString = ConnectionString;
                else
                    using (Securing_ConnectionString sc = new Securing_ConnectionString())
                        cn.ConnectionString = sc.Get_Secured_ConnectionString();

                using (var cmd = new MySqlCommand(Command, cn) { CommandType = CommandType })
                {
                    foreach (KeyValuePair<string, object> kv in parameters)
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);

                    cn.Open();
                    if (Schema.Length > 0) cn.ChangeDatabase(Schema);

                    if (clear)
                    {
                        Command = null;
                        Schema = null;
                        ConnectionString = null;
                        CommandType = 0;
                        parameters = null;
                    }

                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public int ExecuteScript(bool clear = false)
        {
            using (MySqlConnection cn = new MySqlConnection())
            {
                if (ConnectionString.Length > 0)
                    cn.ConnectionString = ConnectionString;
                else
                    using (Securing_ConnectionString sc = new Securing_ConnectionString())
                        cn.ConnectionString = sc.Get_Secured_ConnectionString();

                try
                {
                    MySqlScript ms = new MySqlScript(cn, Command);
                    ms.Error += ExecuteScriptError;
                    return ms.Execute();
                }
                catch (MySqlException ex)
                {
                    Global.GLB_AppEvents = new Util.AppEvents("GCSC_CORE");
                    Global.GLB_AppEvents.WriteToLog(ex.Message, System.Diagnostics.EventLogEntryType.Error, CategoryType.WriteToDB, EventIDTytpe.Write);
                    Global.GLB_AppEvents.CloseLog();
                }
                finally
                {
                    if (clear)
                    {
                        Command = null;
                        Schema = null;
                        ConnectionString = null;
                        CommandType = 0;
                    }
                }
                return -1;
            }
        }

        private void ExecuteScriptError(object sender, MySqlScriptErrorEventArgs args)
        {
            DevExpress.XtraEditors.XtraMessageBox.Show(args.Exception.ToString());
        }

        public string Command { get; set; }

        public string ConnectionString { get; set; }

        public string Schema { get; set; }

        public CommandType CommandType { get; set; }
    }
}