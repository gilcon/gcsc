using MySql.Data.MySqlClient;

namespace GCSC.MySQL
{
    [System.Flags]
    public enum DataType
    {
        Decimal = MySqlDbType.Decimal,
        Byte = MySqlDbType.Byte,
        Int16 = MySqlDbType.Int16,
        Int32 = MySqlDbType.Int24,
        Float = MySqlDbType.Float,
        Double = MySqlDbType.Double,
        Timestamp = MySqlDbType.Timestamp,
        Int64 = MySqlDbType.Int64,
        Int24 = MySqlDbType.Int24,
        Date = MySqlDbType.Date,
        Time = MySqlDbType.Time,
        DateTime = MySqlDbType.DateTime,
        Year = MySqlDbType.Year,
        Newdate = MySqlDbType.Newdate,
        VarString = MySqlDbType.VarString,
        Bit = MySqlDbType.Bit,
        NewDecimal = MySqlDbType.NewDecimal,
        Enum = MySqlDbType.Enum,
        Set = MySqlDbType.Set,
        TinyBlob = MySqlDbType.TinyBlob,
        MediumBlob = MySqlDbType.MediumBlob,
        LongBlob = MySqlDbType.LongBlob,
        Blob = MySqlDbType.Blob,
        VarChar = MySqlDbType.VarChar,
        String = MySqlDbType.String,
        Geometry = MySqlDbType.Geometry,
        UByte = MySqlDbType.UByte,
        UInt16 = MySqlDbType.UInt16,
        UInt32 = MySqlDbType.UInt24,
        UInt64 = MySqlDbType.UInt64,
        UInt24 = MySqlDbType.UInt24,
        Binary = MySqlDbType.Binary,
        VarBinary = MySqlDbType.VarBinary,
        TinyText = MySqlDbType.TinyText,
        MediumText = MySqlDbType.MediumText,
        LongText = MySqlDbType.LongText,
        Text = MySqlDbType.Text,
        Guid = MySqlDbType.Guid,
    }
}