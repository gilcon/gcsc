﻿using System;
using System.Collections.Generic;

namespace GCSC.MySQL.Schema
{
    public sealed class Schemata
    {
        public Schemata(string schemaName)
            : this(schemaName, false, false) { }

        public Schemata(string schemaName, bool isDefault)
            : this(schemaName, isDefault, false) { }

        public Schemata(string schemaName, bool isDefault, bool overwrite)
        {
            if (Global.GLB_SchemataList.ContainsKey(schemaName) && !overwrite) return;

            SchemaName = schemaName;
            Refresh();

            if (isDefault) Global.GLB_DefaultSchemata = schemaName;
            if (Global.GLB_SchemataList.ContainsKey(schemaName)) Global.GLB_SchemataList.Remove(schemaName);
            Global.GLB_SchemataList.Add(schemaName, this);
        }

        public void Refresh()
        {
            Tables = new Dictionary<string, Table>();
            Views = new Dictionary<string, Table>();
            TempTables = new Dictionary<string, Table>();
            StoredProcedures = new Dictionary<string, Routine>();
            Functions = new Dictionary<string, Routine>();

            GCSC.MySQL.DML.Select s = new MySQL.DML.Select("SELECT `SCHEMA_NAME` FROM `information_schema`.`SCHEMATA` WHERE `SCHEMA_NAME` = @schema_name");
            s.AddParamWithValue("schema_name", SchemaName);
            if (s.RowCount != 1) throw new ArgumentException("Database not found: " + SchemaName, "schemaName");
            s.Clear();

            Tables ts = new Tables(SchemaName);
            ts.GetTables.ForEach(t => Tables.Add(t.TableName.Trim().ToLower(), t));
            ts.GetViews.ForEach(t => Views.Add(t.TableName.Trim().ToLower(), t));

            Routines rt = new Routines(SchemaName);
            rt.GetProcedures.ForEach(r => StoredProcedures.Add(r.RoutineName.Trim().ToLower(), r));
            rt.GetFunctions.ForEach(r => Functions.Add(r.RoutineName.Trim().ToLower(), r));
            Routines = rt.RoutineLists;
            MaxParamCount = rt.MaxParamCount;
            MinParamCount = rt.MinParamCount;
        }

        public static Dictionary<string, Schemata> SchemaLists
        {
            get { return Global.GLB_SchemataList; }
        }

        public static Schemata GetSchema(string schemata)
        {
            return Global.GLB_SchemataList[schemata];
        }

        public Dictionary<string, Table> Tables { get; private set; }

        public Dictionary<string, Table> Views { get; private set; }

        public Dictionary<string, Table> TempTables { get; private set; }

        public Dictionary<string, Routine> StoredProcedures { get; private set; }

        public Dictionary<string, Routine> Functions { get; private set; }

        public List<Routine> Routines { get; private set; }

        public int MaxParamCount { get; private set; }

        public int MinParamCount { get; private set; }

        public string SchemaName { get; private set; }
    }
}