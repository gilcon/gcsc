﻿using System;
using GCSC.Util;

namespace GCSC.MySQL.Schema
{
    public class Parameter
    {
        public Parameter(object[] parameterParameters, string schemata)
        {
            SpecificSchema = schemata;
            SpecificName = parameterParameters[0].ToString();
            OrdinalPosition = parameterParameters[1].ToInt();

            string paramDirection = parameterParameters[2].ToString().ToLower().Trim();

            if (paramDirection == "inout")
                ParameterDirection = System.Data.ParameterDirection.InputOutput;
            else if (paramDirection == "out")
                ParameterDirection = System.Data.ParameterDirection.Output;
            else
                ParameterDirection = System.Data.ParameterDirection.Input;

            ParameterName = parameterParameters[3].ToString();
            DataType = parameterParameters[4].ToString();
            CharacterMaximumLength = parameterParameters[5].ToInt64();
            NumericPrecision = parameterParameters[6].ToInt64();
            NumericScale = parameterParameters[7].ToInt64();
            CollationName = parameterParameters[8].ToString();
        }

        public string SpecificSchema { get; internal set; }

        public string SpecificName { get; internal set; }

        public int OrdinalPosition { get; internal set; }

        public System.Data.ParameterDirection ParameterDirection { get; internal set; }

        public string ParameterName { get; internal set; }

        public string DataType { get; internal set; }

        public Int64 CharacterMaximumLength { get; internal set; }

        public Int64 NumericPrecision { get; internal set; }

        public Int64 NumericScale { get; internal set; }

        public string CollationName { get; internal set; }
    }
}