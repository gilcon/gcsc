﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GCSC.MySQL.Schema
{
    internal sealed class Columns
    {
        public Columns(string tableName, string schema)
        {
            ColumnList = new List<Column>();

            GCSC.MySQL.DML.Select s = new MySQL.DML.Select()
            {
                Command = "SELECT `TABLE_NAME`, `COLUMN_NAME`, `ORDINAL_POSITION`, `COLUMN_DEFAULT`, `IS_NULLABLE`, `DATA_TYPE`, `CHARACTER_MAXIMUM_LENGTH`, `NUMERIC_PRECISION`, `NUMERIC_SCALE`,`COLLATION_NAME` ,`COLUMN_KEY`, `EXTRA` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = @schema AND `TABLE_NAME` = @table_name GROUP BY `ORDINAL_POSITION` ASC"
            };
            s.AddParamWithValue("schema", schema);
            s.AddParamWithValue("table_name", tableName);

            Array.ForEach(s.Reader(), r => ColumnList.Add(new Column(r, schema)));
            s.Clear();
        }

        public void Add(Column column)
        {
            ColumnList.Add(column);
        }

        public Column GetColumn(string columnName)
        {
            return (Column)ColumnList.Where(column => column.ColumnName == columnName);
        }

        public List<Column> ColumnList { get; internal set; }
    }
}