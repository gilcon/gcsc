﻿using System;
using System.Collections.Generic;
using System.Linq;
using GCSC.MySQL.DML;

namespace GCSC.MySQL.Schema
{
    internal sealed class Tables
    {
        public Tables(string schema)
        {
            TableLists = new List<Table>();

            Select s = new Select("SELECT `TABLE_NAME`, `TABLE_TYPE`, `TABLE_COLLATION` FROM `information_schema`.`TABLES` WHERE `TABLE_SCHEMA` = @schema");
            s.AddParamWithValue("schema", schema);
            Array.ForEach(s.Reader(), r => TableLists.Add(new Table(r, schema)));
            s.Clear();
        }

        public void Add(Table table)
        {
            TableLists.Add(table);
        }

        public Table GetTable(string tableName)
        {
            return (Table)TableLists.Where(table => table.TableName == tableName && table.TableType == "BASE TABLE");
        }

        public Table GetView(string viewName)
        {
            return (Table)TableLists.Where(table => table.TableName == viewName && table.TableType == "VIEW");
        }

        public List<Table> GetTables
        {
            get { return new List<Table>(TableLists.Where(table => table.TableType == "BASE TABLE")); }
        }

        public List<Table> GetViews
        {
            get { return new List<Table>(TableLists.Where(table => table.TableType == "VIEW")); }
        }

        public List<string> TableListsString
        {
            get
            {
                List<string> l = new List<string>();

                foreach (Table s in TableLists.Where(table => table.TableType == "BASE TABLE"))
                    l.Add(s.TableName);
                return l;
            }
        }

        public List<string> ViewListString
        {
            get
            {
                List<string> l = new List<string>();

                foreach (Table s in TableLists.Where(table => table.TableType == "VIEW"))
                    l.Add(s.TableName);
                return l;
            }
        }

        public List<Table> TableLists { get; internal set; }
    }
}