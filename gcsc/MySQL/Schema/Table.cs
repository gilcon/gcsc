using System.Collections.Generic;

namespace GCSC.MySQL.Schema
{
    public class Table
    {
        public Table(object[] tableProperties, string schema)
        {
            Columns = new Dictionary<string, Column>();
            TableSchema = schema;
            TableName = tableProperties[0].ToString();
            TableType = tableProperties[1].ToString();
            TableCollation = tableProperties[2].ToString();

            new Columns(TableName, schema).ColumnList.ForEach(cn => Columns.Add(cn.ColumnName, cn));
        }

        public Dictionary<string, Column> Columns { get; internal set; }

        public string TableSchema { get; internal set; }

        public string TableName { get; internal set; }

        public string TableType { get; internal set; }

        public string TableCollation { get; internal set; }
    }
}