﻿using System;
using System.Collections.Generic;
using System.Text;
using GCSC.MySQL.DML;
using GCSC.Util;

namespace GCSC.MySQL.Schema
{
    public static class DML
    {
        public class ColumnFields
        {
            private readonly List<string> _fields;

            public ColumnFields()
            {
                _fields = new List<string>();
                IsFullTable = false;
            }

            public ColumnFields(IEnumerable<string> fields)
            {
                _fields = new List<string>(fields);
                IsFullTable = false;
            }

            public void Add(string fieldName)
            {
                _fields.Add(fieldName);
            }

            public void Add(string[] fieldNames)
            {
                Array.ForEach(fieldNames, Add);
            }

            public void Add(List<string> fieldNames)
            {
                fieldNames.ForEach(Add);
            }

            public void Add(IEnumerable<string> fieldNames)
            {
                foreach (string s in fieldNames) Add(s);
            }

            public void Clear()
            {
                _fields.Clear();
            }

            public int Count
            {
                get { return _fields.Count; }
            }

            public string[] Items
            {
                get { return _fields.ToArray(); }
            }

            public bool IsFullTable { get; set; }
        }

        public class Values
        {
            readonly MultiMapCollections<int?, object> v;
            int? counter;

            public Values()
            {
                v = new MultiMapCollections<int?, object>();
                counter = 0;
            }

            public Values(params object[] values)
            {
                v = new MultiMapCollections<int?, object>();
                counter = 0;
                Add(values);
            }

            public void Add(params object[] values)
            {
                v.Add(counter, values);
                counter++;
            }

            public int Count
            {
                get { return v.Count; }
            }

            public object[] GetValues
            {
                get { return v.ToArray(); }
            }

            public void Clear()
            {
                v.Dispose();
                counter = null;
            }
        }

        public static int Delete(string tableName, string condition)
        {
            return Delete(string.Empty, tableName, condition);
        }

        public static int Delete(Schemata schemaName, string tableName, string condition)
        {
            return Delete(schemaName.SchemaName, tableName, condition);
        }

        public static int Delete(string schemaName, string tableName, string condition)
        {
            using (Delete d = new Delete()
            {
                Schema = schemaName,
                Table = tableName,
                Condition = condition
            })
            {
                return d.Execute();
            }
        }

        public static int Insert(string tableName, params object[] values)
        {
            return Insert(tableName, false, values);
        }

        public static int Insert(string tableName, bool isFullTable, params object[] values)
        {
            using (Insert i = new Insert() { Table = tableName, IsFullTable = isFullTable })
            {
                i.Values(values);
                return i.Execute();
            }
        }

        public static int Insert(string tableName, Values values, bool isFullTable = false)
        {
            using (Insert i = new Insert() { Table = tableName, IsFullTable = isFullTable })
            {
                foreach (List<object> l in values.GetValues)
                    i.Values(l.ToArray());

                values.Clear();
                return i.Execute();
            }
        }

        public static int Insert(string tableName, ColumnFields columnNames, params object[] values)
        {
            return Insert(tableName, columnNames, new Values(values));
        }

        public static int Insert(string tableName, ColumnFields columnNames, Values values)
        {
            using (Insert i = new Insert() { Table = tableName, IsFullTable = columnNames.IsFullTable })
            {
                i.Fields(columnNames.Items);
                foreach (List<object> o in values.GetValues)
                {
                    if (columnNames.Count != o.ToArray().Length) throw new IndexOutOfRangeException("Number of columns doesn't with the value");
                    i.Values(o.ToArray());
                }
                values.Clear();
                columnNames.Clear();
                return i.Execute();
            }
        }

        public static int Insert(string schemaName, string tableName, Values values)
        {
            return Insert(Global.GLB_SchemataList[schemaName], tableName, new ColumnFields(), values);
        }

        public static int Insert(string schemaName, string tableName, ColumnFields columnName, params object[] values)
        {
            return Insert(Global.GLB_SchemataList[schemaName], tableName, columnName, new Values(values));
        }

        public static int Insert(string schemaName, string tableName, ColumnFields columnName, Values values)
        {
            return Insert(Global.GLB_SchemataList[schemaName], tableName, columnName, values);
        }

        public static int Insert(Schemata schemaName, string tableName, ColumnFields columnNames, params object[] values)
        {
            return Insert(schemaName, tableName, columnNames, new Values(values));
        }

        public static int Insert(Schemata schemaName, string tableName, ColumnFields columnNames, Values values)
        {
            if (values.Count < 1) throw new ArgumentNullException("There is no value to insert");

            using (Insert i = new Insert() { Schema = schemaName.SchemaName, Table = tableName, IsFullTable = columnNames.IsFullTable })
            {
                i.Fields(columnNames.Items);
                foreach (List<object> o in values.GetValues)
                {
                    if (columnNames.Count != o.ToArray().Length) throw new IndexOutOfRangeException("Number of columns doesn't with the value");
                    i.Values(o.ToArray());
                }
                values.Clear();
                columnNames.Clear();
                return i.Execute();
            }
        }

        public static int StoredProcedure(string sprocName, params object[] values)
        {
            return StoredProcedure(Global.GLB_SchemataList[Global.GLB_DefaultSchemata], sprocName, values);
        }

        public static int StoredProcedure(Schemata schemaName, string sprocName, params object[] values)
        {
            string schema;
            if (schemaName.SchemaName == Global.GLB_SchemataList[Global.GLB_DefaultSchemata].SchemaName)
                schema = string.Empty;
            else
                schema = schemaName.SchemaName;

            using (StoredProcedure sp = new StoredProcedure() { Schema = schema, ProcedureName = sprocName })
            {
                schema = null;
                Array.ForEach(values, sp.Add);
                return sp.Execute();
            }
        }

        public static int Truncate(string tableName)
        {
            return Truncate(string.Empty, tableName);
        }

        public static int Truncate(Schemata schemaName, string tableName)
        {
            return Truncate(schemaName.SchemaName, tableName);
        }

        public static int Truncate(string schemaName, string tableName)
        {
            using (Truncate t = new Truncate() { Schema = schemaName, Table = tableName })
            {
                return t.Execute();
            }
        }

        public static int Update(string tableName, IEnumerable<string> columns, IEnumerable<object> values, string condition)
        {
            return (Update(string.Empty, tableName, columns, values, condition));
        }

        public static int Update(Schemata schemaName, string tableName, string condition, IEnumerable<string> columns, IEnumerable<object> values)
        {
            return Update(schemaName.SchemaName, tableName, columns, values, condition);
        }

        public static int Update(string schemaName, string tableName, IEnumerable<string> columns, IEnumerable<object> values, string condition)
        {
            using (Update u = new Update() { Schema = schemaName, Table = tableName, Condition = condition })
            {
                u.Fields(new List<string>(columns).ToArray());
                u.Values(new List<object>(values).ToArray());

                return u.Execute();
            }
        }

        public static int Update(string tableName, string condition, ColumnFields columns, Values values)
        {
            return Update(string.Empty, tableName, columns, values, condition);
        }

        public static int Update(Schemata schemaName, string tableName, string condition, ColumnFields columns, Values values)
        {
            return Update(schemaName.SchemaName, tableName, columns, values, condition);
        }

        public static int Update(string schemaName, string tableName, ColumnFields columns, Values values, string condition)
        {
            using (Update u = new Update() { Schema = schemaName, Table = tableName, Condition = condition })
            {
                u.Fields(columns.Items);
                foreach (List<object> l in values.GetValues)
                {
                    u.Values(l.ToArray());
                    break;
                }
                return u.Execute();
            }
        }

        public static void Select(string tableName, string condition = "")
        {
            Select(string.Empty, tableName, condition);
        }

        public static void Select(string schemaName, string tableName, string[] columns, string condition = "")
        {
            Select(schemaName, tableName, new ColumnFields(columns), condition);
        }

        public static void Select(Schemata schemaName, string tableName, string[] columns, string condition = "")
        {
            Select(schemaName.SchemaName, tableName, new ColumnFields(columns), condition);
        }

        public static void Select(Schemata schemaName, string tableName, string condition = "")
        {
            Select(schemaName.SchemaName, tableName, new ColumnFields(), condition);
        }

        public static void Select(string schemaName, string tableName, string condition = "")
        {
            Select(schemaName, tableName, new ColumnFields(), condition);
        }

        public static void Select(Schemata schemaName, string tableName, ColumnFields columns, string condition = "")
        {
            Select(schemaName.SchemaName, tableName, columns, condition);
        }

        public static void Select(string schemaName, string tableName, ColumnFields columns, string condition = "")
        {
            Schemata schema;

            if (schemaName.Length > 0)
            {
                if (Global.GLB_SchemataList.ContainsKey(schemaName)) new Schemata(schemaName);
                schema = Global.GLB_SchemataList[schemaName];
            }
            else
                schema = Global.GLB_SchemataList[Global.GLB_DefaultSchemata];

            if (!schema.Tables.ContainsKey(tableName)) throw new ArgumentException("Table doesn't exists: " + tableName);

            if (columns.Count > 0)
                foreach (string s in columns.Items)
                    if (!schema.Tables[tableName].Columns.ContainsKey(s)) throw new ArgumentException("Column name doesn't exists: " + s);

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT");

            if (columns.Count < 1)
                sb.Append(" *");
            else
            {
                foreach (string s in columns.Items)
                    sb.Append(" `" + s + "`,");

                sb = sb.Remove(sb.Length - 1, 1);
            }

            if (schemaName.Length > 0)
                sb.Append(" FROM `" + schema.SchemaName + "`.`" + schema.Tables[tableName].TableName + "`");
            else
                sb.Append(" FROM `" + schema.Tables[tableName].TableName + "`");

            if (condition.Length > 0)
                sb.Append(" " + condition);

            //Generics.Command = sb.ToString();
            sb = null;
        }
    }
}