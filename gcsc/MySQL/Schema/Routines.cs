﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GCSC.MySQL.Schema
{
    internal sealed class Routines
    {
        public Routines(string schema)
        {
            RoutineLists = new List<Routine>();

            GCSC.MySQL.DML.Select s = new MySQL.DML.Select()
            {
                Command = "SELECT `ROUTINE_NAME`, `ROUTINE_TYPE`, `DATA_TYPE`, `CHARACTER_MAXIMUM_LENGTH`, `NUMERIC_PRECISION`, `NUMERIC_SCALE` FROM `information_schema`.`ROUTINES` WHERE `ROUTINE_SCHEMA` = @schema"
            };
            s.AddParamWithValue("schema", schema);

            Array.ForEach(s.Reader(), r =>
            {
                var routine = new Routine(r, schema);
                RoutineLists.Add(routine);
                MaxParamCount = MaxParamCount >= routine.ParametersCount ? MaxParamCount : routine.ParametersCount;
                MinParamCount = MinParamCount <= routine.ParametersCount ? MinParamCount : routine.ParametersCount;
            });
        }

        public Routine GetProcedure(string procedureName)
        {
            return (Routine)RoutineLists.Where(proc => proc.RoutineType == "PROCEDURE" && proc.RoutineName == procedureName);
        }

        public Routine GetFunction(string functionName)
        {
            return (Routine)RoutineLists.Where(func => func.RoutineType == "FUNCTION" && func.RoutineName == functionName);
        }

        public List<Routine> GetProcedures
        {
            get { return new List<Routine>(RoutineLists.Where(proc => proc.RoutineType == "PROCEDURE")); }
        }

        public List<Routine> GetFunctions
        {
            get { return new List<Routine>(RoutineLists.Where(func => func.RoutineType == "FUNCTION")); }
        }

        public int MaxParamCount { get; private set; }

        public int MinParamCount { get; private set; }

        public List<Routine> RoutineLists { get; internal set; }
    }
}