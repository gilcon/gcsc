using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security;

namespace GCSC.MySQL
{
    public sealed class ConnectionString : ConnectionStringProperties, IDisposable
    {
        private System.Text.StringBuilder connectionString = new System.Text.StringBuilder();
        private static Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        private ConnectionStringsSection section = config.ConnectionStrings;

        private bool isDisposed;

        public ConnectionString() { }

        public ConnectionString(ref ConnectionStringProperties connectionStringProperties)
        {
            if (connectionStringProperties != null)
            {
                foreach (KeyValuePair<string, object> kv in connectionStringProperties.connectionProperties)
                    connectionProperties[kv.Key] = kv.Value;
                connectionStringProperties.connectionProperties.Clear();
                connectionStringProperties = null;
            }
        }

        ~ConnectionString()
        {
            Dispose(false);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposing)
        {
            if (!isDisposed)
                if (isDisposing)
                {
                    connectionProperties.Clear();
                    connectionString = null;
                    config = null;
                    section = null;
                }

            isDisposed = true;
        }

        [Flags]
        public enum CertificateStoreLocation
        {
            None = 0,
            CurrentUser = 1,
            LocalMachine = 2
        }

        [Flags]
        public enum ConnectionProtocol
        {
            Socket = 0,
            NamedPipe = 1,
            Unix = 2,
            SharedMemory = 3
        }

        [Flags]
        public enum SSLMode
        {
            None = 0,
            Prepared = 1,
            Required = 2,
            VerifyCA = 3,
            VerifyFull = 4
        }

        /// <summary>
        /// Set-up the connection string
        /// </summary>
        /// <param name="connectionName">string connection name</param>
        public void SetConnectionString(string connectionName)
        {
            if (isDisposed) throw new ObjectDisposedException("ConnectionString");
            //server=localhost;User Id=root;password=aspire;Persist Security Info=True;database=test
            if (!connectionProperties.ContainsKey("server") || connectionProperties["server"].ToString().Length < 1) throw new ArgumentNullException("Server");
            if (!connectionProperties.ContainsKey("User Id") || connectionProperties["User Id"].ToString().Length < 1) throw new ArgumentNullException("UserId");
            if (!connectionProperties.ContainsKey("password") || connectionProperties["password"].ToString().Length < 1) throw new ArgumentNullException("Password");
            if (!connectionProperties.ContainsKey("database") || connectionProperties["database"].ToString().Length < 1) throw new ArgumentNullException("Database");
            if (connectionName.Length < 1) throw new ArgumentNullException("connectionName");

            foreach (KeyValuePair<string, object> k in connectionProperties)
                connectionString.Append(k.Key + "=" + k.Value + ";");

            connectionString.Remove(connectionString.Length - 1, 1);
            Encrypt(connectionName, connectionString.ToString(), "MySql.Data.MySqlClient");
            connectionName = null;
        }

        private void Encrypt(string connectionName, string connectionString, string provider)
        {
            section.ConnectionStrings.Clear();
            if (section == null)
            {
                section = new ConnectionStringsSection();
                config.Sections.Add("connnectionSetting", section);
            }

            if (!section.SectionInformation.IsProtected)
                section.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");

            section.ConnectionStrings.Add(new ConnectionStringSettings(connectionName, connectionString, provider));

            connectionString = null;
            connectionName = null;
            provider = null;

            section.SectionInformation.ForceSave = true;
            config.Save(ConfigurationSaveMode.Full);
        }

        /// <summary>
        /// get the connection string from configuration file and store it in secure storage
        /// </summary>
        /// <param name="connectionName">the name of the connection string</param>
        public static void GetConnectionString(string connectionName)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings[connectionName];
            Global.GLB_ConnectionStringSecured.Clear();
            for (int i = 0; i < cs.ConnectionString.Length; i++)
            {
                Global.GLB_ConnectionStringSecured.AppendChar(cs.ConnectionString[i]);
            }

            cs = null;
            new Schema.Schemata(connectionName, true);
        }

        /// <summary>
        /// Custom Connection string
        /// </summary>
        /// <param name="connectionString">connection string</param>
        /// <returns>SecureString</returns>
        public static SecureString ProtectCustomConnectionString(string connectionString)
        {
            SecureString ss = new SecureString();
            for (int i = 0; i < connectionString.Length; i++)
            {
                ss.AppendChar(connectionString[i]);
            }
            connectionString = null;
            ss.MakeReadOnly();

            return ss;
        }

        public static bool CheckConnectionString(string connectionName)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConnectionStringsSection section = config.ConnectionStrings;

            if (section == null)
                return false;

            if (!section.SectionInformation.IsProtected)
                return false;

            GetConnectionString(connectionName);

            return true;
        }
    }
}