﻿using System;
using System.Collections.Generic;
using GCSC.MySQL.Schema;

namespace GCSC.MySQL.DML
{
    public sealed class Delete : Conditions, IDisposable
    {
        private Dictionary<string, object> _parameters;
        private string _schema, _table, _finalQueryString;

        private bool isDisposed;

        public Delete()
        {
            _parameters = new Dictionary<string, object>();
            _schema = string.Empty;
            _table = string.Empty;
            _finalQueryString = string.Empty;
            Condition = string.Empty;
        }

        ~Delete()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool isDisposing)
        {
            if (!isDisposed)
            {
                if (isDisposing)
                {
                    _parameters = null;
                    _schema = null;
                    _table = null;
                    _finalQueryString = null;
                    Condition = null;
                }
            }

            isDisposed = true;
        }

        public void AddParamWithValue(string paramName, object value)
        {
            if (isDisposed) throw new ObjectDisposedException("Delete");
            _parameters.Add("@" + paramName, value);
        }

        public string GenerateQueryString(bool rebuildQueryString = false)
        {
            if (isDisposed) throw new ObjectDisposedException("Delete");
            if (_table.Length < 0) throw new ArgumentNullException("There is no table assigned", new Exception());
            if (rebuildQueryString) _finalQueryString = string.Empty;
            if (_finalQueryString.Length > 0) goto label1;

            Schemata schema;
            if (_schema.Length > 0)
            {
                if (!Global.GLB_SchemataList.ContainsKey(_schema)) new Schemata(_schema);
                schema = Global.GLB_SchemataList[_schema];
            }
            else
                schema = Global.GLB_SchemataList[Global.GLB_DefaultSchemata];

            if (!schema.Tables.ContainsKey(_table)) throw new ArgumentException("Table doesn't exists: " + _table);

            if (Condition.Length < 0)
                _finalQueryString = "DELETE FROM `" + _table + "`";
            else
            {
                int paramCount = Condition.Split('@').Length;
                if (paramCount - 1 > _parameters.Count) throw new ArgumentOutOfRangeException("Parameters", "Parameter supplied doesn't match.");
                _finalQueryString = "DELETE FROM `" + _table + "` WHERE " + Condition;
            }

        label1:
            return _finalQueryString;
        }

        public int Execute()
        {
            return Execute(string.Empty);
        }

        public int Execute(string connectionString)
        {
            if (isDisposed) throw new ObjectDisposedException("Delete");

            var q = new Query(GenerateQueryString(), connectionString) { Schema = _schema };
            return q.Execute(_parameters, true);
        }

        public string Table
        {
            set
            {
                if (isDisposed) throw new ObjectDisposedException("Delete");
                _table = value;
            }
        }

        public string Schema
        {
            set
            {
                if (isDisposed) throw new ObjectDisposedException("Delete");
                _schema = value;
            }
        }

        public bool PreparedStatement
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}