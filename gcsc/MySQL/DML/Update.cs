﻿using System;
using System.Collections.Generic;
using System.Text;
using GCSC.MySQL.Schema;
using GCSC.Util;

namespace GCSC.MySQL.DML
{
    public sealed class Update : Conditions, IDisposable
    {
        private bool isDisposed;
        private StringBuilder _queryString;
        private string _finalQueryString;
        private string _schema;
        private string _table;
        private readonly Collections<string> _fields;
        private readonly Collections<object> _values;
        private Dictionary<string, object> _parameters;

        public Update()
        {
            _queryString = new StringBuilder();
            _finalQueryString = string.Empty;
            _schema = string.Empty;
            _table = string.Empty;
            _fields = new Collections<string>();
            _values = new Collections<object>();
            _parameters = new Dictionary<string, object>();
            Condition = string.Empty;
        }

        ~Update()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposing)
        {
            if (!isDisposed)
                if (isDisposing)
                {
                    _queryString = null;
                    _finalQueryString = null;
                    _schema = null;
                    _table = null;
                    _fields.Dispose();
                    _values.Dispose();
                    _parameters = null;
                    Condition = null;
                }
            isDisposed = true;
        }

        public string Table
        {
            set
            {
                if (isDisposed) throw new ObjectDisposedException("Update");
                _table = value;
            }
        }

        public string Schema
        {
            set
            {
                if (isDisposed) throw new ObjectDisposedException("Update");
                _schema = value;
            }
        }

        public void Fields(params string[] columns)
        {
            if (isDisposed) throw new ObjectDisposedException("Update");
            Array.ForEach(columns, c => _fields.Add(c.Trim()));
        }

        public void Values(params object[] values)
        {
            if (isDisposed) throw new ObjectDisposedException("Update");
            _values.AddRange(values);
        }

        public void AddParamWithValue(string paramName, object value)
        {
            if (isDisposed) throw new ObjectDisposedException("Update");
            _parameters.Add("@" + paramName, value);
        }

        private void Check()
        {
            Schemata schema;
            if (_schema.Length > 0)
            {
                if (!Global.GLB_SchemataList.ContainsKey(_schema)) new Schemata(_schema);
                schema = Global.GLB_SchemataList[_schema];
            }
            else
                schema = Global.GLB_SchemataList[Global.GLB_DefaultSchemata];

            if (!schema.Tables.ContainsKey(_table)) throw new ArgumentException("Table doesn't exists: " + _table);

            foreach (string s in _fields)
                if (!schema.Tables[_table].Columns.ContainsKey(s)) throw new ArgumentOutOfRangeException("column", "Column fields specified, doesn't exists.");
        }

        public string GenerateQueryString()
        {
            if (isDisposed) throw new ObjectDisposedException("Update");
            if (_finalQueryString.Length > 0) goto label1;
            if (_table.Length < 1) throw new ArgumentNullException("There is no table assigned", new Exception());
            if (_values.Count < 1) throw new ArgumentNullException("There is no data", new Exception());
            if (_fields.Count != _values.Count) throw new ArgumentOutOfRangeException("The range of fields is not correspond with the values.", new Exception());

            Check();

            _queryString.Append(string.Format("UPDATE `{0}` SET ", _table));
            string tempString;

            for (int i = 0; i < _fields.Count; i++)
            {
                tempString = "`" + _fields[i] + "`=@_" + _fields[i] + "_" + i + ",";
                _parameters.Add("@_" + _fields[i] + "_" + i, _values[i]);
                _queryString.Append(tempString);
            }

            tempString = Condition.Length > 0 ? _queryString.Remove(_queryString.Length - 1, 1).Append(string.Format(" WHERE {0}", Condition)).ToString() : _queryString.Remove(_queryString.Length - 1, 1).ToString();

            int paramCount = tempString.Split('@').Length;
            if (paramCount - 1 > _parameters.Count) throw new ArgumentOutOfRangeException("Parameters", "Parameter supplied doesn't match.");

            _finalQueryString = tempString;
            tempString = null;

        label1:
            return _finalQueryString;
        }

        public int Execute()
        {
            return Execute(string.Empty);
        }

        public int Execute(string connectionString)
        {
            if (isDisposed) throw new ObjectDisposedException("Update");

            var q = new Query(GenerateQueryString(), connectionString) { Schema = _schema };
            return q.Execute(_parameters, true);
        }

        public bool PreparedStatement
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}