﻿namespace GCSC.MySQL.DML
{
    public class Conditions
    {
        internal Conditions internalConditions;

        public Conditions AssignConditions
        {
            get { return internalConditions; }
            set
            {
                Condition = value.Condition;
                internalConditions = value;
            }
        }

        public string Condition { get; set; }

        public void Where(string columnName, string parameterName)
        {
            Where(columnName, parameterName, Operator.EQUAL);
        }

        public void Where(string columnName, string parameterName, Operator op)
        {
            Condition += string.Format(" `{0}` {1} @{2}", columnName, operatorString(op), parameterName);
        }

        public void AndWhere(string columnName, string parameterName)
        {
            AndWhere(columnName, parameterName, Operator.EQUAL);
        }

        public void AndWhere(string columnName, string parameterName, Operator op)
        {
            Condition += string.Format(" AND `{0}` {1} @{2}", columnName, operatorString(op), parameterName);
        }

        public void OrWhere(string columnName, string parameterName)
        {
            OrWhere(columnName, parameterName, Operator.EQUAL);
        }

        public void OrWhere(string columnName, string parameterName, Operator op)
        {
            Condition += string.Format(" OR `{0}` {1} @{2}", columnName, operatorString(op), parameterName);
        }

        public void _AndWhereSymbol()
        {
            Condition += " AND ";
        }

        public void _OrWhereSymbol()
        {
            Condition += " OR ";
        }

        public void WhereIn(string columnName, string inQuery)
        {
            Condition += string.Format(" `{0}` IN ({1})", columnName, inQuery);
        }

        public void WhereNotIn(string columnName, string inQuery)
        {
            Condition += string.Format(" `{0}` NOT IN ({1})", columnName, inQuery);
        }

        public void WhereIsNull(string columnName)
        {
            Condition += string.Format(" `{0}` IS NULL", columnName);
        }

        public void WhereIsNotNull(string columnName)
        {
            Condition += string.Format(" `{0}` IS NOT NULL", columnName);
        }

        public void WhereBetween(string columnName, string min, string max)
        {
            Condition += string.Format(" `{0}` BETWEEN @{1} AND @{2}", columnName, min, max);
        }

        internal static string operatorString(Operator op)
        {
            string value;
            switch ((int)op)
            {
                case 0:
                    value = " AND ";
                    break;
                case 1:
                    value = " && ";
                    break;
                case 2:
                    value = " <=> ";
                    break;
                case 3:
                    value = " = ";
                    break;
                case 4:
                    value = " >= ";
                    break;
                case 5:
                    value = " > ";
                    break;
                case 6:
                    value = " <= ";
                    break;
                case 7:
                    value = " < ";
                    break;
                case 8:
                    value = " IS NOT NULL ";
                    break;
                case 9:
                    value = " IS NOT ";
                    break;
                case 10:
                    value = " IS NULL ";
                    break;
                case 11:
                    value = " LIKE ";
                    break;
                case 12:
                    value = " != ";
                    break;
                case 13:
                    value = " <> ";
                    break;
                case 14:
                    value = " NOT LIKE ";
                    break;
                case 15:
                    value = " NOT ";
                    break;
                case 16:
                    value = " ! ";
                    break;
                case 17:
                    value = " OR ";
                    break;
                case 18:
                    value = " || ";
                    break;
                case 19:
                    value = " SOUNDS LIKE ";
                    break;
                default:
                    value = " = ";
                    break;
            }

            return value;
        }
    }
}