﻿using System;
using System.Collections.Generic;
using System.Text;
using GCSC.MySQL.Schema;
using GCSC.Util;

namespace GCSC.MySQL.DML
{
    public sealed class Insert : IDisposable
    {
        private bool isDisposed;
        private StringBuilder _queryString;
        private string _finalQueryString, _schema, _table;
        private int? _valueCount;
        private readonly Collections<string> _fields;
        private readonly Collections<object> _values;
        private Dictionary<string, object> _parameters;
        Schemata schema;

        public Insert()
        {
            _schema = string.Empty;
            _table = string.Empty;
            _queryString = new StringBuilder();
            _finalQueryString = string.Empty;
            _valueCount = -1;
            _fields = new Collections<string>();
            _values = new Collections<object>();
            _parameters = new Dictionary<string, object>();
            IsFullTable = false;
        }

        ~Insert()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposing)
        {
            if (!isDisposed)
                if (isDisposing)
                {
                    _queryString = null;
                    _finalQueryString = null;
                    _schema = null;
                    _table = null;
                    _valueCount = null;
                    _fields.Dispose();
                    _values.Dispose();
                    _parameters = null;
                    IsFullTable = false;
                    schema = null;
                }

            isDisposed = true;
        }

        public string Table
        {
            set
            {
                if (isDisposed) throw new ObjectDisposedException("Insert");
                _table = value;
            }
        }

        public string Schema
        {
            set
            {
                if (isDisposed) throw new ObjectDisposedException("Insert");
                _schema = value;
            }
        }

        public bool IsFullTable { get; set; }

        public void Fields(params string[] columns)
        {
            if (isDisposed) throw new ObjectDisposedException("Insert");
            Array.ForEach(columns, c => _fields.Add(c.Trim()));
        }

        public void Values(params object[] values)
        {
            if (isDisposed) throw new ObjectDisposedException("Insert");
            if (_valueCount > 0 && _valueCount != values.Length) throw new ArgumentOutOfRangeException("values");

            _valueCount = values.Length;
            _values.AddRange(values);
        }

        private void Check()
        {
            if (_schema.Length > 0)
            {
                if (!Global.GLB_SchemataList.ContainsKey(_schema)) new Schemata(_schema);
                schema = Global.GLB_SchemataList[_schema];
            }
            else
                schema = Global.GLB_SchemataList[Global.GLB_DefaultSchemata];

            if (!schema.Tables.ContainsKey(_table)) throw new ArgumentException("Table doesn't exists: " + _table);

            if (_fields.Count > 0)
            {
                foreach (string s in _fields)
                    if (!schema.Tables[_table].Columns.ContainsKey(s)) throw new ArgumentException("Column name doesn't exists: " + s);

                foreach (KeyValuePair<string, Column> kv in schema.Tables[_table].Columns)
                    if (!kv.Value.IsNullable && kv.Value.ColumnDefault.Length < 1 && !kv.Value.IsAutoIncrement)
                        if (!_fields.Contains(kv.Key)) throw new ArgumentNullException("Not Nullable and has no default value column found: " + kv.Key);
            }
            else
            {
                int? counter = 0;
                foreach (KeyValuePair<string, Column> kv in schema.Tables[_table].Columns)
                    if (!kv.Value.IsNullable && !kv.Value.IsAutoIncrement) counter++;

                if (_valueCount < counter) throw new IndexOutOfRangeException("Value doesn't meet column requirement.");

                List<Column> unassignedColumn = new List<Column>();
                counter = 0;

                foreach (KeyValuePair<string, Column> kv in schema.Tables[_table].Columns)
                {
                    if (kv.Value.IsAutoIncrement)
                        unassignedColumn.Add(kv.Value);
                    else
                    {
                        _fields.Add(kv.Key);
                        if (++counter >= _valueCount) break;
                    }
                }

                counter = 0;
                if (_fields.Count < _valueCount)
                    foreach (Column c in unassignedColumn)
                    {
                        _fields.Insert((int)counter++, c.ColumnName);
                        if (_fields.Count >= _valueCount) break;
                    }

                foreach (KeyValuePair<string, Column> kv in schema.Tables[_table].Columns)
                    if (!kv.Value.IsNullable && kv.Value.ColumnDefault.Length < 1 && !kv.Value.IsAutoIncrement)
                        if (!_fields.Contains(kv.Key)) throw new ArgumentNullException("Not Nullable and has no default value column found: " + kv.Key);

                counter = null;
            }
        }

        public string GenerateQueryString()
        {
            if (isDisposed) throw new ObjectDisposedException("Insert");
            if (_finalQueryString.Length > 0) goto label2;
            if (_table.Length < 1) throw new ArgumentNullException("There is no table assigned.", new Exception());
            if (_values.Count < 1) throw new ArgumentNullException("There is no data.", new Exception());

            _queryString.Append("INSERT INTO `" + _table + "`()");

            if (IsFullTable) goto label1;

            Check();

            if (_fields.Count > 0)
            {
                if (_fields.Count != _valueCount) throw new ArgumentOutOfRangeException("The range of fields does not correspond with the values.");
                Array.ForEach(_fields.ToArray(), f => _queryString.Insert(_queryString.Length - 1, "`" + f + "`,"));
                _queryString = _queryString.Remove(_queryString.Length - 2, 1);
            }
        label1:
            if (IsFullTable && schema.Tables[_table].Columns.Count != _valueCount) throw new ArgumentOutOfRangeException("Invalid values count");

            _queryString.Append(" VALUES");

            int? valueOccurance = _values.Count / _valueCount, counter = -1;
            string tempString, tempParamString;

            for (int i = 0; i < valueOccurance; i++)
            {
                tempString = string.Empty;
                for (int j = 0; j < _valueCount; j++)
                {
                    tempParamString = _fields[j] + "_" + i;
                    _parameters.Add("@" + tempParamString, _values[(int)++counter]);
                    tempString += "@" + tempParamString + ",";
                }
                _queryString.Append("(" + tempString.Remove(tempString.Length - 1) + "),");
            }

            counter = valueOccurance = null;
            tempString = tempParamString = null;

            _finalQueryString = _queryString.Remove(_queryString.Length - 1, 1).ToString();
        label2:
            return _finalQueryString;
        }

        public int Execute()
        {
            return Execute(string.Empty);
        }

        public int Execute(string connectionString)
        {
            if (isDisposed) throw new ObjectDisposedException("Insert");
            var q = new Query(GenerateQueryString(), connectionString) { Schema = _schema };
            return q.Execute(_parameters, true);
        }

        public bool PreparedStatement
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}