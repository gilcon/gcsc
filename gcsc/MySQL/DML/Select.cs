﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace GCSC.MySQL.DML
{
    public sealed class Select
    {
        int _rowCount, _fieldCount;
        string _customConnectionString;
        object[][] _data;
        object _scalarData;
        Dictionary<string, object> _paramters;
        bool _refresherFlag;

        public Select()
        {
            Command = string.Empty;
            _customConnectionString = string.Empty;
            _paramters = new Dictionary<string, object>();
            Schema = string.Empty;
            _data = null;
            _scalarData = null;
            _refresherFlag = false;
        }

        public Select(string command)
        {
            Command = command;
            _customConnectionString = string.Empty;
            _paramters = new Dictionary<string, object>();
            Schema = string.Empty;
            _data = null;
            _scalarData = null;
            _refresherFlag = false;
        }

        public Select(string command, string connectionString)
        {
            Command = command;
            _customConnectionString = connectionString;
            _paramters = new Dictionary<string, object>();
            Schema = string.Empty;
            _data = null;
            _scalarData = null;
            _refresherFlag = false;
        }

        private bool CheckParamLenght()
        {
            int l = Command.Split('@').Length;

            if (l - 1 > _paramters.Count)
                return false;
            return true;
        }

        public void Clear()
        {
            Command = null;
            _customConnectionString = null;
            _paramters = null;
            Schema = null;
            _data = null;
            _scalarData = null;
            _refresherFlag = false;
            _rowCount = _fieldCount = 0;
        }

        public void RefreshReader()
        {
            _refresherFlag = true;
            Reader();
            _refresherFlag = false;
        }

        public void RefreshScalar()
        {
            _refresherFlag = true;
            Scalar();
            _refresherFlag = false;
        }

        public void AddParamWithValue(string paramName, object value)
        {
            _paramters.Add(paramName.Trim(), value);
        }

        public object Scalar()
        {
            if (_scalarData != null && !_refresherFlag) return _scalarData;
            if (!CheckParamLenght()) throw new ArgumentOutOfRangeException("Paramter");

            using (var cn = new MySqlConnection())
            {
                if (_customConnectionString.Length > 0)
                    cn.ConnectionString = _customConnectionString;
                else
                    using (var sc = new Securing_ConnectionString())
                        cn.ConnectionString = sc.Get_Secured_ConnectionString();

                using (var cmd = new MySqlCommand(Command, cn))
                {
                    foreach (KeyValuePair<string, object> kv in _paramters)
                        cmd.Parameters.AddWithValue("@" + kv.Key, kv.Value);
                    cn.Open();
                    if (Schema.Length > 0) cn.ChangeDatabase(Schema);

                    _rowCount = 1;
                    return _scalarData = cmd.ExecuteScalar();
                }
            }
        }

        public object[][] Reader()
        {
            if (_data != null && !_refresherFlag) return _data;
            if (!CheckParamLenght()) throw new ArgumentOutOfRangeException("Paramters");

            using (var cn = new MySqlConnection())
            {
                if (_customConnectionString.Length > 0)
                    cn.ConnectionString = _customConnectionString;
                else
                    using (var sc = new Securing_ConnectionString())
                        cn.ConnectionString = sc.Get_Secured_ConnectionString();

                using (var cmd = new MySqlCommand(Command, cn))
                {
                    foreach (KeyValuePair<string, object> kv in _paramters)
                        cmd.Parameters.AddWithValue("@" + kv.Key, kv.Value);

                    cn.Open();
                    if (Schema.Length > 0) cn.ChangeDatabase(Schema);
                    if (TableDirect) cmd.CommandType = System.Data.CommandType.TableDirect;

                    MySqlDataReader reader = cmd.ExecuteReader();

                    using (GCSC.Util.Collections<object[]> _tempData = new GCSC.Util.Collections<object[]>())
                    {
                        object[] dataStorage;
                        _fieldCount = reader.FieldCount;

                        while (reader.Read())
                        {
                            dataStorage = new object[reader.FieldCount];
                            for (int i = 0; i < reader.FieldCount; i++)
                                dataStorage[i] = reader[i];
                            _tempData.Add(dataStorage);
                            _rowCount++;
                        }
                        reader.Dispose();
                        return _data = _tempData.ToArray();
                    }
                }
            }
        }

        public string Command { get; set; }

        public int FielCount
        {
            get
            {
                if (_fieldCount < 1)
                    Reader();
                return _fieldCount;
            }
        }

        public int RowCount
        {
            get
            {
                if (_rowCount < 1)
                    Reader();
                return _rowCount;
            }
        }

        public string Schema { get; set; }

        public bool TableDirect { get; set; }

        public bool PreparedStatement
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}