﻿using GCSC.Util;
using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace GCSC.Security
{
	public sealed class Encryption : IDisposable
	{
		private bool isDisposed;

		private byte[] result_byte;
		private byte[] dataToHash_byte;

		public string Salt { get; set; }

		public Encryption()
		{
			Salt = "";
			isDisposed = false;
		}

		~Encryption()
		{
			Dispose(false);
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!isDisposed)
				if (disposing)
				{
					Salt = null;
					dataToHash_byte = null;
					result_byte = null;
				}

			isDisposed = true;
		}

		public string Encrypt(string dataToHash)
		{
			if (isDisposed)
				throw new ObjectDisposedException("Encryption");

			if (Salt.Length == 0)
				Salt = new RandomText().String(24, true, true);

			using (HashAlgorithm HashAlgo = new SHA512Managed())
			{
				dataToHash_byte = Encoding.Unicode.GetBytes(dataToHash);
				dataToHash = null;

				using (var mac = new MACTripleDES(Encoding.ASCII.GetBytes(Salt)))
					result_byte = HashAlgo.ComputeHash(mac.ComputeHash(dataToHash_byte));
			}

			string rs = Encoding.ASCII.GetString(result_byte);
			var sb = new StringBuilder(rs.Length);

			foreach (char c in rs)
				if (c != '\n' && c != '\r' && c != '\t' && c != '\v')
					sb.Append(c);

			return sb.ToString();//Encoding.ASCII.GetString(result_byte);
		}

		public string Encrypt(SecureString str)
		{
			if (isDisposed)
				throw new ObjectDisposedException("Encryption");

			using (var s = str)
			{
				var secretStrPtr = Marshal.SecureStringToBSTR(s);

				if (Salt.Length == 0)
					Salt = new RandomText().String(24, true, true);

				using (HashAlgorithm HashAlgo = new SHA512Managed())
				{
					using (var mac = new MACTripleDES(Encoding.ASCII.GetBytes(Salt)))
						result_byte = HashAlgo.ComputeHash(mac.ComputeHash(
							Encoding.Unicode.GetBytes(Marshal.PtrToStringBSTR(secretStrPtr))));
				}

				Marshal.ZeroFreeBSTR(secretStrPtr);
				if (!s.IsReadOnly())
					s.Clear();
			}

			string rs = Encoding.ASCII.GetString(result_byte);
			var sb = new StringBuilder(rs.Length);

			foreach (char c in rs)
				if (c != '\n' && c != '\r' && c != '\t' && c != '\v')
					sb.Append(c);

			return sb.ToString();
		}

		/// <summary>
		/// Generate Random number to be used in hashing algorithm
		/// </summary>
		/// <param name="byteLenght">int lenght of bytes</param>
		/// <returns>string random number</returns>
		public static string CryptRandomNumber(int byteLenght)
		{
			byte[] n = new byte[byteLenght];
			var r = RandomNumberGenerator.Create();
			r.GetBytes(n);
			return BitConverter.ToString(n);
		}

		/// <summary>
		/// Generate ordinary hashed password
		/// </summary>
		/// <param name="algorithm">string name of the algorithm to be used</param>
		/// <param name="password">string password to be hashed</param>
		/// <returns>string hashed password</returns>
		public static string OrdPasswordHash(string algorithm, string password)
		{
			var h = algorithm.CompareTo("SHA1Managed") == 0 ? new SHA1Managed() : HashAlgorithm.Create(algorithm);

			using (h)
			{
				return BitConverter.ToString(h.ComputeHash(Encoding.Default.GetBytes(password)));
			}
		}

		/// <summary>
		/// Compare newly genereted hash code with an existing hash code represented by hex code
		/// </summary>
		/// <param name="hash">new hash code</param>
		/// <param name="oldHashString">HEX: old hash code</param>
		/// <returns></returns>
		public static bool VerifyHexHash(byte[] hash, string oldHashString)
		{
			var n = new StringBuilder(hash.Length);
			Array.ForEach(hash, b => n.AppendFormat("{0:X2}", b));

			return (oldHashString == n.ToString());
		}

		/// <summary>
		/// Compare newly genereted hash code with an existing hash code represented by  Base64-encoded string
		/// </summary>
		/// <param name="hash">new hash code</param>
		/// <param name="oldHashString">Base64: old hash code string</param>
		/// <returns></returns>
		public static bool VerifyB64Hash(byte[] hash, string oldHashString)
		{
			return (oldHashString == Convert.ToBase64String(hash));
		}

		/// <summary>
		/// Compare newly genereted hash code with an existing hash code represented by byte array
		/// </summary>
		/// <param name="hash">new hash code</param>
		/// <param name="oldhash">old hash code</param>
		/// <returns>boolean</returns>
		public static bool VerifyByteHash(byte[] hash, byte[] oldhash)
		{
			if (hash == null || oldhash == null || hash.Length != oldhash.Length)
				return false;

			for (int count = 0; count < hash.Length; count++)
			{
				if (hash[count] != oldhash[count])
					return false;
			}

			return true;
		}
	}
}
