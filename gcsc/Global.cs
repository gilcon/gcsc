using GCSC.Data.MySQL.Schema;
using System.Collections.Generic;
using System.Security;

namespace GCSC
{
	internal class Global
	{
		protected internal static Dictionary<string, Dictionary<string, SecureString>> GLB_ProtectedConnectionStringCollection = new Dictionary<string, Dictionary<string, SecureString>>();
		protected internal static Dictionary<string, SecureString> GLB_DefaultProviderConnectionStrings = new Dictionary<string, SecureString>();
		protected internal static Dictionary<string, MySQLSchemata> GLB_SchemataList = new Dictionary<string, MySQLSchemata>();
		protected internal static string GLB_DefaultSchemata = "";
		protected internal static Util.AppEvents GLB_AppEvents;
	}
}
