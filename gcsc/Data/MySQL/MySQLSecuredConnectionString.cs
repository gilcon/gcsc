﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace GCSC.Data.MySQL
{
	internal sealed class MySQLSecuredConnectionString : IDisposable
	{
		private readonly SecureString securedStr;
		private readonly SecuredConnectionStringProviderType provider;
		private IntPtr strPtr;
		private bool isDisposed;

		public MySQLSecuredConnectionString()
			: this(string.Empty, SecuredConnectionStringProviderType.Default)
		{
		}

		public MySQLSecuredConnectionString(string connectionName, SecuredConnectionStringProviderType provider)
		{
			this.provider = provider;
			if (this.provider == SecuredConnectionStringProviderType.Default)
				securedStr = Global.GLB_DefaultProviderConnectionStrings["MySql.Data.MySqlClient"].Copy();
			else
			{
				if (connectionName.Length < 1) throw new ArgumentNullException("connectionName");
				securedStr = Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"][connectionName].Copy();
			}
		}

		~MySQLSecuredConnectionString()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool isDisposing)
		{
			if (!isDisposed)
				if (isDisposing)
				{
					securedStr.Dispose();
					Marshal.ZeroFreeBSTR(strPtr);
				}
			isDisposed = true;
		}

		public string GetConnectionString()
		{
			return GetConnectionString(null);
		}

		public string GetConnectionString(MySQLConnectionStringProperties properties)
		{
			if (isDisposed) throw new ObjectDisposedException("MySQLSecuredConnectionString");

			strPtr = Marshal.SecureStringToBSTR(securedStr);
			return properties != null ?
				ModifyConnectionString(Marshal.PtrToStringBSTR(strPtr), properties) : Marshal.PtrToStringBSTR(strPtr);
		}

		private string ModifyConnectionString(string fs, MySQLConnectionStringProperties properties)
		{
			MySQLConnectionString.ModifyConnection(ref fs, properties);
			return fs;
		}
	}
}
