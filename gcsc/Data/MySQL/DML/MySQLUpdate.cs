﻿using GCSC.Data.MySQL.Schema;
using System;
using System.Collections.Generic;
using System.Text;

namespace GCSC.Data.MySQL.DML
{
	public sealed class MySQLUpdate : MySQLConditions, IDisposable, GCSC.Interface.Data.DML.IUpdate
	{
		/// <summary>
		/// used for incrementing when adding value without specifing the parameters
		/// </summary>
		private int _paramIncrement = 0;

		private bool isDisposed, _executed;
		private StringBuilder _queryString;
		private string _finalQueryString;
		private string _schema;
		private string _table;
		private readonly List<string> _fields;
		private readonly List<object> _values;
		private Dictionary<string, object> _parameters;
		private MySQLQuery _query;

		public MySQLUpdate()
			: this(false)
		{
		}

		public MySQLUpdate(bool isPrepared)
		{
			_queryString = new StringBuilder();
			_finalQueryString = string.Empty;
			_schema = string.Empty;
			_table = string.Empty;
			_fields = new List<string>();
			_values = new List<object>();
			_parameters = new Dictionary<string, object>();
			Condition = string.Empty;
			PreparedStatement = isPrepared;
		}

		~MySQLUpdate()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool isDisposing)
		{
			if (!isDisposed)
				if (isDisposing)
				{
					_queryString = null;
					_finalQueryString = null;
					_schema = null;
					_table = null;
					_parameters = null;
					Condition = null;
					if (_query != null) _query.Dispose();
				}
			isDisposed = true;
		}

		public void Fields(params string[] columns)
		{
			if (isDisposed) throw new ObjectDisposedException("Update");
			Array.ForEach(columns, c => _fields.Add(c.Trim()));
		}

		public void Values(params object[] values)
		{
			if (isDisposed) throw new ObjectDisposedException("Update");
			_values.AddRange(values);
		}

		public void AddParamWithValue(object value)
		{
			AddParamWithValue("__p" + _paramIncrement++, value);
		}

		public void AddParamWithValue(string paramName, object value)
		{
			if (isDisposed) throw new ObjectDisposedException("Update");
			_parameters.Add("@" + paramName, value);
		}

		public void ModifyValues(params object[] values)
		{
			if (isDisposed) throw new ObjectDisposedException("Update");
			if (!PreparedStatement || !_executed)
				throw new InvalidOperationException("Prepared statement not allowed or no primary execution executed.");
			if (_parameters.Count != values.Length) throw new ArgumentOutOfRangeException("values", "Total numbers of values not equal to numbers of paramters");

			int counter = 0;
			foreach (var kv in _parameters)
				_parameters[kv.Key] = values[counter++];
		}

		public int ExecuteWithModifiedValue(params object[] values)
		{
			if (isDisposed) throw new ObjectDisposedException("Update");
			if (!PreparedStatement || !_executed)
				throw new InvalidOperationException("Prepared statement not allowed or no primary execution executed.");
			if (_parameters.Count != values.Length) throw new ArgumentOutOfRangeException("values", "Total numbers of values not equal to numbers of paramters");

			int counter = 0;
			foreach (var kv in _parameters)
				_parameters[kv.Key] = values[counter++];
			return Execute();
		}

		private void Check()
		{
			MySQLSchemata schema;
			if (_schema.Length > 0)
			{
				if (!Global.GLB_SchemataList.ContainsKey(_schema)) new MySQLSchemata(_schema);
				schema = Global.GLB_SchemataList[_schema];
			}
			else
				schema = Global.GLB_SchemataList[Global.GLB_DefaultSchemata];

			if (!schema.Tables.ContainsKey(_table)) throw new ArgumentException("Table doesn't exists: " + _table);

			foreach (string s in _fields)
				if (!schema.Tables[_table].Columns.ContainsKey(s)) throw new ArgumentOutOfRangeException("column", "Column fields specified, doesn't exists.");
		}

		public string GenerateQueryString()
		{
			if (isDisposed) throw new ObjectDisposedException("Update");
			if (_finalQueryString.Length > 0) goto label1;
			if (_table.Length < 1) throw new ArgumentNullException("There is no table assigned", new Exception());
			if (_values.Count < 1) throw new ArgumentNullException("There is no data", new Exception());
			if (_fields.Count != _values.Count) throw new ArgumentOutOfRangeException("The range of fields is not correspond with the values.", new Exception());

			Check();

			_queryString.Append(string.Format("UPDATE `{0}` SET ", _table));
			string tempString;

			for (int i = 0; i < _fields.Count; i++)
			{
				tempString = "`" + _fields[i] + "`=@_" + _fields[i] + "_" + i + ",";
				_parameters.Add("@_" + _fields[i] + "_" + i, _values[i]);
				_queryString.Append(tempString);
			}

			tempString = Condition.Length > 0 ? _queryString.Remove(_queryString.Length - 1, 1).Append(string.Format(" WHERE {0}", Condition)).ToString() : _queryString.Remove(_queryString.Length - 1, 1).ToString();

			int paramCount = tempString.Split('@').Length;
			if (paramCount - 1 > _parameters.Count) throw new ArgumentOutOfRangeException("Parameters", "Parameter supplied doesn't match.");

			_finalQueryString = tempString;
			tempString = null;

		label1:
			return _finalQueryString;
		}

		public int Execute()
		{
			return Execute(string.Empty);
		}

		public int Execute(string connectionString)
		{
			if (isDisposed) throw new ObjectDisposedException("Update");

			if (_executed)
				return _query.Execute(_parameters);

			_query = new MySQLQuery(GenerateQueryString(), connectionString) { Schema = _schema };
			_executed = true;
			return ExecutionResult = _query.Execute(_parameters);
		}

		public string Table
		{
			set
			{
				if (isDisposed) throw new ObjectDisposedException("Update");
				_table = value;
			}
		}

		public string Schema
		{
			set
			{
				if (isDisposed) throw new ObjectDisposedException("Update");
				_schema = value;
			}
		}

		public bool PreparedStatement { get; private set; }

		public int ExecutionResult { get; private set; }
	}
}
