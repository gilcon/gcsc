﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace GCSC.Data.MySQL.DML
{
	public sealed class MySQLSelect : Interface.Data.ISelect
	{
		/// <summary>
		/// used for incrementing when adding value without specifing the parameters
		/// </summary>
		private int _paramIncrement = 0;

		private int _rowCount, _fieldCount;
		private string _customConnectionString;
		private object[][] _data;
		private List<IDataRecord> _dataRecordList;
		private object _scalarData;
		private Dictionary<string, object> _paramters;
		private bool _refresherFlag;

		public MySQLSelect()
		{
			Command = string.Empty;
			_customConnectionString = string.Empty;
			_paramters = new Dictionary<string, object>();
			Schema = string.Empty;
			_data = null;
			_dataRecordList = null;
			_scalarData = null;
			_refresherFlag = false;
		}

		public MySQLSelect(string command)
		{
			Command = command;
			_customConnectionString = string.Empty;
			_paramters = new Dictionary<string, object>();
			Schema = string.Empty;
			_data = null;
			_scalarData = null;
			_refresherFlag = false;
		}

		public MySQLSelect(string command, string connectionString)
		{
			Command = command;
			_customConnectionString = connectionString;
			_paramters = new Dictionary<string, object>();
			Schema = string.Empty;
			_data = null;
			_scalarData = null;
			_refresherFlag = false;
		}

		private bool CheckParamLenght()
		{
			int l = Command.Split('@').Length;

			if (l - 1 > _paramters.Count)
				return false;
			return true;
		}

		public void Clear()
		{
			Command = null;
			_customConnectionString = null;
			_paramters = null;
			Schema = null;
			_data = null;
			_scalarData = null;
			_refresherFlag = false;
			_rowCount = _fieldCount = 0;
		}

		public void RefreshReader()
		{
			_refresherFlag = true;
			_rowCount = 0;
			Reader();
			_refresherFlag = false;
		}

		public void RefreshScalar()
		{
			_refresherFlag = true;
			_rowCount = 0;
			Scalar();
			_refresherFlag = false;
		}

		public void AddParamWithValue(object value)
		{
			AddParamWithValue("__p" + _paramIncrement++, value);
		}

		public void AddParamWithValue(string paramName, object value)
		{
			_paramters.Add(paramName.Trim(), value);
		}

		public object Scalar()
		{
			if (_scalarData != null && !_refresherFlag) return _scalarData;
			if (!CheckParamLenght()) throw new ArgumentOutOfRangeException("Paramter");

			using (var cn = new MySqlConnection())
			{
				if (_customConnectionString.Length > 0)
					cn.ConnectionString = _customConnectionString;
				else
					using (var sc = new MySQLSecuredConnectionString())
						cn.ConnectionString = sc.GetConnectionString();

				using (var cmd = new MySqlCommand(Command, cn) { CommandTimeout = MySQLConnectionString.GlobalCommandTimeOut })
				{
					foreach (var kv in _paramters)
						cmd.Parameters.AddWithValue("@" + kv.Key, kv.Value);
					cn.Open();
					if (Schema.Length > 0) cn.ChangeDatabase(Schema);

					_rowCount = 1;
					return _scalarData = cmd.ExecuteScalar();
				}
			}
		}

		public object[][] Reader()
		{
			if (_data != null && !_refresherFlag) return _data;
			if (!CheckParamLenght()) throw new ArgumentOutOfRangeException("Paramters");

			var _tempData = new HashSet<object[]>();
			foreach (var item in DataReader())
			{
				var dataStorage = new object[_fieldCount];
				for (int i = 0; i < _fieldCount; i++)
					dataStorage[i] = item[i];
				_tempData.Add(dataStorage);
				_rowCount++;
			}
			_data = _tempData.ToArray();
			return _data;
		}

		public IEnumerable<IDataRecord> DataReader()
		{
			if (_dataRecordList != null && !_refresherFlag)
			{
				foreach (var item in _dataRecordList) yield return item;
			}

			if (!CheckParamLenght()) throw new ArgumentOutOfRangeException("Paramters");

			using (var cn = new MySqlConnection())
			{
				if (_customConnectionString.Length > 0)
					cn.ConnectionString = _customConnectionString;
				else
					using (var sc = new MySQLSecuredConnectionString())
						cn.ConnectionString = sc.GetConnectionString();

				using (var cmd = new MySqlCommand(Command, cn) { CommandTimeout = MySQLConnectionString.GlobalCommandTimeOut })
				{
					foreach (var kv in _paramters)
						cmd.Parameters.AddWithValue("@" + kv.Key, kv.Value);

					cn.Open();
					if (Schema.Length > 0) cn.ChangeDatabase(Schema);
					if (TableDirect) cmd.CommandType = CommandType.TableDirect;
					using (var reader = cmd.ExecuteReader())
					{
						_fieldCount = reader.FieldCount;
						_dataRecordList = new List<IDataRecord>();
						while (reader.Read())
						{
							_dataRecordList.Add(reader);
							yield return reader;
						}
					}
				}
			}
		}

		public string Command { get; set; }

		public int FielCount
		{
			get
			{
				if (_fieldCount < 1)
					Reader();
				return _fieldCount;
			}
		}

		public int RowCount
		{
			get
			{
				if (_rowCount < 1)
					Reader();
				return _rowCount;
			}
		}

		public string Schema { get; set; }

		public bool TableDirect { get; set; }

		public bool PreparedStatement
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}
	}
}
