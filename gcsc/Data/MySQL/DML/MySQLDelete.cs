﻿using System;
using System.Collections.Generic;

namespace GCSC.Data.MySQL.DML
{
	public sealed class MySQLDelete : MySQLConditions, IDisposable, GCSC.Interface.Data.DML.IDelete
	{
		private Dictionary<string, object> _parameters;
		private string _schema, _table, _finalQueryString;

		/// <summary>
		/// used for incrementing when adding value without specifing the parameters
		/// </summary>
		private int _paramIncrement = 0;

		private MySQLQuery _query;

		private bool isDisposed, _executed;

		public MySQLDelete()
			: this(false)
		{
		}

		public MySQLDelete(bool isPrepared)
		{
			_parameters = new Dictionary<string, object>();
			_schema = string.Empty;
			_table = string.Empty;
			_finalQueryString = string.Empty;
			Condition = string.Empty;
			PreparedStatement = isPrepared;
		}

		~MySQLDelete()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public void Dispose(bool isDisposing)
		{
			if (!isDisposed)
			{
				if (isDisposing)
				{
					_parameters = null;
					_schema = null;
					_table = null;
					_finalQueryString = null;
					Condition = null;
					if (_query != null) _query.Dispose();
				}
			}

			isDisposed = true;
		}

		public void AddParamWithValue(object value)
		{
			AddParamWithValue("__p" + _paramIncrement++, value);
		}

		public void AddParamWithValue(string paramName, object value)
		{
			if (isDisposed) throw new ObjectDisposedException("Delete");
			_parameters.Add("@" + paramName, value);
		}

		public void ModifyParamWithValue(string paramName, object value)
		{
			if (isDisposed) throw new ObjectDisposedException("Delete");
			if (!PreparedStatement || !_executed)
				throw new InvalidOperationException("Prepared statement not allowed or no primary execution executed.");
			_parameters["@" + paramName] = value;
		}

		public int ExecuteWithModifiedValue(Dictionary<string, object> parameterNewValue)
		{
			if (isDisposed) throw new ObjectDisposedException("Delete");
			if (!PreparedStatement || !_executed)
				throw new InvalidOperationException("Prepared statement not allowed or no primary execution executed.");

			foreach (var kv in parameterNewValue)
				_parameters["@" + kv.Key] = kv.Value;
			return Execute();
		}

		public string GenerateQueryString(bool rebuildQueryString = false)
		{
			if (isDisposed) throw new ObjectDisposedException("Delete");
			if (_table.Length < 0) throw new ArgumentNullException("There is no table assigned", new Exception());
			if (rebuildQueryString) _finalQueryString = string.Empty;
			if (_finalQueryString.Length > 0) goto label1;

			MySQL.Schema.MySQLSchemata schema;
			if (_schema.Length > 0)
			{
				if (!Global.GLB_SchemataList.ContainsKey(_schema)) new MySQL.Schema.MySQLSchemata(_schema);
				schema = Global.GLB_SchemataList[_schema];
			}
			else
				schema = Global.GLB_SchemataList[Global.GLB_DefaultSchemata];

			if (!schema.Tables.ContainsKey(_table)) throw new ArgumentException("Table doesn't exists: " + _table);

			if (Condition.Length < 0)
				_finalQueryString = "DELETE FROM `" + _table + "`";
			else
			{
				int paramCount = Condition.Split('@').Length;
				if (paramCount - 1 > _parameters.Count) throw new ArgumentOutOfRangeException("Parameters", "Parameter supplied doesn't match.");
				_finalQueryString = "DELETE FROM `" + _table + "` WHERE " + Condition;
			}

		label1:
			return _finalQueryString;
		}

		public int Execute()
		{
			return Execute(string.Empty);
		}

		public int Execute(string connectionString)
		{
			if (isDisposed) throw new ObjectDisposedException("Delete");

			if (_executed)
				return ExecutionResult = _query.Execute(_parameters);

			_query = new MySQLQuery(GenerateQueryString(), connectionString, PreparedStatement) { Schema = _schema };
			_executed = true;
			return ExecutionResult = _query.Execute(_parameters);
		}

		public string Table
		{
			set
			{
				if (isDisposed) throw new ObjectDisposedException("Delete");
				_table = value;
			}
		}

		public string Schema
		{
			set
			{
				if (isDisposed) throw new ObjectDisposedException("Delete");
				_schema = value;
			}
		}

		public bool PreparedStatement { get; private set; }

		public int ExecutionResult { get; private set; }
	}
}
