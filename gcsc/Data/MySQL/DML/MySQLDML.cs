﻿using System;
using System.Collections.Generic;

namespace GCSC.Data.MySQL.DML
{
	public static class MySQLDML
	{
		#region Delete

		public static int Delete(string tableName, string whereClause, Dictionary<string, object> parameterWithValue = null)
		{
			return Delete(string.Empty, tableName, whereClause, parameterWithValue);
		}

		public static int Delete(string schemaName, string tableName, string whereClause, Dictionary<string, object> parameterWithValue = null)
		{
			using (var d = new MySQLDelete() { Schema = schemaName, Table = tableName, Condition = whereClause })
			{
				if (parameterWithValue != null)
					foreach (var kv in parameterWithValue)
						d.AddParamWithValue(kv.Key, kv.Value);

				return d.Execute();
			}
		}

		public static int Delete(string tableName, MySQLConditions condition, Dictionary<string, object> parameterWithValue)
		{
			return Delete(string.Empty, tableName, condition, parameterWithValue);
		}

		public static int Delete(string schemaName, string tableName, MySQLConditions condition, Dictionary<string, object> parameterWithValue)
		{
			using (var d = new MySQLDelete() { Schema = schemaName, Table = tableName, AssignConditions = condition })
			{
				foreach (var kv in parameterWithValue)
					d.AddParamWithValue(kv.Key, kv.Value);
				return d.Execute();
			}
		}

		#endregion Delete

		#region Insert

		public static int Insert(string tableName, params object[] data)
		{
			return Insert(string.Empty, tableName, data);
		}

		public static int Insert(string schemaName, string tableName, params object[] data)
		{
			using (var i = new MySQLInsert() { Table = tableName, Schema = schemaName })
			{
				i.Values(data);
				return i.Execute();
			}
		}

		public static int Insert(string tableName, params object[][] data)
		{
			return Insert(string.Empty, tableName, data);
		}

		public static int Insert(string schemaName, string tableName, params object[][] data)
		{
			using (var i = new MySQLInsert() { Schema = schemaName, Table = tableName })
			{
				Array.ForEach(data, i.Values);
				return i.Execute();
			}
		}

		public static int Insert(string tableName, string[] fields, params object[] data)
		{
			return Insert(string.Empty, tableName, fields, data);
		}

		public static int Insert(string schemaName, string tableName, string[] fields, params object[] data)
		{
			using (var i = new MySQLInsert() { Table = tableName, Schema = schemaName })
			{
				i.Fields(fields);
				i.Values(data);
				return i.Execute();
			}
		}

		public static int Insert(string tableName, string[] fields, params object[][] data)
		{
			return Insert(string.Empty, tableName, fields, data);
		}

		public static int Insert(string schemaName, string tableName, string[] fields, params object[][] data)
		{
			using (var i = new MySQLInsert() { Schema = schemaName, Table = tableName })
			{
				if (fields != null)
					i.Fields(fields);
				Array.ForEach(data, i.Values);
				return i.Execute();
			}
		}

		#endregion Insert

		#region Select

		public static object[][] Select(string command)
		{
			return Select(string.Empty, command);
		}

		public static object[][] Select(string schemaName, string command)
		{
			var s = new MySQLSelect(command) { Schema = schemaName };
			try
			{
				return s.Reader();
			}
			finally
			{
				s.Clear();
			}
		}

		public static object[][] Select(string command, Dictionary<string, object> parameterWithValue = null)
		{
			return Select(string.Empty, command, parameterWithValue);
		}

		public static object[][] Select(string schemaName, string command, Dictionary<string, object> paramterWithValue = null)
		{
			var s = new MySQLSelect(command) { Schema = schemaName };
			try
			{
				if (paramterWithValue != null)
					foreach (var kv in paramterWithValue)
						s.AddParamWithValue(kv.Key, kv.Value);
				return s.Reader();
			}
			finally
			{
				s.Clear();
			}
		}

		#endregion Select

		#region SelectScalar

		public static object SelectScalar(string command)
		{
			return SelectScalar(string.Empty, command);
		}

		public static object SelectScalar(string schemaName, string command)
		{
			var s = new MySQLSelect(command) { Schema = schemaName };
			try
			{
				return s.Scalar();
			}
			finally
			{
				s.Clear();
			}
		}

		public static object SelectScalar(string command, Dictionary<string, object> parameterWithValue = null)
		{
			return SelectScalar(string.Empty, command, parameterWithValue);
		}

		public static object SelectScalar(string schemaName, string command, Dictionary<string, object> paramterWithValue = null)
		{
			var s = new MySQLSelect(command) { Schema = schemaName };
			try
			{
				if (paramterWithValue != null)
					foreach (var kv in paramterWithValue)
						s.AddParamWithValue(kv.Key, kv.Value);
				return s.Scalar();
			}
			finally
			{
				s.Clear();
			}
		}

		#endregion SelectScalar

		#region SelectRowCount

		public static int SelectRowCount(string command)
		{
			return SelectRowCount(string.Empty, command);
		}

		public static int SelectRowCount(string schemaName, string command)
		{
			var s = new MySQLSelect(command) { Schema = schemaName };
			try
			{
				return s.RowCount;
			}
			finally
			{
				s.Clear();
			}
		}

		public static int SelectRowCount(string command, Dictionary<string, object> parameterWithValue = null)
		{
			return SelectRowCount(string.Empty, command, parameterWithValue);
		}

		public static int SelectRowCount(string schemaName, string command, Dictionary<string, object> paramterWithValue = null)
		{
			var s = new MySQLSelect(command) { Schema = schemaName };
			try
			{
				if (paramterWithValue != null)
					foreach (var kv in paramterWithValue)
						s.AddParamWithValue(kv.Key, kv.Value);
				return s.RowCount;
			}
			finally
			{
				s.Clear();
			}
		}

		#endregion SelectRowCount

		#region SelectTableDirect

		public static object[][] SelectTableDirect(string tableName)
		{
			return SelectTableDirect(string.Empty, tableName);
		}

		public static object[][] SelectTableDirect(string schemaName, string tableName)
		{
			var s = new MySQLSelect(tableName) { Schema = schemaName, TableDirect = true };
			try
			{
				return s.Reader();
			}
			finally
			{
				s.Clear();
			}
		}

		#endregion SelectTableDirect

		#region StoredProcedure

		public static int StoredProcedure(string procedureName, Dictionary<string, object> parameterWithValue)
		{
			return StoredProcedure(string.Empty, procedureName, parameterWithValue);
		}

		public static int StoredProcedure(string schemaName, string procedureName, Dictionary<string, object> parameterWithValue)
		{
			using (var s = new MySQLStoredProcedure(procedureName) { Schema = schemaName })
			{
				foreach (var kv in parameterWithValue)
					s.Add(kv.Key, kv.Value);
				return s.Execute();
			}
		}

		public static int StoredProcedure(string procedureName, ref Dictionary<string, object> parameterWithValue)
		{
			return StoredProcedure(string.Empty, procedureName, ref parameterWithValue);
		}

		public static int StoredProcedure(string schemaName, string procedureName, ref Dictionary<string, object> parameterWithValue)
		{
			using (var s = new MySQLStoredProcedure(procedureName) { Schema = schemaName })
			{
				string[] ss = new string[parameterWithValue.Count];
				parameterWithValue.Keys.CopyTo(ss, 0);

				foreach (var kv in parameterWithValue)
					s.Add(kv.Key, kv.Value);
				int i = s.Execute();
				parameterWithValue.Clear();
				foreach (var kv in ss)
					parameterWithValue[kv] = s.ParamReturnValue(kv);
				return i;
			}
		}

		public static int StoredProcedure(string procedureName, params object[] data)
		{
			return StoredProcedure(string.Empty, procedureName, data);
		}

		public static int StoredProcedure(string schemaName, string procedureName, params object[] data)
		{
			using (var s = new MySQLStoredProcedure(procedureName) { Schema = schemaName })
			{
				s.Add(data);
				return s.Execute();
			}
		}

		#endregion StoredProcedure

		#region StoredProcedureReader

		public static object[][] StoredProcedureReader(string procedureName, Dictionary<string, object> parameterWithValue)
		{
			return StoredProcedureReader(string.Empty, procedureName, parameterWithValue);
		}

		public static object[][] StoredProcedureReader(string schemaName, string procedureName, Dictionary<string, object> parameterWithValue)
		{
			using (var s = new MySQLStoredProcedure(procedureName) { Schema = schemaName, ExecutionType = GCSC.Data.ExecutionType.ExecuteReader })
			{
				foreach (var kv in parameterWithValue)
					s.Add(kv.Key, kv.Value);
				return s.Read();
			}
		}

		public static object[][] StoredProcedureReader(string procedureName, params object[] data)
		{
			return StoredProcedureReader(string.Empty, procedureName, data);
		}

		public static object[][] StoredProcedureReader(string schemaName, string procedureName, params object[] data)
		{
			using (var s = new MySQLStoredProcedure(procedureName) { Schema = schemaName, ExecutionType = GCSC.Data.ExecutionType.ExecuteReader })
			{
				s.Add(data);
				return s.Read();
			}
		}

		#endregion StoredProcedureReader

		#region Truncate

		public static int Truncate(string tableName)
		{
			return Truncate(string.Empty, tableName);
		}

		public static int Truncate(string schemaName, string tableName)
		{
			using (var t = new MySQLTruncate() { Schema = schemaName, Table = tableName })
			{
				return t.Execute();
			}
		}

		#endregion Truncate

		#region Update

		public static int Update(string tableName, string whereClause, string[] fields, Dictionary<string, object> parameterWithValue = null, params object[] values)
		{
			return Update(string.Empty, tableName, whereClause, fields, parameterWithValue, values);
		}

		public static int Update(string schemanName, string tableName, string whereClause, string[] fields, Dictionary<string, object> parameterWithValue = null, params object[] values)
		{
			using (var u = new MySQLUpdate() { Schema = schemanName, Table = tableName, Condition = whereClause })
			{
				u.Fields(fields);
				u.Values(values);
				if (parameterWithValue != null)
					foreach (var kv in parameterWithValue)
						u.AddParamWithValue(kv.Key, kv.Value);
				return u.Execute();
			}
		}

		public static int Update(string tableName, MySQLConditions condition, Dictionary<string, object> parameterWithValue, string[] fields, params object[] values)
		{
			return Update(string.Empty, tableName, condition, parameterWithValue, fields, values);
		}

		public static int Update(string schemaName, string tableName, MySQLConditions condition, Dictionary<string, object> parameterWithValue, string[] fields, params object[] values)
		{
			using (var u = new MySQLUpdate() { Schema = schemaName, Table = tableName, AssignConditions = condition })
			{
				u.Fields(fields);
				u.Values(values);
				foreach (var kv in parameterWithValue)
					u.AddParamWithValue(kv.Key, kv.Value);
				return u.Execute();
			}
		}

		#endregion Update
	}
}
