﻿using GCSC.Data.MySQL.Schema;
using System;
using System.Collections.Generic;
using System.Text;

namespace GCSC.Data.MySQL.DML
{
	public sealed class MySQLInsert : IDisposable, GCSC.Interface.Data.DML.IInsert
	{
		private bool isDisposed, _executed;
		private StringBuilder _queryString;
		private string _finalQueryString, _schema, _table;
		private int? _valueCount;
		private readonly List<string> _fields;
		private readonly List<object> _values;
		private Dictionary<string, object> _parameters;
		private MySQLSchemata schema;
		private MySQLQuery _query;
		private List<object> _modifiedItemList;

		public MySQLInsert()
			: this(false)
		{
		}

		public MySQLInsert(bool isPrepared)
		{
			_schema = string.Empty;
			_table = string.Empty;
			_queryString = new StringBuilder();
			_finalQueryString = string.Empty;
			_valueCount = -1;
			_fields = new List<string>();
			_values = new List<object>();
			_parameters = new Dictionary<string, object>();
			_modifiedItemList = null;
			IsFullTable = false;
			PreparedStatement = isPrepared;
		}

		~MySQLInsert()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool isDisposing)
		{
			if (!isDisposed)
				if (isDisposing)
				{
					_queryString = null;
					_finalQueryString = null;
					_schema = null;
					_table = null;
					_valueCount = null;
					_parameters = null;
					IsFullTable = false;
					schema = null;
					if (_query != null) _query.Dispose();
					_modifiedItemList = null;
				}

			isDisposed = true;
		}

		public void Fields(params string[] columns)
		{
			if (isDisposed) throw new ObjectDisposedException("Insert");
			Array.ForEach(columns, c => _fields.Add(c.Trim()));
		}

		public void Values(params object[] values)
		{
			if (isDisposed) throw new ObjectDisposedException("Insert");
			if (_valueCount > 0 && _valueCount != values.Length) throw new ArgumentOutOfRangeException("values");

			_valueCount = values.Length;
			_values.AddRange(values);
		}

		public void ModifyValues(params object[] values)
		{
			if (isDisposed) throw new ObjectDisposedException("Insert");
			if (!PreparedStatement || !_executed)
				throw new InvalidOperationException("Prepared statement not allowed or no primary execution executed.");
			_modifiedItemList = new List<object>(_parameters.Count);
			_modifiedItemList.AddRange(values);
		}

		public int ExecuteWithModifiedValue(params object[] values)
		{
			if (isDisposed) throw new ObjectDisposedException("Insert");
			if (!PreparedStatement || !_executed)
				throw new InvalidOperationException("Prepared statement not allowed or no primary execution executed.");
			if (_parameters.Count != values.Length) throw new ArgumentOutOfRangeException("values", "Total numbers of values not equal to numbers of paramters");

			int counter = 0;
			string[] ss = new string[_parameters.Count];
			_parameters.Keys.CopyTo(ss, 0);
			foreach (var s in ss)
				_parameters[s] = values[counter++];
			ss = null;
			return Execute();
		}

		private void Check()
		{
			if (_schema.Length > 0)
			{
				if (!Global.GLB_SchemataList.ContainsKey(_schema)) new MySQLSchemata(_schema);
				schema = Global.GLB_SchemataList[_schema];
			}
			else
				schema = Global.GLB_SchemataList[Global.GLB_DefaultSchemata];

			if (!schema.Tables.ContainsKey(_table)) throw new ArgumentException("Table doesn't exists: " + _table);

			if (_fields.Count > 0)
			{
				foreach (string s in _fields)
					if (!schema.Tables[_table].Columns.ContainsKey(s)) throw new ArgumentException("Column name doesn't exists: " + s);

				foreach (var kv in schema.Tables[_table].Columns)
					if (!kv.Value.IsNullable && kv.Value.ColumnDefault.Length < 1 && !kv.Value.IsAutoIncrement)
						if (!_fields.Contains(kv.Key)) throw new ArgumentNullException("Not Nullable and has no default value column found: " + kv.Key);
			}
			else
			{
				int? counter = 0;
				foreach (var kv in schema.Tables[_table].Columns)
					if (!kv.Value.IsNullable && !kv.Value.IsAutoIncrement) counter++;

				if (_valueCount < counter) throw new IndexOutOfRangeException("Value doesn't meet column requirement.");

				var unassignedColumn = new List<MySQLColumn>();
				counter = 0;

				foreach (var kv in schema.Tables[_table].Columns)
				{
					if (kv.Value.IsAutoIncrement)
						unassignedColumn.Add(kv.Value);
					else
					{
						_fields.Add(kv.Key);
						if (++counter >= _valueCount) break;
					}
				}

				counter = 0;
				if (_fields.Count < _valueCount)
					foreach (var c in unassignedColumn)
					{
						_fields.Insert((int)counter++, c.ColumnName);
						if (_fields.Count >= _valueCount) break;
					}

				foreach (var kv in schema.Tables[_table].Columns)
					if (!kv.Value.IsNullable && kv.Value.ColumnDefault.Length < 1 && !kv.Value.IsAutoIncrement)
						if (!_fields.Contains(kv.Key)) throw new ArgumentNullException("Not Nullable and has no default value column found: " + kv.Key);

				counter = null;
			}
		}

		public string GenerateQueryString()
		{
			if (isDisposed) throw new ObjectDisposedException("Insert");
			if (_finalQueryString.Length > 0) goto label2;
			if (_table.Length < 1) throw new ArgumentNullException("There is no table assigned.", new Exception());
			if (_values.Count < 1) throw new ArgumentNullException("There is no data.", new Exception());

			if (InsertIgnore)
			{
				_queryString.Append("INSERT IGNORE INTO `" + _table + "`()");
			}
			else
			{
				_queryString.Append("INSERT INTO `" + _table + "`()");
			}

			if (IsFullTable) goto label1;

			Check();

			if (_fields.Count > 0)
			{
				if (_fields.Count != _valueCount) throw new ArgumentOutOfRangeException("The range of fields does not correspond with the values.");
				Array.ForEach(_fields.ToArray(), f => _queryString.Insert(_queryString.Length - 1, "`" + f + "`,"));
				_queryString = _queryString.Remove(_queryString.Length - 2, 1);
			}
		label1:
			if (IsFullTable && schema.Tables[_table].Columns.Count != _valueCount) throw new ArgumentOutOfRangeException("Invalid values count");

			_queryString.Append(" VALUES");

			int? valueOccurance = _values.Count / _valueCount, counter = -1;
			string tempString, tempParamString;

			for (int i = 0; i < valueOccurance; i++)
			{
				tempString = string.Empty;
				for (int j = 0; j < _valueCount; j++)
				{
					tempParamString = _fields[j] + "_" + i;
					_parameters.Add("@" + tempParamString, _values[(int)++counter]);
					tempString += "@" + tempParamString + ",";
				}
				_queryString.Append("(" + tempString.Remove(tempString.Length - 1) + "),");
			}

			counter = valueOccurance = null;
			tempString = tempParamString = null;

			_finalQueryString = _queryString.Remove(_queryString.Length - 1, 1).ToString();
		label2:
			return _finalQueryString;
		}

		public int Execute()
		{
			return Execute(string.Empty);
		}

		public int Execute(string connectionString)
		{
			if (isDisposed) throw new ObjectDisposedException("Insert");

			if (_executed)
			{
				string[] ss = new string[_parameters.Count];
				if (_modifiedItemList != null)
				{
					int counter = 0;
					_parameters.Keys.CopyTo(ss, 0);
					foreach (var s in ss)
					{
						_parameters[s] = _modifiedItemList[counter++];
					}
				}
				ss = null;
				return ExecutionResult = _query.Execute(_parameters);
			}

			_query = new MySQLQuery(GenerateQueryString(), connectionString, PreparedStatement) { Schema = _schema };
			_executed = true;
			return ExecutionResult = _query.Execute(_parameters);
		}

		public string Table
		{
			set
			{
				if (isDisposed) throw new ObjectDisposedException("Insert");
				_table = value;
			}
		}

		public string Schema
		{
			set
			{
				if (isDisposed) throw new ObjectDisposedException("Insert");
				_schema = value;
			}
		}

		/// <summary>
		/// Not Implemented will cause an error
		/// </summary>
		public bool IsFullTable { get; set; }

		public bool PreparedStatement { get; private set; }

		public int ExecutionResult { get; private set; }

		public bool InsertIgnore { get; set; }
	}
}
