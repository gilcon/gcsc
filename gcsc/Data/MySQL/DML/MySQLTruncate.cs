﻿using GCSC.Data.MySQL.Schema;
using System;

namespace GCSC.Data.MySQL.DML
{
	public sealed class MySQLTruncate : IDisposable, GCSC.Interface.Data.DML.ITruncate
	{
		private bool isDisposed;
		private string _schema;
		private string _table;

		public MySQLTruncate()
		{
			_table = string.Empty;
			_schema = string.Empty;
		}

		~MySQLTruncate()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public void Dispose(bool isDisposing)
		{
			if (!isDisposed)
				if (isDisposing)
				{
					_schema = null;
					_table = null;
				}
			isDisposed = true;
		}

		public int Execute()
		{
			return Execute(string.Empty);
		}

		public int Execute(string connectionString)
		{
			if (isDisposed) throw new ObjectDisposedException("Truncate");
			if (_table.Length < 1) throw new ArgumentNullException("There is no table assigned.", new Exception());

			MySQLSchemata schema;
			if (_schema.Length > 0)
			{
				if (!Global.GLB_SchemataList.ContainsKey(_schema)) new MySQLSchemata(_schema);
				schema = Global.GLB_SchemataList[_schema];
			}
			else
				schema = Global.GLB_SchemataList[Global.GLB_DefaultSchemata];

			if (!schema.Tables.ContainsKey(_table)) throw new ArgumentException("Table doesn't exists: " + _table);

			using (var q = new MySQLQuery("TRUNCATE `" + _table + "`", connectionString) { Schema = _schema })
				return ExecutionResult = q.Execute();
		}

		public string Table
		{
			set
			{
				if (isDisposed) throw new ObjectDisposedException("Truncate");
				_table = value;
			}
		}

		public string Schema
		{
			set
			{
				if (isDisposed) throw new ObjectDisposedException("Truncate");
				_schema = value;
			}
		}

		public int ExecutionResult { get; private set; }
	}
}
