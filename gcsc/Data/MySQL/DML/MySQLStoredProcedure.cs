﻿using GCSC.Data.MySQL.Schema;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace GCSC.Data.MySQL.DML
{
	public sealed class MySQLStoredProcedure : IDisposable, GCSC.Interface.Data.DML.IStoredProcedure
	{
		private bool isDisposed;
		private string _schema, _sprocName;
		private short _namedParamters;
		private object[][] _readReturn;
		private int _executeNonQueryReturn, _executionCount;

		private Dictionary<int, object> _parametersNoName;
		private Dictionary<string, object> _parameters;
		private Dictionary<string, ParameterDirection> _direction;

		private MySQLRoutine _procedure;
		private MySQLSchemata schema;

		public MySQLStoredProcedure()
			: this(string.Empty) { }

		public MySQLStoredProcedure(string ProcedureName)
		{
			_schema = string.Empty;
			_sprocName = ProcedureName;

			ExecutionType = GCSC.Data.ExecutionType.Unknown;

			_parametersNoName = new Dictionary<int, object>();
			_parameters = new Dictionary<string, object>();
			_direction = new Dictionary<string, ParameterDirection>();
			_namedParamters = 1;
		}

		~MySQLStoredProcedure()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public void Dispose(bool isDisposing)
		{
			if (!isDisposed)
				if (isDisposing)
				{
					schema = null;
					_schema = null;
					_sprocName = null;

					_parametersNoName = null;
					_parameters = null;
					_direction = null;
					_namedParamters = 0;
					_readReturn = null;
					_executeNonQueryReturn = _executionCount = 0;
					_procedure = null;
				}

			isDisposed = true;
		}

		public void Add<T>(T item, bool track)
		{
			PropertyInfo[] pi = typeof(T).GetProperties();
			foreach (var p in pi)
			{
				Add(p.GetValue(item, null));
			}
		}

		public void Add(params object[] value)
		{
			if (isDisposed) throw new ObjectDisposedException("StoredProcedure");
			Array.ForEach(value, Add);
		}

		public void Add(object value)
		{
			if (isDisposed) throw new ObjectDisposedException("StoredProcedure");
			if (_namedParamters == 0) throw new ArgumentException("Function not allowed, because you already specified named parameters");

			_parametersNoName.Add(_namedParamters++ - 1, value);
		}

		public void Add(string parameterName, object value)
		{
			if (isDisposed) throw new ObjectDisposedException("StoredProcedure");
			if (_namedParamters > 1) throw new ArgumentException("Function not allowed, because you already specified unnamed parameters");

			_namedParamters = 0;
			_parameters.Add(parameterName.Trim().ToLower(), value);
		}

		public object ParamReturnValue(string ParamaterName)
		{
			if (isDisposed) throw new ObjectDisposedException("StoredProcedure");
			return _parameters[ParamaterName];
		}

		public object[][] Read(bool refresh = false)
		{
			using (var sc = new MySQLSecuredConnectionString())
				return Read(sc.GetConnectionString(), refresh);
		}

		public object[][] Read(string connectionString, bool refresh = false)
		{
			if (isDisposed) throw new ObjectDisposedException("StoredProcedure");
			if (ExecutionType != GCSC.Data.ExecutionType.ExecuteReader) throw new NotSupportedException("ExecuteReader not supported, please the execution type to ExecuteReader.");
			if (_executionCount == 0 || (_executionCount > 0 && refresh)) CoreExecute(connectionString);
			return _readReturn;
		}

		public int Execute(bool refresh = false)
		{
			using (var sc = new MySQLSecuredConnectionString())
				return Execute(sc.GetConnectionString(), refresh);
		}

		public int Execute(string connectionString, bool refresh = false)
		{
			if (isDisposed) throw new ObjectDisposedException("StoredProcedure");
			if (ExecutionType == GCSC.Data.ExecutionType.ExecuteReader) throw new NotSupportedException("Execution type is set to ExecuteReader");
			if (_executionCount == 0 || (_executionCount > 0 && refresh)) CoreExecute(connectionString);
			return _executeNonQueryReturn;
		}

		private void Check()
		{
			if (_schema.Length > 0)
			{
				if (!Global.GLB_SchemataList.ContainsKey(_schema)) new MySQLSchemata(_schema);
				schema = Global.GLB_SchemataList[_schema];
			}
			else
				schema = Global.GLB_SchemataList[Global.GLB_DefaultSchemata];

			if (!schema.StoredProcedures.ContainsKey(_sprocName)) throw new ArgumentException("Procedure doesn't exists: " + _sprocName);

			_procedure = schema.StoredProcedures[_sprocName];

			if (_namedParamters == 0)
			{
				foreach (string s in _parameters.Keys)
				{
					if (!_procedure.Parameters.ContainsKey(s)) throw new ArgumentException("Procedure parameter not found: " + s);
					_direction.Add(s, _procedure.Parameters[s].ParameterDirection);
				}

				foreach (string s in _procedure.Parameters.Keys.Except(_parameters.Keys))
				{
					if (_procedure.Parameters[s].ParameterDirection == ParameterDirection.Input ||
						_procedure.Parameters[s].ParameterDirection == ParameterDirection.InputOutput)
						throw new ArgumentNullException("Parameter", "Value required for the parameter - " + s);
					_parameters.Add(s, null);
					_direction.Add(s, ParameterDirection.Output);
				}
			}
			else
			{
				int counter = 0;
				foreach (var kv in _procedure.Parameters)
				{
					if (counter >= _parametersNoName.Count)
						if (kv.Value.ParameterDirection == ParameterDirection.Input ||
								kv.Value.ParameterDirection == ParameterDirection.InputOutput)
							throw new ArgumentNullException("Parameter", "Please provide valid data for the parameter - " + kv.Key);

					_parameters.Add(kv.Key, counter < _parametersNoName.Count ? _parametersNoName[counter] : null);
					_direction.Add(kv.Key, kv.Value.ParameterDirection);
					counter++;
				}
			}
		}

		private void CoreExecute(string connectionString)
		{
			if (isDisposed) throw new ObjectDisposedException("StoredProcedure");
			if (_sprocName.Length < 1) throw new ArgumentNullException("ProcedureName", "No procedure name defined.");
			if (_executionCount > 0 && !AllowMultipleExecution) throw new ArgumentException("Does not allowed to execute more than one.");
			if (ExecutionType == GCSC.Data.ExecutionType.ExecuteScalar) throw new NotSupportedException("ExecuteScalar is not supported.");
			_executionCount++;

			Check();

			using (var cn = new MySqlConnection(connectionString))
			{
				using (var cmd = new MySqlCommand(_sprocName, cn)
				{
					CommandType = CommandType.StoredProcedure,
					CommandTimeout = MySQLConnectionString.GlobalCommandTimeOut
				})
				{
					foreach (var kv in _parameters)
						cmd.Parameters.AddWithValue(kv.Key, kv.Value).Direction = _direction[kv.Key];

					cn.Open();
					if (_schema.Length > 0) cn.ChangeDatabase(_schema);

					if (ExecutionType == GCSC.Data.ExecutionType.Unknown || ExecutionType == GCSC.Data.ExecutionType.ExecuteNonQuery)
					{
						ExecutionType = GCSC.Data.ExecutionType.ExecuteNonQuery;
						_executeNonQueryReturn = cmd.ExecuteNonQuery();

						foreach (var kv in _procedure.Parameters.
							Where(p => p.Value.ParameterDirection == ParameterDirection.InputOutput
								|| p.Value.ParameterDirection == ParameterDirection.Output))
							_parameters[kv.Key] = cmd.Parameters[kv.Key].Value;
					}
					else if (ExecutionType == GCSC.Data.ExecutionType.ExecuteReader)
					{
						var reader = cmd.ExecuteReader();

						var _tempData = new HashSet<object[]>();
						var _fieldCount = reader.FieldCount;
						while (reader.Read())
						{
							var dataStorage = new object[_fieldCount];
							for (int i = 0; i < _fieldCount; i++)
								dataStorage[i] = reader[i];
							_tempData.Add(dataStorage);
						}
						reader.Dispose();
						_readReturn = _tempData.ToArray();
					}
				}
			}
		}

		public string ProcedureName
		{
			set
			{
				if (isDisposed) throw new ObjectDisposedException("StoredProcedure");
				_sprocName = value;
			}
		}

		public string Schema
		{
			set
			{
				if (isDisposed) throw new ObjectDisposedException("StoredProcedure");
				_schema = value;
			}
		}

		public GCSC.Data.ExecutionType ExecutionType { get; set; }

		public bool AllowMultipleExecution { get; set; }

		public bool PreparedStatement
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}

		public int ExecutionResult
		{
			get
			{
				return _executeNonQueryReturn;
			}
		}
	}
}
