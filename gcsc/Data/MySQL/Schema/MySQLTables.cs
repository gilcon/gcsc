﻿using GCSC.Data.MySQL.DML;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GCSC.Data.MySQL.Schema
{
	internal sealed class MySQLTables
	{
		public MySQLTables(string schema)
		{
			TableLists = new List<MySQLTable>();

			var s = new MySQLSelect("SELECT `TABLE_NAME`, `TABLE_TYPE`, `TABLE_COLLATION` FROM `information_schema`.`TABLES` WHERE `TABLE_SCHEMA` = @schema");
			s.AddParamWithValue("schema", schema);
			Array.ForEach(s.Reader(), r => TableLists.Add(new MySQLTable(r, schema)));
			s.Clear();
		}

		public void Add(MySQLTable table)
		{
			TableLists.Add(table);
		}

		public MySQLTable GetTable(string tableName)
		{
			return (MySQLTable)TableLists.Where(table => table.TableName == tableName && table.TableType == "BASE TABLE");
		}

		public MySQLTable GetView(string viewName)
		{
			return (MySQLTable)TableLists.Where(table => table.TableName == viewName && table.TableType == "VIEW");
		}

		public List<MySQLTable> GetTables
		{
			get { return new List<MySQLTable>(TableLists.Where(table => table.TableType == "BASE TABLE")); }
		}

		public List<MySQLTable> GetViews
		{
			get { return new List<MySQLTable>(TableLists.Where(table => table.TableType == "VIEW")); }
		}

		public List<string> TableListsString
		{
			get
			{
				var l = new List<string>();

				foreach (var s in TableLists.Where(table => table.TableType == "BASE TABLE"))
					l.Add(s.TableName);
				return l;
			}
		}

		public List<string> ViewListString
		{
			get
			{
				var l = new List<string>();

				foreach (var s in TableLists.Where(table => table.TableType == "VIEW"))
					l.Add(s.TableName);
				return l;
			}
		}

		public List<MySQLTable> TableLists { get; internal set; }
	}
}
