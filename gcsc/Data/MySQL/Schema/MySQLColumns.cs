﻿using GCSC.Data.MySQL.DML;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GCSC.Data.MySQL.Schema
{
	internal sealed class MySQLColumns
	{
		public MySQLColumns(string tableName, string schema)
		{
			ColumnList = new List<MySQLColumn>();

			var s = new MySQLSelect()
						{
							Command = "SELECT `TABLE_NAME`, `COLUMN_NAME`, `ORDINAL_POSITION`, `COLUMN_DEFAULT`, `IS_NULLABLE`, `DATA_TYPE`, `CHARACTER_MAXIMUM_LENGTH`, `NUMERIC_PRECISION`, `NUMERIC_SCALE`,`COLLATION_NAME` ,`COLUMN_KEY`, `EXTRA` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = @schema AND `TABLE_NAME` = @table_name GROUP BY `ORDINAL_POSITION` ASC"
						};
			s.AddParamWithValue("schema", schema);
			s.AddParamWithValue("table_name", tableName);

			Array.ForEach(s.Reader(), r => ColumnList.Add(new MySQLColumn(r, schema)));
			s.Clear();
		}

		public void Add(MySQLColumn column)
		{
			ColumnList.Add(column);
		}

		public MySQLColumn GetColumn(string columnName)
		{
			return (MySQLColumn)ColumnList.Where(column => column.ColumnName == columnName);
		}

		public List<MySQLColumn> ColumnList { get; internal set; }
	}
}
