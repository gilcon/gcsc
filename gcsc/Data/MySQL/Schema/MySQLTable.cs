using System.Collections.Generic;

namespace GCSC.Data.MySQL.Schema
{
	public class MySQLTable
	{
		public MySQLTable(object[] tableProperties, string schema)
		{
			Columns = new Dictionary<string, MySQLColumn>();
			TableSchema = schema;
			TableName = tableProperties[0].ToString();
			TableType = tableProperties[1].ToString();
			TableCollation = tableProperties[2].ToString();

			new MySQLColumns(TableName, schema).ColumnList.ForEach(cn => Columns.Add(cn.ColumnName, cn));
		}

		public Dictionary<string, MySQLColumn> Columns { get; internal set; }

		public string TableSchema { get; internal set; }

		public string TableName { get; internal set; }

		public string TableType { get; internal set; }

		public string TableCollation { get; internal set; }
	}
}
