﻿using GCSC.Util;
using System;

namespace GCSC.Data.MySQL.Schema
{
	public class MySQLColumn
	{
		public MySQLColumn(object[] columnProperties, string schema)
		{
			TableSchema = schema;
			TableName = columnProperties[0].ToString().Trim().ToLower();
			ColumnName = columnProperties[1].ToString().Trim().ToLower();
			OrdinalPosition = columnProperties[2].ToInt();
			ColumnDefault = columnProperties[3].ToString().Trim().ToLower();
			IsNullable = columnProperties[4].ToString().Trim().ToLower().Contains("yes");
			DataType = columnProperties[5].ToString().Trim().ToLower();
			CharacterMaximumLength = columnProperties[6].ToInt64();
			NumericPrecision = columnProperties[7].ToInt64();
			NumericScale = columnProperties[8].ToInt64();
			CollationName = columnProperties[9].ToString();
			ColumnKey = columnProperties[10].ToString().Trim().ToLower();
			IsAutoIncrement = columnProperties[11].ToString().Trim().ToLower().Contains("auto_increment");
		}

		public string TableSchema { get; internal set; }

		public string TableName { get; internal set; }

		public string ColumnName { get; internal set; }

		public int OrdinalPosition { get; internal set; }

		public string ColumnDefault { get; internal set; }

		public bool IsNullable { get; internal set; }

		public string DataType { get; internal set; }

		public Int64 CharacterMaximumLength { get; internal set; }

		public Int64 NumericPrecision { get; internal set; }

		public Int64 NumericScale { get; internal set; }

		public string ColumnKey { get; internal set; }

		public bool IsAutoIncrement { get; internal set; }

		public string CollationName { get; internal set; }
	}
}
