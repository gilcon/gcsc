﻿using GCSC.Data.MySQL.DML;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GCSC.Data.MySQL.Schema
{
	internal sealed class MySQLRoutines
	{
		public MySQLRoutines(string schema)
		{
			RoutineLists = new List<MySQLRoutine>();

			var s = new MySQLSelect()
						{
							Command = "SELECT `ROUTINE_NAME`, `ROUTINE_TYPE`, `DATA_TYPE`, `CHARACTER_MAXIMUM_LENGTH`, `NUMERIC_PRECISION`, `NUMERIC_SCALE` FROM `information_schema`.`ROUTINES` WHERE `ROUTINE_SCHEMA` = @schema"
						};
			s.AddParamWithValue("schema", schema);

			Array.ForEach(s.Reader(), r =>
			{
				var routine = new MySQLRoutine(r, schema);
				RoutineLists.Add(routine);
				MaxParamCount = MaxParamCount >= routine.ParametersCount ? MaxParamCount : routine.ParametersCount;
				MinParamCount = MinParamCount <= routine.ParametersCount ? MinParamCount : routine.ParametersCount;
			});
		}

		public MySQLRoutine GetProcedure(string procedureName)
		{
			return (MySQLRoutine)RoutineLists.Where(proc => proc.RoutineType == "PROCEDURE" && proc.RoutineName == procedureName);
		}

		public MySQLRoutine GetFunction(string functionName)
		{
			return (MySQLRoutine)RoutineLists.Where(func => func.RoutineType == "FUNCTION" && func.RoutineName == functionName);
		}

		public List<MySQLRoutine> GetProcedures
		{
			get { return new List<MySQLRoutine>(RoutineLists.Where(proc => proc.RoutineType == "PROCEDURE")); }
		}

		public List<MySQLRoutine> GetFunctions
		{
			get { return new List<MySQLRoutine>(RoutineLists.Where(func => func.RoutineType == "FUNCTION")); }
		}

		public int MaxParamCount { get; private set; }

		public int MinParamCount { get; private set; }

		public List<MySQLRoutine> RoutineLists { get; internal set; }
	}
}
