﻿using GCSC.Data.MySQL.DML;
using System;
using System.Collections.Generic;

namespace GCSC.Data.MySQL.Schema
{
	public sealed class MySQLSchemata
	{
		public MySQLSchemata(string schemaName)
			: this(schemaName, false, false) { }

		public MySQLSchemata(string schemaName, bool isDefault)
			: this(schemaName, isDefault, false) { }

		public MySQLSchemata(string schemaName, bool isDefault, bool overwrite)
		{
			if (Global.GLB_SchemataList.ContainsKey(schemaName) && !overwrite)
				return;
			SchemaName = schemaName;
			Refresh();

			if (isDefault) Global.GLB_DefaultSchemata = schemaName;
			if (Global.GLB_SchemataList.ContainsKey(schemaName)) Global.GLB_SchemataList.Remove(schemaName);
			Global.GLB_SchemataList.Add(schemaName, this);
		}

		public void Refresh()
		{
			Tables = new Dictionary<string, MySQLTable>();
			Views = new Dictionary<string, MySQLTable>();
			TempTables = new Dictionary<string, MySQLTable>();
			StoredProcedures = new Dictionary<string, MySQLRoutine>();
			Functions = new Dictionary<string, MySQLRoutine>();

			var s = new MySQLSelect("SELECT `SCHEMA_NAME` FROM `information_schema`.`SCHEMATA` WHERE `SCHEMA_NAME` = @schema_name");
			s.AddParamWithValue("schema_name", SchemaName);
			if (s.RowCount != 1) throw new ArgumentException("Database not found: " + SchemaName, "schemaName");
			s.Clear();

			var ts = new MySQLTables(SchemaName);
			ts.GetTables.ForEach(t => Tables.Add(t.TableName.Trim().ToLower(), t));
			ts.GetViews.ForEach(t => Views.Add(t.TableName.Trim().ToLower(), t));

			var rt = new MySQLRoutines(SchemaName);
			rt.GetProcedures.ForEach(r => StoredProcedures.Add(r.RoutineName.Trim().ToLower(), r));
			rt.GetFunctions.ForEach(r => Functions.Add(r.RoutineName.Trim().ToLower(), r));
			Routines = rt.RoutineLists;
			MaxParamCount = rt.MaxParamCount;
			MinParamCount = rt.MinParamCount;
		}

		public static MySQLSchemata GetSchema(string schemata)
		{
			return Global.GLB_SchemataList[schemata];
		}

		public static Dictionary<string, MySQLSchemata> SchemaLists
		{
			get { return Global.GLB_SchemataList; }
		}

		public Dictionary<string, MySQLTable> Tables { get; private set; }

		public Dictionary<string, MySQLTable> Views { get; private set; }

		public Dictionary<string, MySQLTable> TempTables { get; private set; }

		public Dictionary<string, MySQLRoutine> StoredProcedures { get; private set; }

		public Dictionary<string, MySQLRoutine> Functions { get; private set; }

		public List<MySQLRoutine> Routines { get; private set; }

		public int MaxParamCount { get; private set; }

		public int MinParamCount { get; private set; }

		public string SchemaName { get; private set; }
	}
}
