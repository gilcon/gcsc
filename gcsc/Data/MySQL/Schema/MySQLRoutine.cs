﻿using GCSC.Data.MySQL.DML;
using GCSC.Util;
using System;
using System.Collections.Generic;

namespace GCSC.Data.MySQL.Schema
{
	public class MySQLRoutine
	{
		public MySQLRoutine(object[] routineProperties, string schema)
		{
			Parameters = new Dictionary<string, MySQLParameter>();
			RoutineSchema = schema;
			RoutineName = routineProperties[0].ToString();
			RoutineType = routineProperties[1].ToString();
			DataType = routineProperties[2].ToString();
			CharacterMaximumLength = routineProperties[3].ToInt64();
			NumericPrecision = routineProperties[4].ToInt64();
			NumericScale = routineProperties[5].ToInt64();

			var s = new MySQLSelect()
						{
							Command = "SELECT `SPECIFIC_NAME`, `ORDINAL_POSITION`, `PARAMETER_MODE`, `PARAMETER_NAME`, `DATA_TYPE`, `CHARACTER_MAXIMUM_LENGTH`, `NUMERIC_PRECISION`, `NUMERIC_SCALE`, `COLLATION_NAME` FROM `information_schema`.`PARAMETERS` WHERE `SPECIFIC_SCHEMA` = @schema AND `SPECIFIC_NAME` = @routine_name GROUP BY `ORDINAL_POSITION` ASC"
						};
			s.AddParamWithValue("schema", schema);
			s.AddParamWithValue("routine_name", RoutineName);

			Array.ForEach(s.Reader(), r =>
			{
				Parameters.Add(r[3].ToString().Trim().ToLower(), new MySQLParameter(r, schema));
				ParametersCount++;
			});
		}

		public Dictionary<string, MySQLParameter> Parameters { get; internal set; }

		public string RoutineSchema { get; internal set; }

		public string RoutineName { get; internal set; }

		public string RoutineType { get; internal set; }

		public string DataType { get; internal set; }

		public Int64 CharacterMaximumLength { get; internal set; }

		public Int64 NumericPrecision { get; internal set; }

		public Int64 NumericScale { get; internal set; }

		public Int16 ParametersCount { get; private set; }
	}
}
