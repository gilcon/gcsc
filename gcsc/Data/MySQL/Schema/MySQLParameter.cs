﻿using GCSC.Util;
using System;
using System.Data;

namespace GCSC.Data.MySQL.Schema
{
	public class MySQLParameter
	{
		public MySQLParameter(object[] parameterParameters, string schemata)
		{
			SpecificSchema = schemata;
			SpecificName = parameterParameters[0].ToString();
			OrdinalPosition = parameterParameters[1].ToInt();

			string paramDirection = parameterParameters[2].ToString().ToLower().Trim();

			ParameterDirection = paramDirection == "inout" ?
				ParameterDirection.InputOutput : paramDirection == "out" ?
				ParameterDirection.Output : ParameterDirection.Input;

			ParameterName = parameterParameters[3].ToString();
			DataType = parameterParameters[4].ToString();
			CharacterMaximumLength = parameterParameters[5].ToInt64();
			NumericPrecision = parameterParameters[6].ToInt64();
			NumericScale = parameterParameters[7].ToInt64();
			CollationName = parameterParameters[8].ToString();
		}

		public string SpecificSchema { get; internal set; }

		public string SpecificName { get; internal set; }

		public int OrdinalPosition { get; internal set; }

		public ParameterDirection ParameterDirection { get; internal set; }

		public string ParameterName { get; internal set; }

		public string DataType { get; internal set; }

		public Int64 CharacterMaximumLength { get; internal set; }

		public Int64 NumericPrecision { get; internal set; }

		public Int64 NumericScale { get; internal set; }

		public string CollationName { get; internal set; }
	}
}
