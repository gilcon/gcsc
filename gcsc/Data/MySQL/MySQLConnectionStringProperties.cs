﻿using GCSC.Util;
using System;
using System.Collections.Generic;

namespace GCSC.Data.MySQL
{
	public class MySQLConnectionStringProperties
	{
		internal Dictionary<string, object> internalConnectionProperties = new Dictionary<string, object>();

		private object GetValue(string name)
		{
			return internalConnectionProperties[name];
		}

		/// <summary>
		/// Allows execution of multiple SQL commands in single stetament.
		/// </summary>
		public bool AllowBatch
		{
			get { return GetValue("Allow Batch").Tobool(); }
			set { internalConnectionProperties["Allow Batch"] = value; }
		}

		/// <summary>
		/// Should the provider expect user variable to appear in the SQL.
		/// </summary>
		public bool AllowUserVariables
		{
			get { return GetValue("Allow User Variables").Tobool(); }
			set { internalConnectionProperties["Allow User Variables"] = value; }
		}

		/// <summary>
		/// Should zero datetimes be suppoerted.
		/// </summary>
		public bool AllowZeroDatetime
		{
			get { return GetValue("Allow Zero Datetime").Tobool(); }
			set { internalConnectionProperties["Allow Zero Datetime"] = value; }
		}

		/// <summary>
		/// Should the connection automatically enlist in the active connection, if there any.
		/// </summary>
		public bool AutoEnlist
		{
			get { return GetValue("Auto Enlist").Tobool(); }
			set { internalConnectionProperties["Auto Enlist"] = value; }
		}

		/// <summary>
		/// Pattern that matches columns that should not be treated as UTF8.
		/// </summary>
		public string BlobAsUTF8ExcludePattern
		{
			get { return GetValue("blobasutf8excludepattern").ToString(); }
			set { internalConnectionProperties["blobasutf8excludepattern"] = value; }
		}

		/// <summary>
		/// Pattern that should coulumns that should be treated as UTF8.
		/// </summary>
		public string BlobAsUTF8IncludePattern
		{
			get { return GetValue("blobasutf8includepattern").ToString(); }
			set { internalConnectionProperties["blobasutf8includepattern"] = value; }
		}

		/// <summary>
		/// When true, server properties will be cached after the first server in the pool is created.
		/// </summary>
		public bool CacheServerProperties
		{
			get { return GetValue("Cache Server Properties").Tobool(); }
			set { internalConnectionProperties["Cache Server Properties"] = value; }
		}

		/// <summary>
		/// Ciertificate file in PKCS#12 format(.pfx).
		/// </summary>
		public string CertificateFile
		{
			internal get { return GetValue("Certificate File").ToString(); }
			set { internalConnectionProperties["Certificate File"] = value; }
		}

		/// <summary>
		/// Password for certificate file.
		/// </summary>
		public string CertificatePassword
		{
			internal get { return GetValue("Certificate Password").ToString(); }
			set { internalConnectionProperties["Certificate Password"] = value; }
		}

		/// <summary>
		/// The connection to be used in connecting in MySWL.
		/// </summary>
		public ConnectionProtocol ConnectionProtocolSetting
		{
			get { return (ConnectionProtocol)GetValue("Connection Protocol"); }
			set { internalConnectionProperties["Connection Protocol"] = value; }
		}

		/// <summary>
		/// Certificate store location for client certificate.
		/// </summary>
		public CertificateStoreLocation CertificateStoreLocationSetting
		{
			internal get { return (CertificateStoreLocation)GetValue("Certificate Store Location"); }
			set { internalConnectionProperties["Certificate Store Location"] = value; }
		}

		/// <summary>
		/// Certificate thumbprint.
		/// Can be used together with Certificate Store Location to uniquely identify certificate to be used for SSL
		/// authentication.
		/// </summary>
		public string CertificteThumbprint
		{
			internal get { return GetValue("Certificate Thumbprint").ToString(); }
			set { internalConnectionProperties["Certificate Thumbprint"] = value; }
		}

		/// <summary>
		/// Character Set this connection should use.
		/// </summary>
		public string CharacterSet
		{
			get { return GetValue("Character Set").ToString(); }
			set { internalConnectionProperties["Character Set"] = value; }
		}

		/// <summary>
		/// Indicates if stored routine parameter should be checked against the server.
		/// </summary>
		public bool CheckParameter
		{
			get { return GetValue("Check Parameter").Tobool(); }
			set { internalConnectionProperties["Check Parameter"] = value; }
		}

		/// <summary>
		/// The list of interceptor that can intercept command operation.
		/// </summary>
		public string CommandInterceptor
		{
			get { return GetValue("Command Interceptor").ToString(); }
			set { internalConnectionProperties["Command Interceptor"] = value; }
		}

		/// <summary>
		/// The minimum amount of time(in seconds) for this connection to live in the pool before being destroyed.
		/// </summary>
		public int ConnectionLifeTime
		{
			get { return GetValue("Connection Life Time").ToInt(); }
			set { internalConnectionProperties["Connection Life Time"] = value; }
		}

		/// <summary>
		/// When true, indicates the connection state is reset when removed from the pool.
		/// </summary>
		public bool ConnectionReset
		{
			get { return GetValue("Connection Reset").Tobool(); }
			set { internalConnectionProperties["Connection Reset"] = value; }
		}

		/// <summary>
		/// The lenght of time (in seconds) to wait for a connection to the server before terminating
		/// the attemp and generating an error.
		/// </summary>
		public int ConnectTimeout
		{
			get { return GetValue("Connect Timeout").ToInt(); }
			set { internalConnectionProperties["Connect Timeout"] = value; }
		}

		/// <summary>
		/// Should illegal datetime values be converted to DateTime.MinValue.
		/// </summary>
		public bool ConvertZeroDatetime
		{
			get { return GetValue("Convert Zero Datetime").Tobool(); }
			set { internalConnectionProperties["Convert Zero Datetime"] = value; }
		}

		/// <summary>
		/// Database to use initially.
		/// </summary>
		public string Database
		{
			get { return GetValue("database").ToString(); }
			set { internalConnectionProperties["database"] = value; }
		}

		/// <summary>
		/// THe default timeout that MysqlCommand object will use, unless changed.
		/// </summary>
		public int DefaultCommandTimeout
		{
			get { return GetValue("Default Command Timeout").ToInt(); }
			set { internalConnectionProperties["Default Command Timeout"] = value; }
		}

		/// <summary>
		/// Specifies how long a TableDirect result should be cached in seconds.
		/// </summary>
		public int DefaultTableCacheAge
		{
			get { return GetValue("Default Table Cache Age").ToInt(); }
			set { internalConnectionProperties["Default Table Cache Age"] = value; }
		}

		/// <summary>
		/// The list of interceptor that can triage throw MySqlExceptions.
		/// </summary>
		public string ExceptionInterceptor
		{
			get { return GetValue("Exception Interceptor").ToString(); }
			set { internalConnectionProperties["Exception Interceptor"] = value; }
		}

		/// <summary>
		/// Should all server functions be treated as returning strings?
		/// </summary>
		public bool FunctionsReturnString
		{
			get { return GetValue("Functions Return String").Tobool(); }
			set { internalConnectionProperties["Functions Return String"] = value; }
		}

		/// <summary>
		/// Instruct the provider to ignore any attempts to prepare a command.
		/// </summary>
		public bool IgnorePrepare
		{
			get { return GetValue("Ignore Prepare").Tobool(); }
			set { internalConnectionProperties["Ignore Prepare"] = value; }
		}

		/// <summary>
		/// Include security assets to support Medium Trust.
		/// </summary>
		public bool IncludeSecurityAsserts
		{
			get { return GetValue("Include Security Asserts").Tobool(); }
			set { internalConnectionProperties["Include Security Asserts"] = value; }
		}

		/// <summary>
		/// Use windows authentication when conneccting to server.
		/// </summary>
		public bool IntegratedSecurity
		{
			get { return GetValue("Integrated Security").Tobool(); }
			set { internalConnectionProperties["Integrated Security"] = value; }
		}

		/// <summary>
		/// Should this session be considered interactive?
		/// </summary>
		public bool InteractiveSession
		{
			get { return GetValue("Interactive Session").Tobool(); }
			set { internalConnectionProperties["Interactive Session"] = value; }
		}

		/// <summary>
		/// For TCP connections, idle connecion time measured in seconds,
		/// before the first keepalive packet is sent.
		/// A value of 0 indicates that keepalive is not in use.
		/// </summary>
		public int KeepAlive
		{
			get { return GetValue("Keep Alive").ToInt(); }
			set { internalConnectionProperties["Keep Alive"] = value; }
		}

		/// <summary>
		/// Enable output for diagnostic messages.
		/// </summary>
		public bool Logging
		{
			get { return GetValue("logging").Tobool(); }
			set { internalConnectionProperties["logging"] = value; }
		}

		/// <summary>
		/// The maximum number of connections allowed in the pool.
		/// </summary>
		public int MaximumPoolSize
		{
			get { return GetValue("Maximum Pool Size").ToInt(); }
			set { internalConnectionProperties["Maximum Pool Size"] = value; }
		}

		/// <summary>
		/// The minimum number of connection allowed in the pool.
		/// </summary>
		public int MinimumPoolSize
		{
			get { return GetValue("Minimum Pool Size").ToInt(); }
			set { internalConnectionProperties["Minimum Pool Size"] = value; }
		}

		/// <summary>
		/// Treat binary(16) column as guids.
		/// </summary>
		public bool OldGuids
		{
			get { return GetValue("Old Guids").Tobool(); }
			set { internalConnectionProperties["Old Guids"] = value; }
		}

		/// <summary>
		/// Indicates the password to be used when connecting to the data source.
		/// </summary>
		public string Password
		{
			internal get { return GetValue("password").ToString(); }
			set { internalConnectionProperties["password"] = value; }
		}

		/// <summary>
		/// When false, security-sensitive information,
		/// such as the password, is not returned as part of the connection if the connection is open
		/// or has even been in an open state.
		/// </summary>
		public bool PersistSecurityInfo
		{
			get { return GetValue("Persist Security Info").Tobool(); }
			set { internalConnectionProperties["Persist Security Info"] = value; }
		}

		/// <summary>
		/// Name of pipe to use when connecting with named pipes (Win32 only).
		/// </summary>
		public string PipeName
		{
			get { return GetValue("Pipe Name").ToString(); }
			set { internalConnectionProperties["Pipe Name"] = value; }
		}

		/// <summary>
		/// When true, the connection object is drawn from the appropriate pool,
		/// or if necessary, is created and added to appropriate pool.
		/// </summary>
		public bool Pooling
		{
			get { return GetValue("pooling").Tobool(); }
			set { internalConnectionProperties["pooling"] = value; }
		}

		/// <summary>
		/// Port to use for TCP/IP connections.
		/// </summary>
		public int Port
		{
			get { return GetValue("port").ToInt(); }
			set { internalConnectionProperties["port"] = value; }
		}

		/// <summary>
		/// Indicates how many stored procedure can be cached at one time. A value 0 effectevly disable the procedure cache.
		/// </summary>
		public int ProcedureCacheSize
		{
			get { return GetValue("Procedure Cache Size").ToInt(); }
			set { internalConnectionProperties["Procedure Cache Size"] = value; }
		}

		/// <summary>
		/// Indicates if this connection is to use replicated server.
		/// </summary>
		public bool Replication
		{
			get { return GetValue("replication").Tobool(); }
			set { internalConnectionProperties["replication"] = value; }
		}

		/// <summary>
		/// Should binary flag on column metadata be respected.
		/// </summary>
		public bool RespectBinaryFlags
		{
			get { return GetValue("Respect Binary Flags").Tobool(); }
			set { internalConnectionProperties["Respect Binary Flags"] = value; }
		}

		/// <summary>
		/// Server to connect to.
		/// </summary>
		public string Server
		{
			get { return GetValue("server").ToString(); }
			set { internalConnectionProperties["server"] = value; }
		}

		/// <summary>
		/// Name of the shared memory object to use.
		/// </summary>
		public string SharedMemoryName
		{
			get { return GetValue("Shared Memory Name").ToString(); }
			set { internalConnectionProperties["Shared Memory Name"] = value; }
		}

		/// <summary>
		/// Allow Sql Server. A value of yes allow symbols to be enclosed with [] instead of ``.
		/// This does incur a performance hit so only use if necessary.
		/// </summary>
		public bool SqlServerMode
		{
			get { return GetValue("Sql Server Mode").Tobool(); }
			set { internalConnectionProperties["Sql Server Mode"] = value; }
		}

		/// <summary>
		/// SSL properties for connection.
		/// </summary>
		public SSLMode SslMode
		{
			internal get { return (SSLMode)GetValue("Ssl Mode"); }
			set { internalConnectionProperties["Ssl Mode"] = value; }
		}

		/// <summary>
		/// Enable or disables caching of TableDirect command.
		/// A value of yes enable the cache while no disable it.
		/// </summary>
		public bool TableCache
		{
			get { return GetValue("Table Cache").Tobool(); }
			set { internalConnectionProperties["Table Cache"] = value; }
		}

		/// <summary>
		/// Should binary blob will be treated as UTF8.
		/// </summary>
		public bool TreatBlobsasUTF8
		{
			get { return GetValue("Treat Blobs As UTF8").Tobool(); }
			set { internalConnectionProperties["Treat Blobs As UTF8"] = value; }
		}

		/// <summary>
		/// Should the provider treat TINYINT(1) columns as boolean.
		/// </summary>
		public bool TreatTinyAsBoolean
		{
			get { return GetValue("Treat Tiny As Boolean").Tobool(); }
			set { internalConnectionProperties["Treat Tiny As Boolean"] = value; }
		}

		/// <summary>
		/// Should the returned affected row count reflected rows instead of found rows?
		/// </summary>
		public bool UseAffectedRows
		{
			get { return GetValue("Use Affected Rows").Tobool(); }
			set { internalConnectionProperties["Use Affected Rows"] = value; }
		}

		/// <summary>
		/// Should the connection will use compression.
		/// </summary>
		public bool UseCompression
		{
			get { return GetValue("Use Compression").Tobool(); }
			set { internalConnectionProperties["Use Compression"] = value; }
		}

		/// <summary>
		/// Allows to use the old style @ syntax for parameters.
		/// </summary>
		public bool UseOldSyntax
		{
			get { return GetValue("Use Old Syntax").Tobool(); }
			set { internalConnectionProperties["Use Old Syntax"] = value; }
		}

		/// <summary>
		/// Indicates that performance counter should be updated during execution.
		/// </summary>
		public bool UsePerformanceMonitor
		{
			get { return GetValue("Use Performance Monitor").Tobool(); }
			set { internalConnectionProperties["Use Performance Monitor"] = value; }
		}

		/// <summary>
		/// Indicates if stored procedure bodies will be available for parameter detection.
		/// </summary>
		public bool UseProcedureBodies
		{
			get { return GetValue("Use Procedure Bodies").Tobool(); }
			set { internalConnectionProperties["Use Procedure Bodies"] = value; }
		}

		/// <summary>
		/// Indicates the user ID to be used when connecting to the data source.
		/// </summary>
		public string UserId
		{
			get { return GetValue("User Id").ToString(); }
			set { internalConnectionProperties["User Id"] = value; }
		}

		/// <summary>
		/// Logs inefficient database operation.
		/// </summary>
		public bool UseUsageAdvisor
		{
			get { return GetValue("Use Usage Advisor").Tobool(); }
			set { internalConnectionProperties["Use Usage Advisor"] = value; }
		}

		[Flags]
		public enum CertificateStoreLocation
		{
			None = 0,
			CurrentUser = 1,
			LocalMachine = 2
		}

		[Flags]
		public enum ConnectionProtocol
		{
			Socket = 0,
			NamedPipe = 1,
			Unix = 2,
			SharedMemory = 3
		}

		[Flags]
		public enum SSLMode
		{
			None = 0,
			Prepared = 1,
			Required = 2,
			VerifyCA = 3,
			VerifyFull = 4
		}
	}
}
