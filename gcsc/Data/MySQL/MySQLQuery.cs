﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace GCSC.Data.MySQL
{
	public sealed class MySQLQuery : IDisposable, Interface.Data.IQuery
	{
		private MySqlConnection _mysqlConnection;
		private MySqlCommand _mysqlCommand;
		private bool isDisposed;

		public MySQLQuery()
			: this(string.Empty, string.Empty, false)
		{
		}

		public MySQLQuery(string queryString)
			: this(queryString, string.Empty, false)
		{
		}

		public MySQLQuery(string queryString, string connectionString)
			: this(queryString, connectionString, false)
		{
		}

		public MySQLQuery(string queryString, bool isPreparedStatement)
			: this(queryString, string.Empty, isPreparedStatement)
		{
		}

		public MySQLQuery(string queryString, string connectionString, bool isPreparedStatement)
		{
			Command = queryString;
			ConnectionString = connectionString;
			Schema = string.Empty;
			ExecutionCount = 0;
			PreparedStatement = isPreparedStatement;
		}

		~MySQLQuery()
		{
			Dispose(true);
		}

		public void Dispose()
		{
			Dispose(false);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool isDisposing)
		{
			if (!isDisposed)
				if (isDisposing)
				{
					_mysqlCommand.Dispose();
					_mysqlConnection.Dispose();
					Command = null;
					ConnectionString = null;
					ExecutionCount = 0;
					Schema = null;
				}

			isDisposed = true;
		}

		public int Execute()
		{
			return Execute(new Dictionary<string, object>());
		}

		public int Execute(Dictionary<string, object> parameters)
		{
			if (isDisposed) throw new ObjectDisposedException("Query");
			if (ExecutionCount == 0)
			{
				_mysqlConnection = new MySqlConnection();
				if (ConnectionString.Length > 0)
					_mysqlConnection.ConnectionString = ConnectionString;
				else
					using (var sc = new MySQLSecuredConnectionString())
						if (PreparedStatement)
							_mysqlConnection.ConnectionString = sc.GetConnectionString(new MySQLConnectionStringProperties() { IgnorePrepare = false });
						else
							_mysqlConnection.ConnectionString = sc.GetConnectionString();
				_mysqlCommand = new MySqlCommand(Command, _mysqlConnection)
				{
					CommandType = CommandType,
					CommandTimeout = MySQLConnectionString.GlobalCommandTimeOut
				};
				foreach (var kv in parameters)
					_mysqlCommand.Parameters.AddWithValue(kv.Key, kv.Value);
				_mysqlConnection.Open();
				if (Schema.Length > 0) _mysqlConnection.ChangeDatabase(Schema);
				if (PreparedStatement) _mysqlCommand.Prepare();
			}
			if (ExecutionCount > 0 && PreparedStatement)
				foreach (var kv in parameters)
				{
					if (!_mysqlCommand.Parameters.Contains(kv.Key)) throw new ArgumentException("parameter", "Unknown parameter name : " + kv.Key);
					_mysqlCommand.Parameters[kv.Key].Value = kv.Value;
				}
			ExecutionCount++;
			return _mysqlCommand.ExecuteNonQuery();
		}

		public int ExecuteScript()
		{
			if (isDisposed) throw new ObjectDisposedException("Query");
			using (var cn = new MySqlConnection())
			{
				if (ConnectionString.Length > 0)
					cn.ConnectionString = ConnectionString;
				else
					using (var sc = new MySQLSecuredConnectionString())
						cn.ConnectionString = sc.GetConnectionString();
				try
				{
					var ms = new MySqlScript(cn, Command);
					ms.Error += ExecuteScriptError;
					return ms.Execute();
				}
				catch (MySqlException ex)
				{
					Global.GLB_AppEvents = new Util.AppEvents("GCSC_CORE");
					Global.GLB_AppEvents.WriteToLog(ex.Message, System.Diagnostics.EventLogEntryType.Error, CategoryType.WriteToDB, EventIDTytpe.Write);
					Global.GLB_AppEvents.CloseLog();
				}
				return -1;
			}
		}

		private void ExecuteScriptError(object sender, MySqlScriptErrorEventArgs args)
		{
			DevExpress.XtraEditors.XtraMessageBox.Show(args.Exception.ToString());
		}

		public string Command { get; set; }

		public string ConnectionString { get; set; }

		public string Schema { get; set; }

		public int ExecutionCount { get; private set; }

		public bool PreparedStatement { get; private set; }

		public System.Data.CommandType CommandType { get; set; }
	}
}
