using System;
using System.Configuration;
using System.Security;
using System.Text;

namespace GCSC.Data.MySQL
{
	public sealed class MySQLConnectionString : MySQLConnectionStringProperties, IDisposable, GCSC.Interface.Data.IConnectionString
	{
		private static int _globalCommandTimeOut = 30;

		public static int GlobalCommandTimeOut
		{
			get { return _globalCommandTimeOut; }
			set { _globalCommandTimeOut = value; }
		}

		private StringBuilder connectionString = new StringBuilder();
		private static Configuration config;
		internal ConnectionStringsSection ConnectionStringSection;
		private bool isDisposed;

		public MySQLConnectionString()
			: this(ConfigurationUserLevel.None)
		{
		}

		public MySQLConnectionString(ConfigurationUserLevel ConfigurationUserLevel)
		{
			this.ConfigurationUserLevel = ConfigurationUserLevel;
			config = ConfigurationManager.OpenExeConfiguration(this.ConfigurationUserLevel);
			ConnectionStringSection = config.ConnectionStrings;
		}

		public MySQLConnectionString(ConfigurationUserLevel ConfigurationUserLevel, ref MySQLConnectionStringProperties connectionStringProperties)
		{
			if (connectionStringProperties == null)
				throw new ArgumentNullException("connectionStringProperties", "connectionStringProperties must not be null.");

			this.ConfigurationUserLevel = ConfigurationUserLevel;

			config = ConfigurationManager.OpenExeConfiguration(this.ConfigurationUserLevel);
			ConnectionStringSection = config.ConnectionStrings;

			foreach (var kv in connectionStringProperties.internalConnectionProperties)
				internalConnectionProperties.Add(kv.Key, kv.Value);

			connectionStringProperties.internalConnectionProperties.Clear();
			connectionStringProperties = null;
		}

		~MySQLConnectionString()
		{
			Dispose(false);
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool isDisposing)
		{
			if (!isDisposed)
				if (isDisposing)
				{
					internalConnectionProperties.Clear();
					connectionString = null;
					config = null;
					ConnectionStringSection = null;
				}

			isDisposed = true;
		}

		/// <summary>
		/// Set-up the connection string
		/// </summary>
		/// <param name="connectionName">string connection name</param>
		public void RegisterConnectionString(string connectionName)
		{
			if (isDisposed) throw new ObjectDisposedException("ConnectionString");
			//server=localhost;User Id=root;password=aspire;Persist Security Info=True;database=test
			if (!internalConnectionProperties.ContainsKey("server") || internalConnectionProperties["server"].ToString().Length < 1) throw new ArgumentNullException("Server");
			if (!internalConnectionProperties.ContainsKey("User Id") || internalConnectionProperties["User Id"].ToString().Length < 1) throw new ArgumentNullException("UserId");
			if (!internalConnectionProperties.ContainsKey("password") || internalConnectionProperties["password"].ToString().Length < 1) throw new ArgumentNullException("Password");
			if (!internalConnectionProperties.ContainsKey("database") || internalConnectionProperties["database"].ToString().Length < 1) throw new ArgumentNullException("Database");
			if (connectionName.Length < 1) throw new ArgumentNullException("connectionName");

			foreach (var k in internalConnectionProperties)
				connectionString.Append(k.Key + "=" + k.Value + ";");

			Encrypt(connectionName, connectionString.ToString(), "MySql.Data.MySqlClient");
			connectionName = null;
		}

		private void Encrypt(string connectionName, string connectionString, string provider)
		{
			if (ConnectionStringSection == null)
			{
				ConnectionStringSection = new ConnectionStringsSection();
				config.Sections.Add("connnectionSetting", ConnectionStringSection);
			}

			if (!ConnectionStringSection.SectionInformation.IsProtected)
				ConnectionStringSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");

			ConnectionStringSection.ConnectionStrings.Remove(connectionName);
			ConnectionStringSection.ConnectionStrings.Add(new ConnectionStringSettings(connectionName, connectionString, provider));

			connectionString = null;
			connectionName = null;
			provider = null;

			ConnectionStringSection.SectionInformation.ForceSave = true;
			config.Save(ConfigurationSaveMode.Full);
			ConfigurationManager.RefreshSection("connectionStrings");
			CollectAllConnectionString();
		}

		private void CollectAllConnectionString(string defaultConnectionStringName = null)
		{
			if (ConnectionStringSection == null || !ConnectionStringSection.SectionInformation.IsProtected)
			{
				MySQLConnectionString conn = this;
				if (defaultConnectionStringName == null) throw new ArgumentException("defaultConnectionStringName is null", "defaultConnectionStringName");
				conn.RegisterConnectionString(defaultConnectionStringName);
				this.ConnectionStringSection = conn.ConnectionStringSection;
			}
			if (!Global.GLB_ProtectedConnectionStringCollection.ContainsKey("MySql.Data.MySqlClient"))
				Global.GLB_ProtectedConnectionStringCollection.Add("MySql.Data.MySqlClient", new System.Collections.Generic.Dictionary<string, SecureString>());
			Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"].Clear();
			foreach (ConnectionStringSettings cs in ConnectionStringSection.ConnectionStrings)
			{
				if (cs.ProviderName == "MySql.Data.MySqlClient")
				{
					using (var ss = new SecureString())
					{
						for (int i = 0; i < cs.ConnectionString.Length; i++)
							ss.AppendChar(cs.ConnectionString[i]);
						Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"].Add(cs.Name, ss.Copy());
					}
				}
			}
		}

		public void InitConnectionString(string defaultConnectionStringName, ref bool connectionNameStatus)
		{
			if (!Global.GLB_ProtectedConnectionStringCollection.ContainsKey("MySql.Data.MySqlClient") || Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"].Count < 1)
				CollectAllConnectionString(defaultConnectionStringName);
			if (!Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"].ContainsKey(defaultConnectionStringName)) return;
			connectionNameStatus = true;
			Global.GLB_DefaultProviderConnectionStrings.Remove("MySql.Data.MySqlClient");
			Global.GLB_DefaultProviderConnectionStrings.Add("MySql.Data.MySqlClient", Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"][defaultConnectionStringName]);
		}

		public bool CheckConnectionString(string defaultConnectionStringName)
		{
			if (ConnectionStringSection == null) return false;
			if (!ConnectionStringSection.SectionInformation.IsProtected) return false;
			bool b = false;

			InitConnectionString(defaultConnectionStringName, ref b);
			return b;
		}

		public void InitDefaultSchema(string schemaName)
		{
			new Schema.MySQLSchemata(schemaName, true);
		}

		public void RemoveAll()
		{
			ConnectionStringSection.ConnectionStrings.Clear();
			ConnectionStringSection.SectionInformation.ForceSave = true;
			config.Save(ConfigurationSaveMode.Full);
		}

		public void Remove(string connectionName)
		{
			ConnectionStringSection.ConnectionStrings.Remove(connectionName);
			ConnectionStringSection.SectionInformation.ForceSave = true;
			config.Save(ConfigurationSaveMode.Full);
		}

		public static void ModifyConnection(ref string connectionString, MySQLConnectionStringProperties properties)
		{
			ModifyConnection(string.Empty, ref connectionString, properties, false);
		}

		public static void ModifyConnection(string connectionName, ref string connectionString, MySQLConnectionStringProperties properties, bool save)
		{
			MySQLConnectionStringProperties tempProperties = properties;
			foreach (var c in connectionString.Split(';'))
			{
				string[] tmpString = c.Split('=');
				if (tmpString.Length == 2)
					if (!tempProperties.internalConnectionProperties.ContainsKey(tmpString[0]))
						tempProperties.internalConnectionProperties.Add(tmpString[0], tmpString[1]);
					else
						tempProperties.internalConnectionProperties[tmpString[0]] = tmpString[1];
			}
			StringBuilder sb = new StringBuilder();
			foreach (var k in tempProperties.internalConnectionProperties)
				sb.Append(k.Key + "=" + k.Value + ";");
			connectionString = sb.ToString();
			sb.Clear();
			if (!save) return;
			if (connectionName.Length < 1) throw new ArgumentNullException("connectionName");
			if (!Global.GLB_ProtectedConnectionStringCollection.ContainsKey("MySql.Data.MySqlClient"))
				throw new ApplicationException("Unknown provider: MySql.Data.MySqlClient");
			SecureString ss = new SecureString();
			foreach (var c in connectionString)
				ss.AppendChar(c);
			Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"].Add(connectionName, ss);
			ss.Dispose();
		}

		public static void AddTempConnectionString(string connectionName, string connectionString)
		{
			SecureString ss = new SecureString();
			foreach (var c in connectionString)
				ss.AppendChar(c);
			if (!Global.GLB_ProtectedConnectionStringCollection.ContainsKey("MySql.Data.MySqlClient"))
				throw new ApplicationException("Unknown provider: MySql.Data.MySqlClient");
			Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"].Add(connectionName, ss);
			ss.Dispose();
		}

		public static void AddTempConnectionString(string connectionName, MySQLConnectionStringProperties connectionProperties)
		{
			StringBuilder sb = new StringBuilder();
			foreach (var k in connectionProperties.internalConnectionProperties)
				sb.Append(k.Key + "=" + k.Value + ";");
			SecureString ss = new SecureString();
			string s = sb.ToString();
			sb.Clear();
			foreach (var c in s)
				ss.AppendChar(c);
			if (!Global.GLB_ProtectedConnectionStringCollection.ContainsKey("MySql.Data.MySqlClient"))
				throw new ApplicationException("Unknown provider: MySql.Data.MySqlClient");
			Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"].Add(connectionName, ss);
			ss.Dispose();
		}

		public static void RemoveTempConectionString(string connectionName)
		{
			if (!Global.GLB_ProtectedConnectionStringCollection.ContainsKey("MySql.Data.MySqlClient")) return;
			if (!Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"].ContainsKey(connectionName)) return;
			Global.GLB_ProtectedConnectionStringCollection["MySql.Data.MySqlClient"].Remove(connectionName);
		}

		public static string GetDefaultConnectionString()
		{
			using (var sc = new MySQLSecuredConnectionString())
				return sc.GetConnectionString();
		}

		public static string GetDefaultConnectionString(MySQLConnectionStringProperties properties)
		{
			using (var sc = new MySQLSecuredConnectionString())
				return sc.GetConnectionString(properties);
		}

		public bool IsServerAlive()
		{
			using (var cn = new MySql.Data.MySqlClient.MySqlConnection())
			{
				using (var sc = new MySQLSecuredConnectionString())
				{
					cn.ConnectionString = sc.GetConnectionString();
				}
				try
				{
					cn.Open();
					return true;
				}
				catch
				{
					return false;
				}
			}
		}

		public ConfigurationUserLevel ConfigurationUserLevel { get; set; }

		public bool HasServer
		{
			get { return internalConnectionProperties.ContainsKey("server"); }
		}
	}
}
