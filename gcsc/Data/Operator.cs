namespace GCSC.Data
{
	[System.Flags]
	public enum Operator
	{
		AND = 0,
		_AND = 1,
		NULL_SAFE = 2,
		EQUAL = 3,
		GREATER_than_EQUAL = 4,
		GREATER_than = 5,
		LESS_than_EQUAL = 6,
		LESS_than = 7,
		IS_NOT_NULL = 8,
		IS_NOT = 9,
		IS_NULL = 10,
		LIKE = 11,
		NOT_EQUAL = 12,
		_NOT_EQUAL = 13,
		NOT_LIKE = 14,
		NOT = 15,
		_NOT = 16,
		OR = 17,
		_OR = 18,
		SOUNDS_LIKE = 19
	}
}
