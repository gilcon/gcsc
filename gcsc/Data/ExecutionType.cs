namespace GCSC.Data
{
	[System.Flags]
	public enum ExecutionType : short
	{
		Unknown = 0,
		ExecuteReader = 1,
		ExecuteNonQuery = 2,
		ExecuteScalar = 3
	}
}
