﻿using GCSC.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace GCSC.Util
{
	public sealed class EditableList<T> : BindingList<T>
		where T : GCSC.Interface.IEditableObject
	{
		private delegate bool AsyncUpdateDelagate(IEnumerable<T> ie, EditableListRaiseEventHandler ev);

		private List<T> _removeItems;
		private readonly bool _initValues;
		private bool _asyncBaseUpdate;
		private bool _autoAsyncFailedUpdate;
		private bool _asyncFailedUpdateReQeueStarted;

		private bool _isListChangedHasSubscribedOutSide;

		private EditableListRaiseEventHandler onEditableListRaised;
		private static List<T> AsyncUpdateCache;
		private static Dictionary<int, IEnumerable<T>> AsyncUpdateCacheTMP;

		public IEnumerable<T> GetRemovedItems()
		{
			return _removeItems.AsEnumerable();
		}

		public List<T> GetFailedAsyncUpdates()
		{
			if (!AsyncBaseUpdate)
				throw new ArgumentNullException("AsyncBaseUpdate");
			return AsyncUpdateCache;
		}

		public IEnumerable<T> GetUpdatedItems()
		{
			foreach (T item in this.Where(t => t.IsDirty()))
				yield return item;
		}

		public IEnumerable<T> GetNewItems()
		{
			foreach (T item in this.Where(t => t.IsNew()))
				yield return item;
		}

		private void Inits()
		{
			AllowEdit = true;
			AllowNew = true;
			AllowRemove = true;
			IsTimeBatchUpdateEnabled = false;
			AsyncBaseUpdate = false;
			AutoAsyncFailedUpdateInterval = 1;
			_removeItems = new List<T>();
		}

		public EditableList()
		{
			Inits();
		}

		public EditableList(object[][] items)
		{
			Inits();
			int itemLenght = 0;
			ConstructorInfo finalConstructor = null;
			ParameterInfo[] finalConstructorParameters = null;

			if (items.Length < 1)
				return;

			itemLenght = items[0].Length;

			foreach (ConstructorInfo ci in typeof(T).GetConstructors())
			{
				ParameterInfo[] pi = ci.GetParameters();
				if (pi.Length == itemLenght)
				{
					finalConstructor = ci;
					finalConstructorParameters = pi;
					break;
				}
			}

			_initValues = true;

			if (finalConstructor != null && finalConstructorParameters.Length == itemLenght)
			{
				foreach (object[] o in items)
				{
					for (int i = 0; i < o.Length; i++)
						Extra.IsTypeOf(finalConstructorParameters[i].ParameterType, ref o[i]);

					T t = (T)finalConstructor.Invoke(o);
					t.InitObject();
					base.Add(t);
				}
			}
			else
			{
				PropertyInfo[] pi = typeof(T).GetProperties();
				if (pi.Length == itemLenght)
				{
					ConstructorInfo ci = typeof(T).GetConstructor(System.Type.EmptyTypes);

					foreach (object[] o in items)
					{
						T itemObject = default(T);
						if (ci != null)
							itemObject = (T)ci.Invoke(new Object[0]);
						for (int i = 0; i < o.Length; i++)
						{
							Extra.IsTypeOf(pi[i].PropertyType, ref o[i]);
							pi[i].SetValue(itemObject, o[i], null);
						}
						itemObject.InitObject();
						base.Add(itemObject);
					}
				}
			}

			_initValues = false;
			items = null;
		}

		public EditableList(IList<T> list)
		{
			Inits();

			_initValues = true;

			foreach (T t in list)
			{
				t.InitObject();
				base.Add(t);
			}

			_initValues = false;
		}

		protected override void InsertItem(int index, T item)
		{
			if (!_initValues)
			{
				item.IsNew(true);
				item.IsDirty(false);
			}

			base.InsertItem(index, item);
		}

		public new void Insert(int index, T item)
		{
			item.IsNew(true);
			item.IsDirty(false);
			base.Insert(index, item);
		}

		public void Modify(int index, T newValue)
		{
			base[index] = newValue;
		}

		public void Modify(T oldValue, T newValue)
		{
			Modify(ref oldValue, newValue);
		}

		public void Modify(ref T oldValue, T newValue)
		{
			newValue.InitObject();
			newValue.IsDirty(true);
			oldValue = base[base.IndexOf(oldValue)] = newValue;
		}

		protected override void RemoveItem(int index)
		{
			if (index >= 0 && index < base.Count)
			{
				var item = base[index];
				_removeItems.Add(item);
				base.RemoveItem(index);
			}
		}

		/// <summary>
		/// Finalizing the object and setting the object cleaned
		/// without executing the batch update
		/// </summary>
		public void Finalized()
		{
			Finalized(false);
		}

		/// <summary>
		/// Finalizng the object and setting the object cleaned
		/// </summary>
		/// <param name="executeBatchUpdate">True to execute the batched update otherwise false</param>
		public void Finalized(bool executeBatchUpdate)
		{
			if (executeBatchUpdate)
			{
				if (onEditableListRaised != null)
					foreach (T item in this.Where(t => t.IsDirty() || t.IsNew()))
					{
						onEditableListRaised(null, new EditableListRaiseEventArgs(item));
						item.InitObject();
					}
				return;
			}

			foreach (T item in this.Where(w => w.IsNew() || w.IsDirty()))
				item.InitObject();
		}

		/// <summary>
		/// Enabling the timed batch update with a specific delay.
		/// enabling this, there should be a subscriber on EditableListRaised
		/// </summary>
		/// <param name="timeBatchUpdateDelay">The delay in seconds</param>
		public void EnableTimedBatchUpdate(int timeBatchUpdateDelay)
		{
			TimedBatchUpdateDelay = timeBatchUpdateDelay * 1000;
			IsTimeBatchUpdateEnabled = true;

			if (_isListChangedHasSubscribedOutSide)
				throw new NotSupportedException("Unable to enable TimeBatchUpdate. ListChanged event is already been subscribed.");

			base.ListChanged += EditableList_ListChanged;
		}

		private void EditableList_ListChanged(object sender, ListChangedEventArgs e)
		{
			var _batchTimer = new Timer() { Interval = TimedBatchUpdateDelay };
			_batchTimer.Tick += _batchTimer_Tick;
			_batchTimer.Start();
		}

		private void _batchTimer_Tick(object sender, EventArgs e)
		{
			if (onEditableListRaised == null)
				return;

			if (!AsyncBaseUpdate)
			{
				foreach (T item in this.Where(t => t.IsDirty() || t.IsNew()))
				{
					onEditableListRaised(null, new EditableListRaiseEventArgs(item));
					item.InitObject();
				}
			}
			else
			{
				Finalized();
				AsyncUpdateDelagate ad = AsyncUpdating;
				ad.BeginInvoke(this.Where(w => w.IsDirty() || w.IsNew()), onEditableListRaised, AsyncUpdated, ad);
			}

			var _batchTimer = (Timer)sender;
			_batchTimer.Stop();
			_batchTimer.Dispose();
		}

		private bool AsyncUpdating(IEnumerable<T> ie, EditableListRaiseEventHandler ev)
		{
			AsyncUpdateCacheTMP.Add(System.Threading.Thread.CurrentThread.GetHashCode(), ie);
			return ev(null, new EditableListRaiseEventArgs((IEnumerable<Interface.IEditableObject>)ie));
		}

		private void AsyncUpdated(IAsyncResult ar)
		{
			int key = System.Threading.Thread.CurrentThread.GetHashCode();
			AsyncUpdateDelagate ad = (AsyncUpdateDelagate)ar.AsyncState;

			if (ad.EndInvoke(ar))
			{
				foreach (T i in AsyncUpdateCacheTMP[key])
					AsyncUpdateCache.Remove(i);

				AsyncUpdateCacheTMP.Remove(key);
			}
			else
			{
				foreach (T i in AsyncUpdateCacheTMP[key])
					if (!AsyncUpdateCache.Contains(i))
						AsyncUpdateCache.Add(i);

				AsyncUpdateCacheTMP.Remove(key);

				if (!_asyncFailedUpdateReQeueStarted && AsyncUpdateCache.Count > 0)
					AsyncFailedUpdateReQeue();
			}
		}

		private void AsyncFailedUpdateReQeue()
		{
			_asyncFailedUpdateReQeueStarted = true;
			var _failedUpdateTimer = new Timer() { Interval = AutoAsyncFailedUpdateInterval * 1000 };
			_failedUpdateTimer.Tick += _failedUpdateTimer_Tick;
			_failedUpdateTimer.Start();
		}

		private void _failedUpdateTimer_Tick(object sender, EventArgs e)
		{
			AsyncUpdateDelagate ad = AsyncUpdating;
			ad.BeginInvoke(AsyncUpdateCache, onEditableListRaised, AsyncUpdated, ad);

			if (AsyncUpdateCache.Count < 1)
			{
				((Timer)sender).Dispose();
				_asyncFailedUpdateReQeueStarted = false;
			}
		}

		/// <summary>
		/// subscribed to this event for timed batch update
		/// </summary>
		public event EditableListRaiseEventHandler EditableListRaised
		{
			add { onEditableListRaised += value; }
			remove { onEditableListRaised -= value; }
		}

		/// <summary>
		/// Override from Bindiglist
		/// </summary>
		public new event ListChangedEventHandler ListChanged
		{
			add
			{
				if (IsTimeBatchUpdateEnabled)
					throw new NotSupportedException("Unable to subscribe events.TimeBatchUpdate is already enabled. Please subscribed on EditableListRaised.");

				base.ListChanged += value;
				_isListChangedHasSubscribedOutSide = true;
			}
			remove
			{
				base.ListChanged -= value;
			}
		}

		public bool AsyncBaseUpdate
		{
			get { return _asyncBaseUpdate; }
			set
			{
				if (value)
				{
					AsyncUpdateCache = new List<T>();
					AsyncUpdateCacheTMP = new Dictionary<int, IEnumerable<T>>();
				}
				_asyncBaseUpdate = value;
			}
		}

		public bool AutoAsyncFailedUpdate
		{
			get { return _autoAsyncFailedUpdate; }
			set
			{
				if (AutoAsyncFailedUpdateInterval <= 0)
					throw new ArgumentNullException("AutoAsyncFailedUpdateInterval");
				_autoAsyncFailedUpdate = value;
			}
		}

		public int AutoAsyncFailedUpdateInterval { get; set; }

		public bool IsTimeBatchUpdateEnabled { get; private set; }

		public int TimedBatchUpdateDelay { get; private set; }

		#region Sorting //From http://code.google.com/p/banshee32/trunk/banshee32/Control/SearchableSortableBindingList.cs

		private bool isSorted;
		private PropertyDescriptor sortProperty;
		private ListSortDirection sortDirection;

		protected override bool SupportsSortingCore
		{
			get { return true; }
		}

		// Missing from Part 2
		protected override ListSortDirection SortDirectionCore
		{
			get { return sortDirection; }
		}

		// Missing from Part 2
		protected override PropertyDescriptor SortPropertyCore
		{
			get { return sortProperty; }
		}

		protected override bool IsSortedCore
		{
			get { return isSorted; }
		}

		protected override void ApplySortCore(PropertyDescriptor property, ListSortDirection direction)
		{
			// Get list to sort
			// Note: this.Items is a non-sortable ICollection<T>
			List<T> items = Items as List<T>;

			// Apply and set the sort, if items to sort
			if (items != null)
			{
				PropertyComparer<T> pc = new PropertyComparer<T>(property, direction);

				items.Sort(pc);
				isSorted = true;
			}
			else
			{
				isSorted = false;
			}

			sortProperty = property;
			sortDirection = direction;

			OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
		}

		#endregion Sorting //From http://code.google.com/p/banshee32/trunk/banshee32/Control/SearchableSortableBindingList.cs

		#region Searching //From http://code.google.com/p/banshee32/trunk/banshee32/Control/SearchableSortableBindingList.cs

		protected override bool SupportsSearchingCore
		{
			get { return true; }
		}

		protected override int FindCore(PropertyDescriptor property, object key)
		{
			//TODO: Podría hacer algo como que si property es null busque en todos los campos
			//Todo: si solo quiero buscar por el comienzo?
			//Todo: el qkey tienq que ser texto si o si?

			// Specify search columns
			if (property == null) return -1;

			// Get list to search
			List<T> items = Items as List<T>;

			// Traverse list for value
			foreach (T item in items)
			{
				// Test column search value
				string value = (string)property.GetValue(item);

				// If value is the search value, return the
				// index of the data item
				if ((string)key == value) return IndexOf(item);
			}
			return -1;
		}

		#endregion Searching //From http://code.google.com/p/banshee32/trunk/banshee32/Control/SearchableSortableBindingList.cs
	}
}
