﻿using System;
using System.Diagnostics;

namespace GCSC.Util
{
	public sealed class AppEvents
	{
		/// <summary>
		/// Logging directly to system events
		/// </summary>
		/// <param name="logName">the Log name</param>
		internal AppEvents(string logName)
			: this(logName, Process.GetCurrentProcess().ProcessName, ".")
		{
		}

		/// <summary>
		/// Logging directly to system events
		/// </summary>
		/// <param name="logName">the Log name</param>
		/// <param name="source">the source log</param>
		public AppEvents(string logName, string source)
			: this(logName, source, ".")
		{
		}

		/// <summary>
		/// Logging directly to system events
		/// </summary>
		/// <param name="logName">The Log name</param>
		/// <param name="source">the source log</param>
		/// <param name="machineName">the machine name</param>
		public AppEvents(string logName, string source, string machineName)
		{
			this.logName = logName;
			this.source = source;
			this.machineName = machineName;

			if (!EventLog.SourceExists(source, machineName))
				EventLog.CreateEventSource(new EventSourceCreationData(source, logName) { MachineName = machineName });

			log = new EventLog(logName, machineName, source) { EnableRaisingEvents = true };
		}

		private EventLog log;
		private readonly string source = string.Empty;
		private readonly string logName = string.Empty;
		private readonly string machineName = string.Empty;

		/// <summary>
		/// Get the log name
		/// </summary>
		public string Name
		{
			get { return logName; }
		}

		/// <summary>
		/// Get the source name
		/// </summary>
		public string SourceName
		{
			get { return source; }
		}

		/// <summary>
		/// Get thr machine name
		/// </summary>
		public string Machine
		{
			get { return machineName; }
		}

		/// <summary>
		/// Write the log
		/// </summary>
		/// <param name="message">The message to be written</param>
		/// <param name="type">the eventype </param>
		/// <param name="category">the log category</param>
		/// <param name="eventID">the event id type</param>
		public void WriteToLog(string message, EventLogEntryType type, CategoryType category, EventIDTytpe eventID)
		{
			isNullLog();

			log.WriteEntry(message, type, (int)eventID, (short)category);
		}

		/// <summary>
		/// write the log
		/// </summary>
		/// <param name="message">the message to be written</param>
		/// <param name="type">the event type</param>
		/// <param name="category">the category tpe</param>
		/// <param name="eventID">the event id type</param>
		/// <param name="rawData">the raw data to be  written</param>
		public void WriteToLog(string message, EventLogEntryType type, CategoryType category,
								EventIDTytpe eventID, byte[] rawData)
		{
			isNullLog();

			log.WriteEntry(message, type, (int)eventID, (short)category, rawData);
		}

		/// <summary>
		/// Get all the log entries
		/// </summary>
		/// <returns>log entries</returns>
		public EventLogEntryCollection GetEntries()
		{
			isNullLog();

			return log.Entries;
		}

		/// <summary>
		/// Clear the logs
		/// </summary>
		public void ClearLog()
		{
			isNullLog();

			log.Clear();
		}

		/// <summary>
		/// close the log
		/// </summary>
		public void CloseLog()
		{
			isNullLog();

			log.Close();
			log = null;
		}

		/// <summary>
		/// Delete thelog fom the system events
		/// </summary>
		public void DeleteLog()
		{
			if (EventLog.SourceExists(source, machineName))
				EventLog.DeleteEventSource(source, machineName);

			if (logName != "Application" &&
			logName != "Security" &&
			logName != "System")
				if (EventLog.Exists(logName, machineName))
					EventLog.Delete(logName, machineName);

			if (log == null)
				return;

			log.Close();
			log = null;
		}

		private void isNullLog()
		{
			if (log == null)
				throw new ArgumentNullException("log", "This Event Log has not been opened or has been closed.");
		}
	}
}
