using DevExpress.XtraReports.UI;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace GCSC.Util
{
	public static class Extensions
	{
		private readonly static Random random = new Random();

		/// <summary>
		/// Gets a random element from a callection of elements that implments the IEnumerable interface
		/// </summary>
		/// <typeparam name="T">The type of each element in the collection</typeparam>
		/// <param name="list">A collection of elements of type T</param>
		/// <returns>Returns a random element from a collection</returns>
		public static T GetRandomElement<T>(this IEnumerable<T> list)
		{
			if (list == null)
				throw new ArgumentNullException("list");

			// Get the number of elements in the collection
			int count = list.Count();

			// If there are no elements in the collection, return the default value of T
			if (count == 0)
				return default(T);

			// Get a random index
			int index = random.Next(list.Count());

			// When the collection has 100 elements or less, get the random element
			// by traversing the collection one element at a time.
			if (count <= 100)
			{
				using (var enumerator = list.GetEnumerator())
				{
					// Move down the collection one element at a time.
					// When index is -1 we are at the random element location
					while (index >= 0 && enumerator.MoveNext())
						index--;

					// Return the current element
					return enumerator.Current;
				}
			}

			// Get an element using LINQ which casts the collection
			// to an IList and indexes into it.
			return list.ElementAt(index);
		}

		public static void ShowFormChild(this System.Windows.Forms.Form parentForm, System.Windows.Forms.Form childForm,
			System.Windows.Forms.FormStartPosition position = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation)
		{
			childForm.MdiParent = parentForm;
			childForm.StartPosition = position;
			childForm.Show();
		}

		/// <summary>
		/// Reverse the array
		/// </summary>
		/// <param name="theArray">the source array</param>
		public static void DoReversal<T>(this T[] theArray)
		{
			theArray.DoReversal(0, 0);
		}

		/// <summary>
		/// Reverse the array
		/// </summary>
		/// <param name="theArray">the source array</param>
		/// <param name="index">starting indxing for reversing</param>
		/// <param name="count">number f items to be reversed</param>
		public static void DoReversal<T>(this T[] theArray, int index, int count)
		{
			var tempHolder = default(T);
			if (theArray == null)
				throw new ArgumentNullException("list");
			if (theArray.Length < count || theArray.Length < index + count)
				throw new ArgumentOutOfRangeException("count");
			if (theArray.Length < 0)
				throw new ArgumentOutOfRangeException("index");

			if (theArray.Length > 0)
			{
				count = (count > 0) ? count : theArray.Length;
				for (int i = 0; i < (count / 2); i++)
				{
					tempHolder = theArray[i + index];
					theArray[i + index] = theArray[theArray.Length - i - 1 + index];
					theArray[theArray.Length - i - 1 + index] = tempHolder;
				}
			}
			else
				Trace.WriteLine("Nothing to reverse");
		}

		/// <summary>
		/// Count all the item specified in the array
		/// </summary>
		/// <param name="theArray">the sourcearray</param>
		/// <param name="searchValue">the item to be searched</param>
		/// <returns></returns>
		public static int CountAll<T>(this T[] theArray, T searchValue)
		{
			return ((from t in theArray
					 where t.Equals(searchValue)
					 select t).Count());
		}

		/// <summary>
		/// Count all the item specified in the list
		/// </summary>
		/// <param name="theArray">the source list</param>
		/// <param name="searchValue">the item to be searched</param>
		/// <returns></returns>
		public static int CountAll<T>(this List<T> theArray, T searchValue)
		{
			return ((from t in theArray
					 where t.Equals(searchValue)
					 select t).Count());
		}

		/// <summary>
		/// Insert item in the array
		/// </summary>
		/// <param name="target">the source array</param>
		/// <param name="value">the item to be inserted</param>
		/// <param name="index">the index where the item will be inserted</param>
		public static void InsertIntoArray(this Array target, object value, int index)
		{
			if (index < target.GetLowerBound(0) || index > target.GetUpperBound(0))
				throw new ArgumentOutOfRangeException("index", index, "Array index out of bounds.");

			Array.Copy(target, index, target, index + 1, target.Length - index - 1);

			target.SetValue(value, index);
		}

		/// <summary>
		/// Remove item from array
		/// </summary>
		/// <param name="target">the source T Array</param>
		/// <param name="index">index of the item to be remove</param>
		public static void RemoveFromArray<T>(this T[] target, int index)
		{
			if (index < target.GetLowerBound(0) || index > target.GetUpperBound(0))
				throw new ArgumentOutOfRangeException("index", index, "Array index out of bounds.");
			else
				if (index < target.GetUpperBound(0))
					Array.Copy(target, index + 1, target, index, target.Length - index - 1);

			target.SetValue(null, target.GetUpperBound(0));
		}

		public static Int16 ToInt16(this object value)
		{
			if (Extra.IsTypeOf<DBNull>(value)) return default(Int16);
			return Convert.ToInt16(value);
		}

		public static int ToInt(this object value)
		{
			if (Extra.IsTypeOf<DBNull>(value)) return default(int);
			return Convert.ToInt32(value);
		}

		public static Int64 ToInt64(this object value)
		{
			if (Extra.IsTypeOf<DBNull>(value)) return default(Int64);
			return Convert.ToInt64(value);
		}

		public static long ToLong(this object value)
		{
			if (Extra.IsTypeOf<DBNull>(value)) return default(long);
			return Convert.ToInt64(value);
		}

		public static decimal ToDecimal(this object value)
		{
			if (Extra.IsTypeOf<DBNull>(value)) return default(decimal);
			return Convert.ToDecimal(value);
		}

		public static double ToDouble(this object value)
		{
			if (Extra.IsTypeOf<DBNull>(value)) return default(double);
			return Convert.ToDouble(value);
		}

		public static Boolean Tobool(this object value)
		{
			if (Extra.IsTypeOf<DBNull>(value)) return default(Boolean);
			return Convert.ToBoolean(value);
		}

		public static DateTime ToDateTime(this object value)
		{
			if (Extra.IsTypeOf<DBNull>(value)) return default(DateTime);
			return Convert.ToDateTime(value);
		}

		public static object EscapeString(this object value, bool isQouted = false)
		{
			return Extra.IsTypeOf<string>(value) ? (isQouted ? "'" + MySqlHelper.EscapeString((string)value) + "'" : MySqlHelper.EscapeString((string)value)) : value;
		}

		public static string EscapeString(this string value)
		{
			return MySqlHelper.EscapeString(value);
		}

		public static object DefaultValue(Type targetValue)
		{
			return targetValue.IsValueType ? Activator.CreateInstance(targetValue) : null;
		}

		public static bool ImplementsInterface(this Type type, Type theInterface)
		{
			return (type.GetInterfaces().Contains(theInterface));
		}

		public static bool InheritsClass(this Type type, Type theClass)
		{
			if (type == null)
				return false;
			if (type == theClass)
				return true;
			else
				return type.BaseType.InheritsClass(theClass);
		}

		public static bool ImplementsInterface<InterfaceType>(this Type type)
		{
			return ImplementsInterface(type, typeof(InterfaceType));
		}

		public static string UCase(this string str)
		{
			return Microsoft.VisualBasic.Strings.StrConv(str.Trim(), Microsoft.VisualBasic.VbStrConv.ProperCase);
		}

		public static bool IsNumber(this string str)
		{
			return Extra.IsNumber(str);
		}

		public static bool IsNumber(this object str)
		{
			return str.ToString().IsNumber();
		}

		public static void ShowPreviewDialog(this XtraReport rpt, System.Windows.Forms.Form form)
		{
			using (var rpTool = new ReportPrintTool(rpt))
			{
				rpTool.ShowPreviewDialog(form, DevExpress.LookAndFeel.UserLookAndFeel.Default);
			}
		}

		public static void Print(this XtraReport rpt, string printerName = "")
		{
			using (var rpTool = new ReportPrintTool(rpt))
			{
				if (string.IsNullOrWhiteSpace(printerName))
				{
					rpTool.Print();
					return;
				}
				rpTool.Print(printerName);
			}
		}

		public static string NumberToWords(this int number)
		{
			return NumberToWords(number, NumberWordSeparator.BySpace);
		}

		public static string NumberToWords(this int number, NumberWordSeparator nws)
		{
			if (number == 0)
				return "zero";

			if (number < 0)
				return "minus " + NumberToWords(Math.Abs(number), nws);

			string words = "";

			if ((number / 1000000) > 0)
			{
				words += NumberToWords(number / 1000000, nws) + " million ";
				number %= 1000000;
			}

			if ((number / 1000) > 0)
			{
				words += NumberToWords(number / 1000, nws) + " thousand ";
				number %= 1000;
			}

			if ((number / 100) > 0)
			{
				words += NumberToWords(number / 100, nws) + " hundred ";
				number %= 100;
			}

			if (number > 0)
			{
				if (words != "")
				{
					switch (nws)
					{
						case NumberWordSeparator.BySpace:
							words += " ";
							break;

						case NumberWordSeparator.ByAndWord:
							words += "and ";
							break;

						default:
							throw new ArgumentException("Unkbnown NumberWordSeparator");
					}
				}

				var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
				var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

				if (number < 20)
					words += unitsMap[number];
				else
				{
					words += tensMap[number / 10];
					if ((number % 10) > 0)
						words += "-" + unitsMap[number % 10];
				}
			}

			return words;
		}

		public static string DecimalToWords(this decimal number, NumberWordSeparator nws)
		{
			if (number == 0)
				return "zero";

			if (number < 0)
				return "minus " + Math.Abs(number).DecimalToWords(nws);

			string words = "";

			int intPortion = (int)number;
			decimal fraction = (number - intPortion) * 100;
			int decPortion = (int)fraction;

			words = intPortion.NumberToWords(nws);
			if (decPortion > 0)
			{
				words += " and ";
				words += decPortion.NumberToWords(nws);
			}
			return words;
		}

		public static string ToOrdinal(this int num)
		{
			if (num <= 0) return num.ToString();

			switch (num % 100)
			{
				case 11:
				case 12:
				case 13:
					return num + "th";
			}

			switch (num % 10)
			{
				case 1:
					return num + "st";

				case 2:
					return num + "nd";

				case 3:
					return num + "rd";

				default:
					return num + "th";
			}
		}
	}
}
