﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GCSC.Util
{
	public sealed class MultiMapCollections<TKey, UValue> : IDictionary<TKey, List<UValue>>, IDisposable
	{
		private Dictionary<TKey, List<UValue>> map;
		private bool isDisposed;

		public MultiMapCollections()
		{
			map = new Dictionary<TKey, List<UValue>>();
		}

		~MultiMapCollections()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool isDisposing)
		{
			if (!isDisposed)
				if (isDisposing)
					map = null;

			isDisposed = true;
		}

		/// <summary>
		/// Theget accessor obtains aList<U> of all values that are associated with a key. The set accessor adds an entireList<U> of values to a key.
		/// </summary>
		public List<UValue> this[TKey key]
		{
			get { return map[key]; }
			set { map[key] = value; }
		}

		/// <summary>
		/// Adds a key to the Dictionary<T,List<U>> and its associated value.
		/// </summary>
		/// <param name="key">TKey key</param>
		/// <param name="item">Item list</param>
		public void Add(TKey key, UValue item)
		{
			AddSingleMap(key, item);
		}

		public void Add(TKey key, params UValue[] items)
		{
			Array.ForEach(items, val => AddSingleMap(key, val));
		}

		/// <summary>
		/// Adds an element with the provided key and value to the <see cref="T:System.Collections.Generic.IDictionary`2" />.
		/// </summary>
		/// <param name="key">
		/// The object to use as the key of the element to add.
		/// </param>
		/// <param name="value">
		/// The object to use as the value of the element to add.
		/// </param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="key" /> is null.
		/// </exception>
		/// <exception cref="T:System.ArgumentException">
		/// An element with the same key already exists in the <see cref="T:System.Collections.Generic.IDictionary`2" />.
		/// </exception>
		/// <exception cref="T:System.NotSupportedException">
		/// The <see cref="T:System.Collections.Generic.IDictionary`2" /> is read-only.
		/// </exception>
		public void Add(TKey key, List<UValue> items)
		{
			items.ForEach(val => AddSingleMap(key, val));
		}

		/// <summary>
		/// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </summary>
		/// <param name="item">
		/// The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </param>
		/// <exception cref="T:System.NotSupportedException">
		/// The <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
		/// </exception>
		public void Add(KeyValuePair<TKey, List<UValue>> keyValuePair)
		{
			keyValuePair.Value.ForEach(val => AddSingleMap(keyValuePair.Key, val));
		}

		/// <summary>
		/// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </summary>
		/// <exception cref="T:System.NotSupportedException">
		/// The <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
		/// </exception>
		public void Clear()
		{
			map.Clear();
		}

		/// <summary>
		/// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </summary>
		/// <returns>
		/// The number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </returns>
		public int Count
		{
			get { return map.Count; }
		}

		/// <summary>
		/// Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an element with the specified key.
		/// </summary>
		/// <returns>
		/// true if the <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an element with the key; otherwise, false.
		/// </returns>
		/// <param name="key">
		/// The key to locate in the <see cref="T:System.Collections.Generic.IDictionary`2" />.
		/// </param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="key" /> is null.
		/// </exception>
		public bool ContainsKey(TKey key)
		{
			return map.ContainsKey(key);
		}

		public bool GetKeyByValue(UValue item, out TKey key)
		{
			foreach (KeyValuePair<TKey, List<UValue>> kvp in map)
				if (((List<UValue>)kvp.Value).Contains(item))
				{
					key = kvp.Key;
					return true;
				}

			key = default(TKey);
			return false;
		}

		/// <summary>
		/// Returns abool indicating whether theMultiMap<T,U> contains a particular value.
		/// </summary>
		/// <param name="item">The value that you looking for</param>
		/// <returns>bool</returns>
		public bool ContainsValue(UValue item)
		{
			if (item == null)
			{
				foreach (KeyValuePair<TKey, List<UValue>> kvp in map)
					if (((List<UValue>)kvp.Value).Count == 0)
						return true;
				return false;
			}
			else
			{
				foreach (KeyValuePair<TKey, List<UValue>> kvp in map)
					if (((List<UValue>)kvp.Value).Contains(item))
						return true;
				return false;
			}
		}

		/// <summary>
		/// Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>
		/// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
		/// </returns>
		/// <filterpriority>1</filterpriority>
		IEnumerator<KeyValuePair<TKey, List<UValue>>> IEnumerable<KeyValuePair<TKey, List<UValue>>>.GetEnumerator()
		{
			return map.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>
		/// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
		/// </returns>
		/// <filterpriority>1</filterpriority>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return map.GetEnumerator();
		}

		/// <summary>
		/// Removes the element with the specified key from the <see cref="T:System.Collections.Generic.IDictionary`2" />.
		/// </summary>
		/// <returns>
		/// true if the element is successfully removed; otherwise, false.  This method also returns false if <paramref name="key" /> was not found in the original <see cref="T:System.Collections.Generic.IDictionary`2" />.
		/// </returns>
		/// <param name="key">
		/// The key of the element to remove.
		/// </param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="key" /> is null.
		/// </exception>
		/// <exception cref="T:System.NotSupportedException">
		/// The <see cref="T:System.Collections.Generic.IDictionary`2" /> is read-only.
		/// </exception>
		public bool Remove(TKey key)
		{
			return RemoveSingleMap(key);
		}

		/// <summary>
		/// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </summary>
		/// <returns>
		/// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </returns>
		/// <param name="item">
		/// The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </param>
		/// <exception cref="T:System.NotSupportedException">
		/// The <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
		/// </exception>
		public bool Remove(KeyValuePair<TKey, List<UValue>> keyValuePair)
		{
			return Remove(keyValuePair.Key);
		}

		private void AddSingleMap(TKey key, UValue item)
		{
			if (map.ContainsKey(key))
				((List<UValue>)map[key]).Add(item);
			else
			{
				if (item == null)
					map.Add(key, new List<UValue>());
				else
					map.Add(key, new List<UValue>() { item });
			}
		}

		private bool RemoveSingleMap(TKey key)
		{
			if ((ContainsKey(key)))
				return map.Remove(key);

			throw new ArgumentOutOfRangeException("key", key, "This key does not exists in the map.");
		}

		/// <summary>
		/// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
		/// </summary>
		/// <returns>
		/// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
		/// </returns>
		/// <param name="item">
		/// The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </param>
		public bool Contains(KeyValuePair<TKey, List<UValue>> item)
		{
			foreach (KeyValuePair<TKey, List<UValue>> k in map)
			{
				if (k.Value.Count != item.Value.Count)
					continue;

				int counter = 0;
				foreach (UValue u in item.Value)
				{
					counter++;
					if (!k.Value.Contains(u))
						break;

					if (counter == k.Value.Count)
						return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
		/// </summary>
		/// <returns>
		/// true if the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only; otherwise, false.
		/// </returns>
		public bool IsReadOnly
		{
			get { return false; }
		}

		/// <summary>
		/// Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in the <see cref="T:System.Collections.Generic.IDictionary`2" />.
		/// </summary>
		/// <returns>
		/// An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in the object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
		/// </returns>
		public ICollection<List<UValue>> Values
		{
			get
			{
				List<List<UValue>> l = new List<List<UValue>>();
				foreach (KeyValuePair<TKey, List<UValue>> k in map)
					l.Add(k.Value);

				return l.ToArray();
			}
		}

		/// <summary>
		/// Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the <see cref="T:System.Collections.Generic.IDictionary`2" />.
		/// </summary>
		/// <returns>
		/// An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
		/// </returns>
		public ICollection<TKey> Keys
		{
			get
			{
				List<TKey> l = new List<TKey>();
				foreach (KeyValuePair<TKey, List<UValue>> k in map)
					l.Add(k.Key);

				return l.ToArray();
			}
		}

		public List<UValue>[] ToArray()
		{
			List<List<UValue>> l = new List<List<UValue>>();
			foreach (KeyValuePair<TKey, List<UValue>> k in map)
				l.Add(k.Value);

			return l.ToArray();
		}

		/// <summary>
		/// Gets the value associated with the specified key.
		/// </summary>
		/// <param name="key">The key of the value to get.</param>
		/// <param name="value">When this method returns, contains the value associated with the specified key, if the key is found;
		/// otherwise, the default value for the type of the value parameter. This parameter is passed uninitialized.</param>
		/// <returns>true if the <see cref="T:System.Collections.Generic.ICollection`1" /> key is found; otherwise, false.</returns>
		public bool TryGetValue(TKey key, out List<UValue> value)
		{
			value = default(List<UValue>);
			if (map.ContainsKey(key))
			{
				value = map[key];
				return true;
			}

			return false;
		}

		/// <summary>
		/// Copies the elements of the <see cref="T:System.Collections.Generic.KeyValuePair" /> to an Array, starting at a particular Array index.
		/// </summary>
		/// <param name="keyValuePair"></param>
		/// <param name="arrayIndex"></param>
		public void CopyTo(KeyValuePair<TKey, List<UValue>>[] keyValuePair, int arrayIndex)
		{
			List<KeyValuePair<TKey, List<UValue>>> tempList = new List<KeyValuePair<TKey, List<UValue>>>();
			tempList.AddRange(map);
			tempList.CopyTo(keyValuePair, arrayIndex);
		}
	}
}
