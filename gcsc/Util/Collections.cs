using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace GCSC.Util
{
	[Obsolete]
	/// <summary>
	/// A custom collection for general purpose.
	/// This class cannot be inherete.
	/// </summary>
	/// <typeparam name="T">The type of elements in the collection.</typeparam>
	public sealed class Collections<T> : IEnumerable<T>, IEnumerable, ICollection<T>, IDisposable
	{
		private List<T> _params;
		private bool isDisposed;
		private int? currentPointer = -1;

		public Collections()
		{
			_params = new List<T>();
		}

		public Collections(T[] dataCollection)
		{
			_params = new List<T>();
			_params.AddRange(dataCollection);
		}

		public Collections(List<T> list)
		{
			_params = new List<T>();
			_params = list;
		}

		public Collections(int capacity)
		{
			_params = new List<T>(capacity);
		}

		public Collections(IEnumerable<T> collection)
		{
			_params = new List<T>(collection);
		}

		~Collections()
		{
			Dispose(false);
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose()
		{
			Dispose(true);

			GC.SuppressFinalize(this);
		}

		private void Dispose(bool isDisposing)
		{
			if (!isDisposed)
			{
				if (isDisposing)
				{
					_params = null;
					currentPointer = null;
				}
			}

			isDisposed = true;
		}

		public T this[int index]
		{
			get { return Value(index); }
			set { _params[index] = value; }
		}

		/// <summary>
		/// Add new item
		/// </summary>
		/// <param name="param">Item</param>
		public void Add(T param)
		{
			_params.Add(param);
		}

		/// <summary>
		/// Adds element of the specified collection to the end of the collection
		/// </summary>
		/// <param name="collection">IList<T> collection</T>
		///  </param>
		public void AddRange(List<T> collection)
		{
			_params.AddRange(collection);
		}

		public void AddRange(IEnumerable<T> collection)
		{
			_params.AddRange(collection);
		}

		public ReadOnlyCollection<T> AsReadOnly()
		{
			return _params.AsReadOnly();
		}

		public int BinarySearch(int index, int count, T item, IComparer<T> comparer)
		{
			return _params.BinarySearch(index, count, item, comparer);
		}

		public int BinarySearch(T item, IComparer<T> comparer)
		{
			return _params.BinarySearch(item, comparer);
		}

		public int BinarySearch(T item)
		{
			return _params.BinarySearch(item);
		}

		public void Clear()
		{
			_params.Clear();
		}

		public bool Contains(T item)
		{
			return _params.Contains(item);
		}

		public List<TOutput> ConvertAll<TOutput>(Converter<T, TOutput> converter)
		{
			return _params.ConvertAll(converter);
		}

		public void CopyTo(T[] array)
		{
			_params.CopyTo(array);
		}

		public void CopyTo(int index, T[] array, int arrayIndex, int count)
		{
			_params.CopyTo(index, array, arrayIndex, count);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			_params.CopyTo(array, arrayIndex);
		}

		public bool Exists(Predicate<T> match)
		{
			return _params.Exists(match);
		}

		public T Find(Predicate<T> match)
		{
			return _params.Find(match);
		}

		public Collection<T> FindAll(Predicate<T> match)
		{
			if (match == null)
				throw new ArgumentNullException("match");
			var c = new Collection<T>();
			for (int i = 0; i < Count; ++i)
				if (match(_params[i]))
					c.Add(_params[i]);

			return c;
		}

		public int FindIndex(int startIndex, int count, Predicate<T> match)
		{
			return _params.FindIndex(startIndex, count, match);
		}

		public int FindIndex(int startIndex, Predicate<T> match)
		{
			return _params.FindIndex(startIndex, match);
		}

		public int FindIndex(Predicate<T> match)
		{
			return _params.FindIndex(match);
		}

		public T FindLast(Predicate<T> match)
		{
			return _params.FindLast(match);
		}

		public int FindLastIndex(Predicate<T> match)
		{
			return _params.FindLastIndex(match);
		}

		public int FindLastIndex(int startIndex, int count, Predicate<T> match)
		{
			return _params.FindLastIndex(startIndex, count, match);
		}

		public int FindLastIndex(int startIndex, Predicate<T> match)
		{
			return _params.FindLastIndex(startIndex, match);
		}

		public void ForEach(Action<T> action)
		{
			_params.ForEach(action);
		}

		/// <summary>
		/// Convert array into collection
		/// </summary>
		/// <param name="args">array</param>
		/// <returns>collection</returns>
		public T[] GetArray(params T[] args)
		{
			Array.ForEach(args, Add);
			return _params.ToArray();
		}

		/// <summary>
		/// Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>
		/// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
		/// </returns>
		/// <filterpriority>1</filterpriority>
		public IEnumerator<T> GetEnumerator()
		{
			foreach (T item in _params)
				yield return item;
		}

		/// <summary>
		/// Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>
		/// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
		/// </returns>
		/// <filterpriority>1</filterpriority>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public List<T> GetRange(int index, int count)
		{
			return _params.GetRange(index, count);
		}

		public int IndexOf(T item, int index, int count)
		{
			return _params.IndexOf(item, index, count);
		}

		/// <summary>
		/// Will locate the item position
		/// </summary>
		/// <param name="item">item</param>
		/// <returns>int position</returns>
		public int IndexOf(T item)
		{
			return _params.IndexOf(item);
		}

		public int IndexOf(T item, int index)
		{
			return _params.IndexOf(item, index);
		}

		/// <summary>
		/// Insert item to the specific collection
		/// </summary>
		/// <param name="index">position of the collection</param>
		/// <param name="item">T item to inserted</param>
		public void Insert(int index, T item)
		{
			_params.Insert(index, item);
		}

		public void InsertRange(int index, IEnumerable<T> collection)
		{
			_params.InsertRange(index, collection);
		}

		private static List<T> internalReverse(List<T> list, int index, int count)
		{
			var l = new List<T>(list);
			internalReverse(ref l, index, count);

			return l;
		}

		private static void internalReverse(ref List<T> list, int index, int count)
		{
			T tempHolder = default(T);

			if (list == null)
				throw new ArgumentNullException("list");
			if (list.Count < count || list.Count < index + count)
				throw new ArgumentOutOfRangeException("count");
			if (index < 0)
				throw new ArgumentOutOfRangeException("index");

			if (list.Count > 0)
			{
				count = (count > 0) ? count : list.Count;
				for (int i = 0; i < (count / 2); i++)
				{
					tempHolder = list[i + index];
					list[i + index] = list[count - i - 1 + index];
					list[count - i - 1 + index] = tempHolder;
				}
			}
			else
				Trace.WriteLine("Nothing to Reverse");
		}

		public int LastIndexOf(T item)
		{
			return _params.LastIndexOf(item);
		}

		public int LastIndexOf(T item, int index)
		{
			return _params.LastIndexOf(item, index);
		}

		public int LastIndexOf(T item, int index, int count)
		{
			return _params.LastIndexOf(item, index, count);
		}

		/// <summary>
		/// Move to the next value
		/// </summary>
		/// <returns>boolean: return true if collection index is still has a value</returns>
		public bool MoveNext()
		{
			if (_params.Count < 1)
				throw new ArgumentNullException("Collection");

			currentPointer++;

			if (currentPointer > _params.Count - 1)
				return false;

			return true;
		}

		/// <summary>
		/// Move Back to the next value
		/// </summary>
		/// <returns>boolean: return true if collection index is still has a value</returns>
		public bool MoveBack()
		{
			if (_params.Count < 0)
				throw new ArgumentNullException("Collection");

			if (currentPointer < 0)
				currentPointer = _params.Count;

			currentPointer--;

			if (currentPointer < 0)
				return false;

			return true;
		}

		/// <summary>
		/// Reset the collection index
		/// </summary>
		public void MoveReset()
		{
			currentPointer = -1;
		}

		public bool Remove(T item)
		{
			return _params.Remove(item);
		}

		/// <summary>
		/// Remove value on the specific position of the data
		/// </summary>
		/// <param name="position">int position</param>
		/// <returns>the removed item</returns>
		public T RemoveAt(int position)
		{
			T item = default(T);
			if (_params.Count == 0)
				return item;

			item = _params[position];
			_params.RemoveAt(position);
			return item;
		}

		/// <summary>
		/// Remove all the data of the collection
		/// </summary>
		/// <returns>array of data</returns>
		public T[] RemoveAll()
		{
			T[] i = _params.ToArray();
			_params.Clear();
			return i;
		}

		public int RemoveAll(Predicate<T> match)
		{
			return _params.RemoveAll(match);
		}

		public T[] RemoveRange(int index, int count)
		{
			T[] item = default(T[]);
			if (index < 0)
				throw new ArgumentOutOfRangeException("index");
			if (count < 0)
				throw new ArgumentOutOfRangeException("count");
			if (Count - index < count)
				throw new ArgumentException("Argument_InvalidOffLen");
			if (count < 0)
				return item;

			Array.Copy(_params.ToArray(), index, item, 0, count);
			_params.RemoveRange(index, count);

			return item;
		}

		/// <summary>
		/// Reverse the collection
		/// </summary>
		/// <param name="applyInCollection">optional: Set to true if you want it to apply in the collection</param>
		/// <returns>Array T Value</returns>
		public T[] Reverse(bool applyInCollection = false)
		{
			return Reverse(0, 0, applyInCollection);
		}

		/// <summary>
		/// Reverse the collection
		/// </summary>
		/// <param name="index">the starting index</param>
		/// <param name="count">th number of element to the range to reverse</param>
		/// <param name="applyInCollection">optional: Set to true if you want it to apply in the collection</param>
		/// <returns>Array T Value</returns>
		public T[] Reverse(int index, int count, bool applyInCollection = false)
		{
			if (applyInCollection)
			{
				T[] i = _params.ToArray();
				internalReverse(ref _params, index, count);
				return i;
			}

			return internalReverse(_params, index, count).ToArray();
		}

		public void Sort()
		{
			_params.Sort();
		}

		public void Sort(int index, int count, IComparer<T> comparer)
		{
			_params.Sort(index, count, comparer);
		}

		public void Sort(Comparison<T> comparison)
		{
			_params.Sort(comparison);
		}

		/// <summary>
		/// Convert the collection into aray
		/// </summary>
		/// <returns>collection array</returns>
		public T[] ToArray()
		{
			return _params.ToArray();
		}

		public void TrimExcess()
		{
			_params.TrimExcess();
		}

		public bool TrueForAll(Predicate<T> match)
		{
			return _params.TrueForAll(match);
		}

		/// <summary>
		/// Get a value of the collection
		/// </summary>
		/// <returns>T Value</returns>
		public T Value()
		{
			if (currentPointer < 0)
				throw new ArgumentOutOfRangeException("MoveNext");

			return Value((int)currentPointer);
		}

		/// <summary>
		/// Get the value of the speific position of the collection
		/// </summary>
		/// <param name="position">int position</param>
		/// <returns>T Value</returns>
		public T Value(int position)
		{
			return _params.Count != 0 ? _params[position] : default(T);
		}

		public int Capacity
		{
			get { return _params.Capacity; }
			set { _params.Capacity = value; }
		}

		/// <summary>
		/// Count the number if items
		/// </summary>
		/// <returns>number of items</returns>
		public int Count
		{
			get { return _params.Count; }
		}

		/// <summary>
		/// it will return the internal data
		/// </summary>
		public List<T> InternalCollection
		{
			get { return _params; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		/// <summary>
		/// Gets the last value of the collection
		/// </summary>
		public T LastValue
		{
			get { return _params[_params.Count - 1]; }
		}
	}
}
