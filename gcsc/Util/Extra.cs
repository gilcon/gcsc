﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace GCSC.Util
{
	public static class Extra
	{
		public static bool IsNumber(string s)
		{
			if (string.IsNullOrWhiteSpace(s)) return false;
			var r = new Regex(@"^-?[0-9]\d*(\.\d+)?$");
			return r.Match(s).Success;
		}

		public static bool IsTypeOf<T>(object o)
		{
			return (o.GetType().IsEquivalentTo(typeof(T)));
		}

		public static void IsTypeOf<T>(ref object o)
		{
			if (!IsTypeOf<T>(o))
				o = default(T);
		}

		public static void IsTypeOf(Type compareFrom, ref object compareTo)
		{
			if (compareTo.GetType().IsEquivalentTo(compareFrom))
				return;

			if (compareFrom.IsEnum)
			{
				compareTo = Enum.Parse(compareFrom, compareTo.ToString(), true);
				return;
			}

			if (compareFrom.IsValueType)
			{
				compareTo = Activator.CreateInstance(compareFrom);
				return;
			}

			compareTo = null;
		}

		/// <summary>
		/// Convert Decimal to Byte Array
		/// </summary>
		/// <param name="dec">decimal dex</param>
		/// <returns>byte array result</returns>
		public static byte[] DecimalToByteArray(decimal dec)
		{
			using (var stream = new MemoryStream())
			{
				using (var writer = new BinaryWriter(stream))
				{
					writer.Write(dec);
					return stream.ToArray();
				}
			}
		}

		/// <summary>
		/// convert Byte Array to Decimal
		/// </summary>
		/// <param name="src">byte Array</param>
		/// <returns>decimal</returns>
		public static decimal ByteArrayToDecimal(byte[] src)
		{
			using (var stream = new MemoryStream(src))
			{
				using (var reader = new BinaryReader(stream))
					return reader.ReadDecimal();
			}
		}

		/// <summary>
		/// Convert string to base64
		/// </summary>
		/// <param name="src">source string</param>
		/// <returns>base64 convertion</returns>
		public static string StringToBase64(string src)
		{
			return Convert.ToBase64String(Encoding.Unicode.GetBytes(src));
		}

		/// <summary>
		/// COnvert base64 to string
		/// </summary>
		/// <param name="src">base64 source</param>
		/// <returns>converted string</returns>
		public static string Base64ToString(string src)
		{
			return Encoding.Unicode.GetString(Convert.FromBase64String(src));
		}

		/// <summary>
		/// Decimal to base64
		/// </summary>
		/// <param name="src">decimal source</param>
		/// <returns>base64 convertion</returns>
		public static string DecimalToBase64(decimal src)
		{
			return Convert.ToBase64String(DecimalToByteArray(src));
		}

		/// <summary>
		/// base64 to decimal
		/// </summary>
		/// <param name="src">base64 source</param>
		/// <returns>decimal convertion</returns>
		public static decimal Base64ToDecimal(string src)
		{
			return ByteArrayToDecimal(Convert.FromBase64String(src));
		}

		/// <summary>
		/// convert int to base64
		/// </summary>
		/// <param name="src">int source</param>
		/// <returns>base64 resut string</returns>
		public static string IntToBase64(int src)
		{
			return Convert.ToBase64String(BitConverter.GetBytes(src));
		}

		/// <summary>
		/// base64 to int
		/// </summary>
		/// <param name="src">base64 source</param>
		/// <returns>intthe converted data</returns>
		public static int Base64ToInt(string src)
		{
			return BitConverter.ToInt32(Convert.FromBase64String(src), 0);
		}

		/// <summary>
		/// Clear the text of the control
		/// </summary>
		/// <param name="control">Controls</param>
		public static void ClearTextControl(params Control[] control)
		{
			foreach (var item in control)
				item.Text = string.Empty;
		}

		/// <summary>
		/// Converting string into proper casing.
		/// </summary>
		/// <param name="Str">"String" String to be converted.</param>
		/// <returns>"String" Converted String.</returns>
		public static string UCase(string Str)
		{
			return Strings.StrConv(Str.Trim(), VbStrConv.ProperCase);
		}

		/// <summary>
		/// Get the minute value of time
		/// </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		public static int GetMinutes(string time)
		{
			string[] data = time.Split(':');

			if (data.Length == 3)
				return Convert.ToInt16(data[2]);
			else
				if (data.Length == 2)
					return Convert.ToInt16(data[1]);
				else
					throw new ArgumentNullException("No Time");
		}

		/// <summary>
		/// Convert Hours to Days
		/// </summary>
		/// <param name="time">String time</param>
		/// <returns>float days</returns>
		///
		[Obsolete]
		public static float Hour2Day(params int[] time)
		{
			if (time.Length < 1)
				throw new ArgumentNullException("Time");

			return (float)((time.Length == 2 ? time[1] : 0) + (time[0] * 60)) / 1440;
		}

		/// <summary>
		/// Convert Days to Hours
		/// </summary>
		/// <param name="day">float days</param>
		/// <returns>string Hours</returns>
		///
		[Obsolete]
		public static string Day2Hour(int day)
		{
			return new TimeSpan(day, 0, 0, 0).TotalHours.ToString();
		}

		/// <summary>
		/// Mysql Valid date format
		/// </summary>
		/// <param name="date">"String" date</param>
		/// <returns>"string" Date</returns>
		public static string MySQLDateFormat(string date)
		{
			return MySQLDateFormat(Convert.ToDateTime(date));
		}

		/// <summary>
		/// Mysql valid date format
		/// </summary>
		/// <param name="date">"DateTime" Date</param>
		/// <returns>"string" Date</returns>
		public static string MySQLDateFormat(DateTime date)
		{
			return date.Year + "-" + date.Month + "-" + date.Day;
		}

		public static WindowsOSType GetOSType()
		{
			return (WindowsOSType)(Environment.OSVersion.Version.Major * 100) +
				(Environment.OSVersion.Version.Minor * 10);
		}

		private static void FindAndReplaveDocText(Word.Application WordApp,
									object findText,
									object replaceWithText)
		{
			object matchCase = true;
			object matchWholeWord = true;
			object matchWildCards = false;
			object matchSoundsLike = false;
			object nmatchAllWordForms = false;
			object forward = true;
			object format = false;
			object matchKashida = false;
			object matchDiacritics = false;
			object matchAlefHamza = false;
			object matchControl = false;
			object read_only = false;
			object visible = true;
			object replace = 2;
			object wrap = 1;

			WordApp.Selection.Find.Execute(ref findText,
				ref matchCase, ref matchWholeWord,
				ref matchWildCards, ref matchSoundsLike,
				ref nmatchAllWordForms, ref forward,
				ref wrap, ref format, ref replaceWithText,
				ref replace, ref matchKashida,
				ref matchDiacritics, ref matchAlefHamza,
				ref matchControl);
		}

		public static void ModifyWordDoc(object fileName, object saveAs, IDictionary<string, string> IDic)
		{
			object missing = System.Reflection.Missing.Value;
			Word.Application wordApp = new Word.ApplicationClass();
			Word.Document aDoc = null;

			if (File.Exists((string)fileName))
			{
				object readOnly = false;
				object isVisible = false;
				wordApp.Visible = false;

				aDoc = wordApp.Documents.Open(ref fileName, ref missing,
					ref readOnly, ref missing, ref missing, ref missing,
					ref missing, ref missing, ref missing, ref missing,
					ref missing, ref isVisible, ref missing, ref missing,
					ref missing, ref missing);

				aDoc.Activate();

				foreach (var item in IDic)
				{
					FindAndReplaveDocText(wordApp, item.Key, item.Value);
				}
			}
			else
			{
				MessageBox.Show("File dose not exist.");
				return;
			}

			aDoc.SaveAs(ref saveAs, ref missing, ref missing, ref missing,
					ref missing, ref missing, ref missing, ref missing,
					ref missing, ref missing, ref missing, ref missing,
					ref missing, ref missing, ref missing, ref missing);

			((Microsoft.Office.Interop.Word._Document)aDoc).Close(ref missing, ref missing, ref missing);

			GC.Collect();
		}
	}
}
