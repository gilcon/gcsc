namespace GCSC
{
	[System.Flags]
	public enum EventIDTytpe
	{
		NA = 0,
		Read = 1,
		Write = 2,
		ExceptionThrown = 3,
		BufferOverflowCondition = 4,
		SecurityFailure = 5,
		SecurityPotentiallyCompromised = 6
	}
}
